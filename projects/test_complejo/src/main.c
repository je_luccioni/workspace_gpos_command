 /*
 * ******************************[ Source file main.c ]*************************************//**
 * \addtogroup main_c 
 * @{
 * @copyright 
 * ${date}, Luccioni Jesús Emanuel. \n
 * All rights reserved.\n
 * This file is part of port module.\n
 * Redistribution is not allowed on binary and source forms, with or without 
 * modification.\n
 * Use is permitted with prior authorization by the copyright holder.\n
 * \file main.c
 * \brief bloque de comenteario para documentacion para describir este archivo de 
 * cabecera o header file. 
 * \version v01.01
 * \date   ${date}
 * \note none
 * \author JEL, Jesus Emanuel Luccioni
 * \li piero.jel@gmail.com
 * @} doxygen end group definition
 *************************************************************************************/



/* **********************************************************************************************
 *
 * ===========================[ Begin include header file ]============================
 *
 * ********************************************************************************************** 
 * 
 */
#include <stdio.h> /* header file correspondiente a las libreria
estandar de entrada salida*/
#include <stdlib.h> /* header file correspondiente a las libreria
estandar para el uso de la funcion @ref exit() con sus 
macros @ref EXIT_FAIL y @ref EXIT_SUCCESS*/
#include <stdarg.h>
#include <typedef.h>
/*  
 * **********************************************************************************************
 *
 * ===========================[ End include header file ]============================
 *
 * *********************************************************************************************/


/**
 * \def __main_version__
 * \brief Establecemos la Version para el main.
 * \li + 0, 
 * \li + 1, 
 * \li + 2,
 * \li + 3,  
 * \li + 4,
 * \li + 5, 
 * \li + 6, 
 * \li + 7, 
 * \li + 8, 
 * \li + 9, 
 * TODO: __main_version__
 */
#ifndef __main_version__

#define __main_version__    6
#endif
/*-- Incluimos el header file del modulo principal, para que tome los cambios
 * de la definicion del label __main_version__ */
#include <main.h>



/* 
 *
 * 
 * *********************************************************************************************
 *
 * ===========================[ Begin main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */
#if (__main_version__ == 0)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 0    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 0
*/
#include <complejo.h>
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
#if 0  
    int i = 0;
    /*--
     * Imprimimos las lista de arguementos pasada
     */
    while(i<argn)
    {
        printf(SET_COLOR(FONT,RED)"\tARG[%d] \t %s\n"
        SET_COLOR(FONT,RESET),i,arg[i]);
        i++;        
    }    
    PRINT_SIZE_LEYEND();
    PRINT_SIZE(char );    
    PRINT_SIZE(short int );    
    PRINT_SIZE(int );
    PRINT_SIZE(long int );
    PRINT_SIZE(long long int );
    PRINT_SIZE(float );
    PRINT_SIZE(double );
    PRINT_SIZE(long double );       
    printf("\t El Numero aleatorio de %u\n",get_random(5));
    for (i = 0 ; i < 10 ; i++)
    {
        printf("\tEl factorial de %u = %u\n",i,factorial(i));   
        printf("\tEl Numero aleatorio de %u = %u\n",i,get_random(0));   
    }
    exit(EXIT_SUCCESS);
#else
  complejo_sT *a,*b,*r;
  a = complejo_new("1.522<j25.0"); /*debemo corregir la conversion */
  b = complejo_new("-1.522-j25.0");
  r = complejo_clone(a);
  printf("\t a = (%s) \t r = (%s) \n",complejo_getstr(a), complejo_getstr(r));
  
  
  /* Suma */
  printf("\t (%s) + (%s) = %s \n",complejo_getstr(a),\
        complejo_getstr(b),complejo_getstr(complejo_add(r,a,b)));
  /* Resta */
  printf("\t (%s) - (%s) = %s \n",complejo_getstr(a),\
        complejo_getstr(b),complejo_getstr(complejo_sub(r,a,b)));
  /* Multiplicacion */
  printf("\t (%s) * (%s) = %s \n",complejo_getstr(a),\
        complejo_getstr(b),complejo_getstr(complejo_mul(r,a,b)));
  /* Division */
  printf("\t (%s) / (%s) = %s \n",complejo_getstr(a),\
        complejo_getstr(b),complejo_getstr(complejo_div(r,a,b)));
  
  /*-- Liberamos la memoria */
  complejo_free(a,TRUE);
  complejo_free(b,TRUE);
  complejo_free(r,TRUE);
#endif
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 0      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 1)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 1    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 1
*/
#include <string.h>
#include <complejo.h>

/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
  
  complejo_sT a;
  a.real = 5.4;
  a.img = 6.6;
  a.form = complejo_RECT ;
  printf("El numero complejo es: %s\n",complejo_getstr(&a));
  char num[] = "45.656 +j 8999.9887";
/**/
  char * bcopy, *rp ,*rps;
  bcopy = strdup(num); 
  rp = strtok(bcopy,"+j");
  printf("num = %s \t rp = %s\t bcopy = %s\n",num,rp,bcopy);
  free(bcopy);
  /**/
  char *pnreal ,*pnimg, *premaind;
  bcopy = strdup(num); 
  pnreal = strtok_r(bcopy," +j ",&premaind);
  pnimg = strtok(premaind," +j ");
  printf("Numero = %s \t Real = %s \t Imaginario = %s\t reamind = %s\n",num,pnreal,pnimg,premaind);
  free(bcopy);
  //
  pnimg = strtok(pnimg," +j ");
  printf("Numero = %s \t Real = %s \t Imaginario = %s\n",num,pnreal,pnimg);
  free(bcopy);
  /*-- realizamos el test de las funciones */
  
  complejo_set(&a,"45.656 -j 8999.9887");
//   printf("El numero complejo es: %s\n",complejo_getstr(&a));
  exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 1      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 2)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 2    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 2
*/
#include <string.h>
#include <complejo.h>

/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
  complejo_sT a;
  a.real = 5.4;
  a.img = 6.6;
  a.form = complejo_RECT ;
  printf("El numero complejo es: %s\n",complejo_getstr(&a));
  char num[] = "45.656 +j 8999.9887";
/**/
  char * bcopy, *pnreal ,*pnimg;       
/**/
  bcopy = strdup(num); 
  pnreal = stdlib_str_findtoken(bcopy,"+j",&pnimg);  
  bcopy[pnreal-bcopy] = '\0';
  printf("num = %s \t Real = %s \t Imaginario = %s \n"\
         ,num,bcopy,pnimg);  
  free(bcopy);
  /**/
  
  bcopy = strdup(num); 
  pnreal = stdlib_str_findtoken(bcopy," +j ",&pnimg);  
  bcopy[pnreal-bcopy] = '\0';
  printf("num = %s \t Real = %s \t Imaginario = %s \n"\
         ,num,bcopy,pnimg); 
  free(bcopy); 
  
  exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 2      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 3)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 3    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 3
*/
#include <string.h>
#include <complejo.h>


const char *num[] = {
  "45.656 +j 8999.9887",
  "45.656 -j 8999.9887",
  "45.656+j8999.9887",
  "45.656-j8999.9887",
  "45.656 < 99.9887",
  "45.656<8999.9887",
  "456568999.9887",
  "45.656_8999.9887",
  "45.656.8999.9887",
  "45.656-j",
  "-j8999.9887",
  "+j",
  "45.656 < 99.9887 rad",
  "45.656<8999.9887rad ",
  "45.656<8999.9887°",
  "45.656<8999.9887 [rad]",
  "45.656<8999.9887[Rad",
  NULL };
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
  size_t i = 0;
  complejo_sT a;
  /*
  a.real = 5.4;
  a.img = 6.6;
  a.form = complejo_RECT ;*/
  complejo_set_withvalue(&a,5.4,6.6,complejo_RECT);
  printf("El numero complejo es: %s\n",complejo_getstr(&a));
  /*
   */
  printf("El numero complejo es: %s\n",complejo_getstr(complejo_set_withvalue(&a,5.4,6.6,complejo_POLAR_rad)));
  
  /*-- realizamos el test de las funciones */
  while(num[i] != NULL)
  {    
    if(complejo_set_withstr(&a,num[i] ) == NULL)
    {
      printf("El numero num[%ld] = %s ,no es complejo\n",i,num[i]);
      i++;
      continue;
    }
    printf("num[%ld] = %s \t Complejo = %s \n",i,num[i],complejo_getstr(&a));
    i++;    
  }  
  exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 3      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 4)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 4    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 4
*/
#include <string.h>
#include <complejo.h>
/*
 * 
 */
const char *num[] = {
  "45.656 +j 89.9887",
  "45.656 -j 89.9887",
  "-45.656-j89.9887",
  "45.656-j89.9887",
  "45.656 < -99.9887",
  "45.656<3.1[rad]",
  "499.9887",
  "45.656_8999.9887",
  "45.656.8999.9887",
  "45.656-j",
  "-j89.9887",
  "+j",
  "45.656 < 2.9887 rad",
  "45.656<0.9887rad ",
  "45.656<299.9887°",
  "45.656<1.89 [rad]",
  "45.656<5.9887[Rad",
  NULL };


/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/  
int main(int argn,char **arg)
{   
  size_t i = 0;
#if 0
  complejo_sT a;
  /* bucle de test */
  while(num[i] != NULL)
  {    
    if(complejo_set_withstr(&a,num[i] ) == NULL)
    {
      /* printf("El numero num[%ld] = %s ,no es complejo\n",i,num[i]);*/
      puts("");
      i++;
      continue;
    }
    printf("num[%ld] = %s \t Complejo = %s \n",i,num[i],complejo_getstr(&a));    
    complejo_change(&a,complejo_POLAR);
    printf("\t\t\t complejo_POLAR \t Complejo = %s \n",complejo_getstr(&a));
    complejo_change(&a,complejo_POLAR_rad);
    printf("\t\t\t complejo_POLAR_rad \t Complejo = %s \n",complejo_getstr(&a));
    complejo_change(&a,complejo_RECT);
    printf("\t\t\t complejo_RECT \t Complejo = %s \n",complejo_getstr(&a));    
    i++;    
  } 
#else
  complejo_sT *a;
  a = complejo_new("12.03<45.00°"/*NULL*/);
  printf("\t a = %s \n",complejo_getstr(a));
  /* bucle de test */
  while(num[i] != NULL)
  {    
    if(complejo_set_withstr(a,num[i] ) == NULL)
    {
      /* printf("El numero num[%ld] = %s ,no es complejo\n",i,num[i]); */
      puts("");
      i++;
      continue;
    }
    #if 0
    printf("num[%ld] = %s \t Complejo = %s \n",i,num[i],complejo_getstr(a));    
    complejo_change(a,complejo_POLAR);
    printf("\t\t\t complejo_POLAR \t Complejo = %s \n",complejo_getstr(a));
    complejo_change(a,complejo_POLAR_rad);
    printf("\t\t\t complejo_POLAR_rad \t Complejo = %s \n",complejo_getstr(a));
    complejo_change(a,complejo_RECT);
    printf("\t\t\t complejo_RECT \t Complejo = %s \n",complejo_getstr(a)); 
    #else
    printf("num[%ld] = %s \t Complejo = %s \n",i,num[i],complejo_getstr(a));    
    printf("\t\t\t complejo_POLAR \t Complejo = %s \n",\
            complejo_getstr(complejo_change(a,complejo_POLAR)));
    printf("\t\t\t complejo_POLAR_rad \t Complejo = %s \n",\
            complejo_getstr(complejo_change(a,complejo_POLAR_rad)));
    printf("\t\t\t complejo_RECT \t Complejo = %s \n",\
            complejo_getstr(complejo_change(a,complejo_RECT))); 
    #endif
   
    i++;    
  } 
#endif
  /**/ 
  exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 4      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 5)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 5    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 5
*/
#include <complejo.h>
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
  complejo_sT a,b;
  complejo_sT *r;
  /*-- Inicializamos los numeros Complejos */
  complejo_set_withstr(&a,"1.522+j25.0");
  complejo_set_withstr(&b,"-1.522-j25.0");
  
#if 0  
  /*-- test de la suma */
  r = complejo_add(NULL,&a,&b);  
  printf("Suma: ( %s ) + ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));
  complejo_free(r,TRUE);  
  /*-- test de la Resta */
  r = complejo_sub(NULL,&a,&b);  
  printf("Resta: ( %s ) - ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));
  complejo_free(r,TRUE);   
  /*-- test de la Multimplicacion */
  r = complejo_mul(NULL,&a,&b);  
  printf("Multi: ( %s ) * ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));
  complejo_free(r,TRUE); 
  /*-- test de la Division */
  r = complejo_div(NULL,&a,&b);  
  printf("Divi: ( %s ) / ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));
  complejo_free(r,TRUE); 
#else
  /*-- test de la suma */
  r = complejo_add(NULL,&a,&b);  
  printf("Suma: ( %s ) + ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));
  /*-- test de la Resta */
  r = complejo_sub(r,&a,&b);  
  printf("Resta: ( %s ) - ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));  
  /*-- test de la Multimplicacion */
  r = complejo_mul(r,&a,&b);  
  printf("Multi: ( %s ) * ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));   
  /*-- test de la Division */
  r = complejo_div(r,&a,&b);  
  printf("Divi: ( %s ) / ( %s ) = %s\n",\
    complejo_getstr(&a),complejo_getstr(&b),complejo_getstr(r));
  /* */
  complejo_free(r,TRUE); 
#endif  
  exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 5      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 6)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 6    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 6
*/
#include <dynamic_memory.h>
#include <complejo.h>


#define menu_operacion_END  {NULL,0,NULL}
typedef struct 
{
  const char *op1;
  const char operacion;
  const char *op2;
} menu_operacion_sT;



menu_operacion_sT list_operaciones[] = {
  {"1.2+j32.5",'+',"2.2-j5"},
  {"2.2+j7.8",'+',"1.1<45.0"},
  {"5<53",'+',"7.8+j4.5"},
  {"-j",'+',"12"},
  {"2.5+j2",'-',"4.5-j8"},
  {"8.5+j8",'-',"9.9<8.7rad"},
  {"1.2<60",'-',"9.8"},
  {"12",'-',"+j"},
  {"1.2+j32.5",'*',"2.2-j5"},
  {"2.2+j7.8",'/',"1.1<45.0"},
  {"5<53",'*',"7.8+j4.5"},
  {"-j",'/',"12"},
  {"2.5+j2",'/',"4.5-j8"},
  {"8.5+j8",'*',"9.9<8.7rad"},
  {"1.2<60",'*',"9.8"},
  {"12",'/',"+j"},
  menu_operacion_END
};

/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
  size_t i = 0;  
#if 0
  complejo_sT a,b;
  complejo_sT *r;
  /*-- */
  r = complejo_new(NULL);
  while(list_operaciones[i].op1 != NULL  )
  {
    /*-- Inicializamos los numeros Complejos */
    complejo_set_withstr(&a,list_operaciones[i].op1);
    complejo_set_withstr(&b,list_operaciones[i].op2);    
    r = complejo_operation(r,&a,&b,list_operaciones[i].operacion);
    printf("\t( %s ) \t %c \t ( %s ) \t = %s\n",\
      complejo_getstr(&a),list_operaciones[i].operacion,complejo_getstr(&b),\
      complejo_getstr(r));
    i++;
  }
  /*-- Liberamos el numero complejo */  
  complejo_free(r,TRUE);  
#else
  complejo_sT *a,*b;
  complejo_sT *r;
  char operacion;
  /*-- */
  r = complejo_new(NULL);
  a = complejo_new(NULL);
  b = complejo_new(NULL);
  while(list_operaciones[i].op1 != NULL  )
  {
    operacion = list_operaciones[i].operacion;
    /*-- Inicializamos los numeros Complejos */
    r = complejo_operation(r,\
        complejo_set_withstr(a,list_operaciones[i].op1),\
        complejo_set_withstr(b,list_operaciones[i].op2),\
        operacion);
    complejo_getstr(a);
    complejo_getstr(b);
    complejo_getstr(r);
    printf("\t( %s ) \t %c \t ( %s ) \t = %s\n",\
        a->str,operacion,b->str,r->str );
    i++;
  }
  /*-- Liberamos el numero complejo */  
  complejo_free(r,TRUE);  
#endif
  //
  exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 6      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 7)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 7    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 7
*/
#include <complejo.h>
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{
  complejo_sT *a,*b,*r1,*r2,*r3;
  a = complejo_new("1.522<25.0"); /*debemo corregir la conversion */
  b = complejo_new("-1.522-j25.0");
  printf("\t a = (%s) \t b = (%s) \n",complejo_getstr(a),complejo_getstr(b));
  /*test de clone*/
  r1 = complejo_clone(a);  
  printf("\t r = clone(a) \t a = (%s) \t r = (%s) \n",\
          complejo_getstr(a), complejo_getstr(r1));  
  /*test de inv*/
  r2 = complejo_cinv(a);  
  printf("\t r = inv(a) \t a = (%s) \t r = (%s) \n",\
          complejo_getstr(a), complejo_getstr(r2));
  r3 = complejo_cinv(b);  
  printf("\t r = inv(b) \t b = (%s) \t r = (%s) \n",\
          complejo_getstr(b), complejo_getstr(r3));  
  /*-- */
  complejo_free(r1,TRUE);
  complejo_free(r2,TRUE);
  complejo_free(r3,TRUE);
  /* */
  /*test de conjugado */
  r1 = complejo_cconj(a);  
  printf("\t r = conj(a) \t a = (%s) \t r = (%s) \n",\
          complejo_getstr(a), complejo_getstr(r1));
  r2 = complejo_cconj(b);  
  printf("\t r = conj(b) \t b = (%s) \t r = (%s) \n",\
          complejo_getstr(b), complejo_getstr(r2)); 
  /*-- */
  complejo_free(r1,TRUE);
  complejo_free(r2,TRUE);
   /*test de negativa */
  r1 = complejo_cneg(a);  
  printf("\t r = neg(a) \t a = (%s) \t r = (%s) \n",\
          complejo_getstr(a), complejo_getstr(r1));
  r2 = complejo_cneg(b);  
  printf("\t r = neg(b) \t b = (%s) \t r = (%s) \n",\
          complejo_getstr(b), complejo_getstr(r2));
  /**/
  complejo_free(r1,TRUE);
  complejo_free(r2,TRUE);
  /**/
  puts("\n\tSin Clone");
  /*test de inv*/  
  printf("\t r = inv(a) \t a = (%s)",complejo_getstr(a));
  printf("\t r = (%s) \n",complejo_getstr(complejo_inv(a)));    
  printf("\t r = inv(b) \t b = (%s)",complejo_getstr(b));
  printf("\t r = (%s) \n",complejo_getstr(complejo_inv(b))); 
  /*test de conjugado */  
  printf("\t r = conj(a) \t a = (%s)",complejo_getstr(a));
  printf("\t r = (%s) \n",complejo_getstr(complejo_conj(a)));    
  printf("\t r = conj(b) \t b = (%s)",complejo_getstr(b));
  printf("\t r = (%s) \n",complejo_getstr(complejo_conj(b)));
  /*test de negativa */
  printf("\t r = neg(a) \t a = (%s)",complejo_getstr(a));
  printf("\t r = (%s) \n",complejo_getstr(complejo_neg(a)));    
  printf("\t r = neg(b) \t b = (%s)",complejo_getstr(b));
  printf("\t r = (%s) \n",complejo_getstr(complejo_neg(b)));
  /*-- Liberamos la memoria */
  complejo_free(a,TRUE);
  complejo_free(b,TRUE);
  exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 7      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 8)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 8    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 8
*/
#include <complejo.h>

/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
  complejo_sT *r1,*r2,*r3,*r4,*rt;
  /**/
  r1 = complejo_new("1.522<25.0"); 
  r2 = complejo_new("-4.522<55.0"); /* XXX: debemos asegurarnos que un modulo 
  nunca puede ser negativo, corregir esto FIXME, Corregido falta realizar el TEST.*/
  r3 = complejo_new("-1.522-j25.0");
  r4 = complejo_new("-1.522+j25.0");
  
  /*-- Mostramos los valores */
  printf("\t r1 = (%s) \n",complejo_getstr(r1)); 
  printf("\t r2 = (%s) \n",complejo_getstr(r2)); 
  printf("\t r3 = (%s) \n",complejo_getstr(r3)); 
  printf("\t r4 = (%s) \n",complejo_getstr(r4)); 
  /*-- realizamos la suma, test unicamente */
  rt = complejo_sumatoria(4,r1,r2,r3,r4);
  
  /*-- Mostramos los valores */
  puts("\n\t Mostramos la sumatoria de forma elegante");
  printf("\t r1 = (%s) \n",complejo_getstr(complejo_change(r1,complejo_RECT))); 
  printf("\t r2 = (%s) \n",complejo_getstr(complejo_change(r2,complejo_RECT))); 
  printf("\t r3 = (%s) \n",complejo_getstr(complejo_change(r3,complejo_RECT))); 
  printf("\t r4 = (%s) \n",complejo_getstr(complejo_change(r4,complejo_RECT)));
  printf("\t rt = (%s) \n",complejo_getstr(rt));
  
  
  
  /*-- Libreramos la memora reservada */
  complejo_free(r1,TRUE);
  complejo_free(r2,TRUE);
  complejo_free(r3,TRUE);
  complejo_free(r4,TRUE);
  complejo_free(rt,TRUE);
  exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 8      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 9)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 9    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 9
*/
#include <complejo.h>
#include <dynamic_memory.h>

const char * array_zi[] = {
  "45.656 +j 89.9887",
  "45.656 -j 89.9887",
  "-45.656-j89.9887",
  "45.656-j89.9887",
  "45.656 < -99.9887",
  "45.656<3.1[rad]",
  "499.9887",  
  "45.656-j",
  "-j89.9887",
  "+j",
  "45.656 < 2.9887 rad",
  "45.656<0.9887rad ",
  "45.656<299.9887°",
  "45.656<1.89 [rad]",
  "45.656<5.9887[Rad",
  NULL };
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
  size_t cant,i;
  /**/  
  cant = 0 ;
  while(cant++, array_zi[cant] != NULL)/* descartamos el primero, de otra forma debemo restar -1 */;
  printf("\n\t El numero de Operaciones son : %ld\n", cant);
  /**/
  complejo_sT **zi = XMALLOC(sizeof(complejo_sT *) * cant);
  
  /*-- Reservamos memoria para cada puntero del array de punteros */  
  for( i = 0; i< cant ; i++)
  {
    zi[i] = complejo_new(array_zi[i]);
  }
  /*-- Mostramos el contenido del Array */
  for( i = 0; i< cant ; i++)
  {
    printf("\t zi[%ld] = %s \n", i,complejo_getstr(zi[i]));
  }
  /*-- Probamos a sumatoria */
  complejo_sT *rt = complejo_sumatoria(15,zi[0],zi[1],zi[2],zi[3],zi[4],\
    zi[5],zi[6],zi[7],zi[8],zi[9],zi[10],zi[11],zi[12],zi[13],zi[14] );
  /*-- realizamos la sumatoria */
  printf("\t Sumatoria zi[%ld]= %s \n", cant,complejo_getstr(rt));
  puts("\n\tVisualizamos zi[] en formato rectangular\n");
  /*-- Mostramos el contenido del Array */
  for( i = 0; i< cant ; i++)
  {
    printf("\t zi[%ld] = %s \n", i,complejo_getstr(complejo_change(zi[i],complejo_RECT)));
  }
  
  /*-- debemos libear la memoria*/   
  for( i = 0; i< cant ; i++)
  {
    complejo_free(zi[i],TRUE);    
  }
  free(zi);
  /*-- realizamos otra operaciones */
  
  
  exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 9      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 10)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 10   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 10
*/
#include <complejo.h>
#include <dynamic_memory.h>

const char * array_zi[] = {
  "45.656 +j 89.9887",
  "45.656 -j 89.9887",
  "-45.656-j89.9887",
  "45.656-j89.9887",
  "45.656 < -99.9887",
  "45.656<3.1[rad]",
  "499.9887",  
  "45.656-j",
  "-j89.9887",
  "+j",
  "45.656 < 2.9887 rad",
  "45.656<0.9887rad ",
  "45.656<299.9887°",
  "45.656<1.89 [rad]",
  "45.656<5.9887[Rad",
  NULL };
  
const char * array_zj[] = {
  "45.656 < 89.9887 ",
  "45.656 < 89.9887",
  "-45.656<89.9887",
  "45.656<89.9887",
  "45.656 +j -99.9887",
  "45.656+j3.1",
  "499.9887<0[rad]",  
  "45.656<980[°]",
//   "<-89.9887[rad", /* son errores mateamticos */
//  -1 "<180", /* devemos contemplar a la hora de crear ??, podemos corregirlos 0.000000 y el otro mod() */
  "0.01<-89.9887[rad]",
  "-14<180",
  "45.656 +j 2.9887",
  "45.656+j0.9887",
  "45.656299<9887°",
  "45.656-j1.89 ",
  "45.656+j5.9887",
  NULL };
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
  complejo_sizearr_vT lenarr, i;
  /* creamos el array Zi */
  complejo_sT **zi = complejo_new_arr(array_zi,&lenarr);
  /*-- Mostramos el contenido del Array */
  puts("\nListado de Argumentos");
  for( i = 0; i< lenarr ; i++)
  {
    printf("\t zi[%d] = %s \n", i,complejo_getstr(zi[i]));
  }

  /*-- Probamos a sumatoria */
  complejo_sT *rt = complejo_sumatoria(15,zi[0],zi[1],zi[2],zi[3],zi[4],\
    zi[5],zi[6],zi[7],zi[8],zi[9],zi[10],zi[11],zi[12],zi[13],zi[14] );
  /*-- realizamos la sumatoria */
  printf("\t Sumatoria zi[%d]= %s \n", lenarr,complejo_getstr(rt));
  puts("\n\tVisualizamos zi[] en formato rectangular\n");
  /*-- Mostramos el contenido del Array */
  for( i = 0; i< lenarr ; i++)
  {
    printf("\t zi[%d] = %s \n", i,complejo_getstr(complejo_change(zi[i],complejo_RECT)));
  }
  
  complejo_free_arr(zi,lenarr);
  complejo_free(rt,TRUE);

  /**/
  puts("\n\t realizmos la segunda operacion \n");
  zi = complejo_new_arr(array_zj,&lenarr);
  puts("\n\t Los Nuevos Operandos:");
  /*-- Mostramos el contenido del Array */
  for( i = 0; i< lenarr ; i++)
  {
    printf("\t yi[%d] = %s \n", i,complejo_getstr(zi[i]));
  }
  /*-- Probamos a sumatoria inversa */
  rt = complejo_sumatoriainv(15,zi[0],zi[1],zi[2],zi[3],zi[4],\
    zi[5],zi[6],zi[7],zi[8],zi[9],zi[10],zi[11],zi[12],zi[13],zi[14] );
  /*-- realizamos la sumatoria */
  printf("\t Sumatoria Inversa zi[...] = %s \n",complejo_getstr(rt));
  
  puts("\n\tVisualizamos yi[] en formato polar [rad]\n");
  /*-- Mostramos el contenido del Array */
  for( i = 0; i< lenarr ; i++)
  {
    complejo_inv(zi[i]);
    complejo_change(zi[i],complejo_RECT);
    complejo_getstr(zi[i]);
    printf("\t zi[%d] = %s \n", i,zi[i]->str);
  }
  printf("\t Sumatoria Inversa yi[...] = %s \n",complejo_getstr(rt));
  /*-- Liberamos La memoria ocuapda */
  complejo_free_arr(zi,lenarr);  
  exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 10     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 11)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 11   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 11
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 11     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 12)

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 12   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 12
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 12     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 13)

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 13   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 13
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 13     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 14)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 14   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 14
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 14     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 15)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 15   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 15
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 15     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 16)

#warning "Version no definida aun"
#else /* #if (__main_version__ == 1) */
#warning "__main_version__ mal definido"
#endif /* #if (__main_version__ == 1) */
/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */



/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End APIs Definitions ]============================
 *
 * ******************************************************************************************** 
 * 
 * 
 */
