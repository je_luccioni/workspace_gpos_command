##
##
################################################################################
##
## Configuracion del project
##	>> APP_NAME : Establecemos el Nombre que tomara la APP
##	>> PRODUCTION : Definimos si pertenece al ambito de produccion
##			o testing.
##	>> DOC_FLAGS : Flags para la el que el comando crea la documentacion
##
################################################################################ 
## -- para depuracion esta establecido el siguente nombre
APP_NAME = template

##
##
## #############################################################################
## 
## Establecemso el listado de argumetos a pasarle a la aplicacion "for make run"
##
## #############################################################################
ARG ?= "msg como argumentos" "aaa"


DEBUG_ENABLE ?= TRUE

##
##
## #############################################################################
## 
## Definicion si la aplicacion se compila para Produccion o testing, larga 
##	Una series de seting segun el proyecto
##	>> TRUE
##	>> FALSE
##
## #############################################################################
PRODUCTION = FALSE


##
## #############################################################################
## 
## Definicion el S.O anfitrion, el ambiente de la aplicacion
##
##    __CYGWINCMD__ : 	consola de comando bajo windos CYGWIN
##    __LINUX__ : 	consola de comando Linux
##
## #############################################################################
SO_COMPILER ?= __LINUX__

##
##
######################################################################################################
##
## ---------------------------------------------------------------------------------------------------
## *************************[+ ESTABLECEMOS LOS MODULOS locales a USAR    +]**************************
## ---------------------------------------------------------------------------------------------------
##
## Listamos los modulos que utilizara el proyecto
##	Cada directorio debe tener el formato
##		<module>
##		    |
##		    +-> src
##		    +-> inc
## Ejemplo de uso: MODULES ?= mod0 mod1 mod2
######################################################################################################
MODULES ?= miscellaneous

###
###
######################################################################################################
##
## ---------------------------------------------------------------------------------------------------
## *****************************[+ CONFIGURACION DE LIBRERIAS EXTERNAS +]*****************************
## ---------------------------------------------------------------------------------------------------
##
######################################################################################################

##
##
################################################################################
##
## Listado de Nombre de librerias Externas, debemos recordar que 
##	libpq.a -> le corresponde el nombre pq
##	libm.a -> le corresponde el nombre m
##	for link-time
##
################################################################################ 
LIB_NAME := m


##
##
################################################################################
##
## Listado de la Ruta donde estan las Librerias, algunas estan definida
## por defecto para gcc, como por ejemplo p/math.h
##
################################################################################ 
#LIB_PATH := /usr/include/postgresql
## definimos los flags de pq (psql) para producion, 
ifeq ($(PRODUCTION),TRUE)
LIB_PATH := /usr/lib/ 
else
LIB_PATH := /usr/lib/x86_64-linux-gnu/
endif



##
##
################################################################################
##
## Listado de os headr file de las librearias externas que usaremos
##
##	>> Los Header Files para la libreria "libpq"
##		/usr/include/postgresql
##
##	>> Los Header Genericos 
##		/usr/include
##
################################################################################ 
LIB_INC_PATH_LIST = /usr/include
# LIB_INC_PATH_LIST += /usr/include/postgresql

#LIB_INC_PATH_LIST = /usr/include/x86_64-linux-gnu/sys 
#LIB_INC_PATH_LIST = /usr/include






###
###
######################################################################################################
##
## ---------------------------------------------------------------------------------------------------
## ***************************[+ CONFIGURACION lanzada por $(PRODUCTION) +]***************************
## ---------------------------------------------------------------------------------------------------
##
######################################################################################################
##
## #############################################################################
## 
## Definicion automaticamente la arquitectura del procesador
##
##    __M32__ : PRODUCTION = TRUE, para arquitecuta de 32-Bits 
##    __M64__ : PRODUCTION = FALSE, para arquitecuta de 64-Bits default
##
## #############################################################################
ifeq ($(PRODUCTION),TRUE)
ARCH ?= __M32__
else
ARCH ?= __M64__
endif


###
###
######################################################################################################
##
## ---------------------------------------------------------------------------------------------------
## ******************[+ Establecemos los FLAGS minimos necesario para el proyecto +]******************
## ---------------------------------------------------------------------------------------------------
##
######################################################################################################

##
##
################################################################################
##
## Listado de variables para editar los falgs con el llamado del make
## example :
## 		make new CFLAGS=<flags>
## CFLAGS : 
##		-g -DNDEBUG : enable "gdb" para "debug" de la Aplicacion  
##
################################################################################ 
ifeq ( $(DEBUG_ENABLE), TRUE)
## FIXME: Es automatico, debemos colocar en project.mk "DEBUG_ENABLE = TRUE"
#PROJECT_CCFLAGS = -g -DNDEBUG
else
# PROJECT_CCFLAGS = 
endif





##
##
################################################################################
##
## Establecemos Los flags para cada compilador
##
## 
## Flags para definir el tipo de CPU que ejecutara la aplicacion -m....
##    
##	-marc=native : Habilita todas el subconjunto de intrucciones soportadas
##                     por la maquina local.
##      -mtune=native : optimiza el codigo para la maquina local.
##      -m32 : Establecer la compilacion de codigo para arquitectura de 32-Bits
##	       Para esto sea valido en sistemas (S.O) 64-Bits debemos instalar 
##	       algunas librerias, tanto para gcc como para g++ (para c y c++)
##		>> sudo apt-get install gcc-multilib
##		>> sudo apt-get install g++-multilib
##
##      -m64 : Establecer la compilacion de codigo para arquitectura de 64-Bits
##
##
##         --ansi : para deshabilitar las caracreristicas de GCC que son 
##                  incompatibles con el estandar ISO C90, cuando compilamos
##                  codigo fuente en C. 
##
##         -Iinclude : para notificarle al compilador donde debe buscar los 
##                  header file para las librerias 
##
##
################################################################################ 
##
##--------------------------------------------------------------------------
# 	gcc, flags for compiler C
#  >> -Wall : Esta opcion activa todos los avisos más comunes "se
#             recomienda usar siempre esta opción".
#  >> --ansi : para deshabilitar las caracreristicas de GCC que son 
#              incompatibles con el estandar ISO C90, cuando compilamos
#              codigo fuente en C. 
#
#  >> -Iinclude : para notificarle al compilador donde debe buscar los 
#                 header file para las librerias, es optativo $(INCLUDE)
#  >> -w :
#  >> -g :
#  >> -c :
#
##--------------------------------------------------------------------------
#CCFLAGS := -w -g -c -Wall -m32
##CCFLAGS := -Iinclude -w -g 
ifeq ($(ARCH),__M32__)
##
# definimos los flags p/arquitectura de 32-bits
##
#CCFLAGS = -Iinclude -ansi -Wall -m32
#CCFLAGS = -ansi -Wall -m32 -Iinclude
PROJECT_CCFLAGS += -ansi -Wall -m32 -Iinclude
else ifeq ($(ARCH),__M64__)
##
# definimos los flags p/arquitectura de 64-bits
##
#CCFLAGS := -Iinclude -Wall -m64 -std=c99 -ansi -Wformat-extra-args
#CCFLAGS := -Iinclude -Wall -m64 -ansi 
#CCFLAGS = -Iinclude -g -w 
# PROJECT_CCFLAGS += -Wall -m64 -Iinclude -ansi 
PROJECT_CCFLAGS += -Wall -m64 -Iinclude 
else ifeq ($(ARCH),__Mhost__)
##
# La arquitectura es definida por la maquina anfitrion 
##
PROJECT_CCFLAGS += -ansi -Wall -march=native -mtune=native 
endif

##
##--------------------------------------------------------------------------
# 	gcc, flags for compiler C++
#  >> -w :
#  >> -g :
#  >> -c :
#  >> -Wall : mensaje de warning
#
##--------------------------------------------------------------------------
PROJECT_CCPPFLAGS =
##
##--------------------------------------------------------------------------
# 	gcc, flags for compiler ASM
#  >> -w :
#  >> -g :
#
##--------------------------------------------------------------------------
PROJECT_CASMFLAGS = -g -w


PROJECT_SYMBOLS = -D$(ARCH)

##
## FIXME: debemos cambiar a PROJECT_SYMBOLS
##
ifeq ($(PRODUCTION),FALSE)
PROJECT_SYMBOLS += -DREMOTO_CONNECT
## para test 
PROJECT_SYMBOLS += -DDEBUG_PRINT_DISABLE
else
PROJECT_SYMBOLS += -DDEBUG_PRINT_DISABLE 
endif
