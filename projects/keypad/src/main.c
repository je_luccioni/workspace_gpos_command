 /*
 * ******************************[ Source file main.c ]*************************************//**
 * \addtogroup main_c 
 * @{
 * @copyright 
 * ${date}, Luccioni Jesús Emanuel. \n
 * All rights reserved.\n
 * This file is part of port module.\n
 * Redistribution is not allowed on binary and source forms, with or without 
 * modification.\n
 * Use is permitted with prior authorization by the copyright holder.\n
 * \file main.c
 * \brief bloque de comenteario para documentacion para describir este archivo de 
 * cabecera o header file. 
 * \version v01.01
 * \date   ${date}
 * \note none
 * \author JEL, Jesus Emanuel Luccioni
 * \li piero.jel@gmail.com
 * @} doxygen end group definition
 *************************************************************************************/



/* **********************************************************************************************
 *
 * ===========================[ Begin include header file ]============================
 *
 * ********************************************************************************************** 
 * 
 */
#include <stdio.h> /* header file correspondiente a las libreria
estandar de entrada salida*/
#include <stdlib.h> /* header file correspondiente a las libreria
estandar para el uso de la funcion @ref exit() con sus 
macros @ref EXIT_FAIL y @ref EXIT_SUCCESS*/
#include <stdarg.h>
#include <typedef.h>
/*  
 * **********************************************************************************************
 *
 * ===========================[ End include header file ]============================
 *
 * *********************************************************************************************/


/**
 * \def __main_version__
 * \brief Establecemos la Version para el main.
 * \li + 0, 
 * \li + 1, 
 * \li + 2,
 * \li + 3,  
 * \li + 4,
 * \li + 5, 
 * \li + 6, 
 * \li + 7, 
 * \li + 8, 
 * \li + 9, 
 * TODO: __main_version__
 */
#ifndef __main_version__

#define __main_version__    1
#endif
/*-- Incluimos el header file del modulo principal, para que tome los cambios
 * de la definicion del label __main_version__ */
#include <main.h>



/* 
 *
 * 
 * *********************************************************************************************
 *
 * ===========================[ Begin main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */
#if (__main_version__ == 0)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 0    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 0
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
    int i = 0;
    /*--
     * Imprimimos las lista de arguementos pasada
     */
    while(i<argn)
    {
        printf(SET_COLOR(FONT,RED)"\tARG[%d] \t %s\n"
        SET_COLOR(FONT,RESET),i,arg[i]);
        i++;        
    }    
    PRINT_SIZE_LEYEND();
    PRINT_SIZE(char );    
    PRINT_SIZE(short int );    
    PRINT_SIZE(int );
    PRINT_SIZE(long int );
    PRINT_SIZE(long long int );
    PRINT_SIZE(float );
    PRINT_SIZE(double );
    PRINT_SIZE(long double );       
    printf("\t El Numero aleatorio de %u\n",get_random(5));
    for (i = 0 ; i < 10 ; i++)
    {
        printf("\tEl factorial de %u = %u\n",i,factorial(i));   
        printf("\tEl Numero aleatorio de %u = %u\n",i,get_random(0));   
    }
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 0      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 1)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 1    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 1
*/
#include <string.h>
/**
* \struct keypad_cfg_sT; 
* \brief Redefinicion de un tipo de estructura que contiene la 
* configuracion para manejar un teclado matricial.
*  \li \ref nrow;
*  \li \ref ncol;
*  \li \ref row;
*  \li \ref col;
*  \li \ref map;
* 
* \version 01v01d01.
* \note nota sobre la estructura.
* \warning mensaje de "warning". 
* \date Miercoles 28 De Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example 1
<PRE>
// ejemplo 1, declracion por separado
char* map_keypad [] = { 
  (char[]){'1','2','3','A'},\
  (char[]){'4','5','6','B'},\
  (char[]){'7','8','9','C'},\
  (char[]){'#','0','*','D'}};
  //
uint8_t pinRow[]={1,2,3,4};
uint8_t pinCol[]={11,23,22,20};
  // Asignamso los valores a la estructura
cfg_keymap1.nrow = 4, cfg_keymap1.ncol = 4;
cfg_keymap1.row = pinRow, cfg_keymap1.col = pinCol;
cfg_keymap1.map = (char**) map_keypad;
</PRE>
* \par example 2
<PRE>
// ejemplo 1, declarcion e inicializacion 
keypad_cfg_sT cfg_keymap2 = { 
  4,// 4- Filas
  3,// 3- Columnas
  (uint8_t[]){5,7,6,2},\ // array de Pines p/Filas
  (uint8_t[]){11,2,20},\ // array de Pines p/Columnas
  (char*[]) {(char[]){'9','8','7'},\
             (char[]){'6','5','4'},\
             (char[]){'3','2','1'},\
             (char[]){'*','0','#'}}  
};
</PRE>
*/
typedef struct
{
  uint8_t nrow;   /**<@brief Cantidad de filas de la matriz */
  uint8_t ncol;   /**<@brief Cantidad de Columnas de la matriz */
  uint8_t * row;  /**<@brief Array donde almacenamos los pines que manejan las Filas */
  uint8_t * col;  /**<@brief Array donde almacenamos los pines que manejan las Columnas */
  char ** map;     /**<@brief Matriz que contiene el mapa correspondiente al teclado matricial */
}keypad_cfg_sT;



char* map_keypad [] = { 
  (char[]){'1','2','3','A'},\
  (char[]){'4','5','6','B'},\
  (char[]){'7','8','9','C'},\
  (char[]){'#','0','*','D'}};

uint8_t pinRow[]={1,2,3,4};
uint8_t pinCol[]={11,23,22,20};

// keypad_cfg_sT cfg_keymap1 = {4,4,(uint8_t*) pinRow,(uint8_t*)pinCol,(char**)map_keypad};
keypad_cfg_sT cfg_keymap1;
/* Inicializando array que compone una estructura de datos.  */
keypad_cfg_sT cfg_keymap2 = { 
  4,
  3,
  (uint8_t[]){5,7,6,2},\
  (uint8_t[]){11,23,20},\
  (char*[]) {(char[]){'9','8','7'},\
             (char[]){'6','5','4'},\
             (char[]){'3','2','1'},\
             (char[]){'*','0','#'}}  
};


/**
* ****************************************************************************//** 
* \fn void load_config(keypad_cfg_sT * pcfg);
* \brief Funcion para simular la configuracion de un keymap, haciendo uso de
* la estructura \ref keypad_cfg_sT.
* \param pcfg : Puntero a la estructura que contiene los datos necesarios para 
* la configuracion del keypad.
* \version 01v01d01.
* \note nota sobre la estructura.
* \warning mensaje de "warning". 
* \date Miercoles 28 De Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
keypad_cfg_sT cfg_keymapX = { 
  4,// 4- Filas
  3,// 3- Columnas
  (uint8_t[]){5,7,6,2},\ // array de Pines p/Filas
  (uint8_t[]){11,2,20},\ // array de Pines p/Columnas
  (char*[]) {(char[]){'9','8','7'},\
             (char[]){'6','5','4'},\
             (char[]){'3','2','1'},\
             (char[]){'*','0','#'}}  
};
  // -- 
  load_config(&cfg_keymapX);
</PRE>  
*********************************************************************************/
void load_config(keypad_cfg_sT * pcfg);

/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
//   const keypad_cfg_sT cfg_keymap={{1,2,3,4},{10,11,12,13},map_keypad  };
  /*
   * Nos paso mas de un arguemnto 
   * El 0 es el path/nombre del ejecutable
  */
  cfg_keymap1.nrow = 4, cfg_keymap1.ncol = 4;
  cfg_keymap1.row = pinRow, cfg_keymap1.col = pinCol;
  cfg_keymap1.map = (char**) map_keypad;
  load_config(&cfg_keymap1);
  load_config(&cfg_keymap2);
  
  exit(EXIT_SUCCESS);
}


void load_config(keypad_cfg_sT * pcfg)
{
  uint8_t i,j;
  if(pcfg == NULL) return;
  /**/
  puts("Pines de las Filas: ");
  for(i=0; i<pcfg->nrow ; i++)
  {    
      printf("\tPin ROW[%d] = %d\t",i,*((pcfg->row)+i) );
  }
  puts("\nPines de las Columnas: ");
  for(i=0; i<pcfg->ncol ; i++)
  {    
      printf("\tPin COL[%d] = %d\t",i,*((pcfg->col)+i) );
  }
  puts("\n");
  for(i=0; i<pcfg->nrow ; i++)
  {
    for(j=0; j<pcfg->ncol ; j++ )
    {
      printf("keypad[%d][%d] = %c\t",i,j,*((*pcfg->map)+j+(i*pcfg->ncol)));    
    }
    puts("");
      
  }
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 1      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 2)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 2    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 2
*/
#include <string.h>    
    
/**
* \struct keypad_cfg_sT; 
* \brief Redefinicion de un tipo de estructura que contiene la 
* configuracion para manejar un teclado matricial.
*  \li \ref nrow;
*  \li \ref ncol;
*  \li \ref row;
*  \li \ref col;
*  \li \ref map;
* 
* \version 01v01d01.
* \note nota sobre la estructura.
* \warning mensaje de "warning". 
* \date Miercoles 28 De Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example 1
<PRE>
// ejemplo 1, declracion por separado
char* map_keypad [] = { 
  (char[]){'1','2','3','A'},\
  (char[]){'4','5','6','B'},\
  (char[]){'7','8','9','C'},\
  (char[]){'#','0','*','D'}};
  //
uint8_t pinRow[]={1,2,3,4};
uint8_t pinCol[]={11,23,22,20};
  // Asignamso los valores a la estructura
cfg_keymap1.nrow = 4, cfg_keymap1.ncol = 4;
cfg_keymap1.row = pinRow, cfg_keymap1.col = pinCol;
cfg_keymap1.map = (char**) map_keypad;
</PRE>
* \par example 2
<PRE>
// ejemplo 1, declarcion e inicializacion 
keypad_cfg_sT cfg_keymap2 = { 
  4,// 4- Filas
  3,// 3- Columnas
  (uint8_t[]){5,7,6,2},\ // array de Pines p/Filas
  (uint8_t[]){11,2,20},\ // array de Pines p/Columnas
  (char*[]) {(char[]){'9','8','7'},\
             (char[]){'6','5','4'},\
             (char[]){'3','2','1'},\
             (char[]){'*','0','#'}}  
};
</PRE>
*/
typedef struct
{
  uint8_t nrow;   /**<@brief Cantidad de filas de la matriz */
  uint8_t ncol;   /**<@brief Cantidad de Columnas de la matriz */
  uint8_t * row;  /**<@brief Array donde almacenamos los pines que manejan las Filas */
  uint8_t * col;  /**<@brief Array donde almacenamos los pines que manejan las Columnas */
  char ** map;     /**<@brief Matriz que contiene el mapa correspondiente al teclado matricial */
}keypad_cfg_sT;


/* Macro para Obtener el dato del mapa mediante la 
 * estructura de configuracion y la coordenadas de
 *  Fila y Columna.
 */
#define keypad_GET_DATA_FROM_MAP(pCfgKey,Row,Col) \
    *((*pCfgKey->map)+Col+(Row*pCfgKey->ncol))
/*-- */
#define keypad_GET_PIN_ROW(pCfgKey,Idx)   *((pCfgKey->row)+Idx)
#define keypad_GET_PIN_COL(pCfgKey,Idx)   *((pCfgKey->col)+Idx)

#define keypad_INSERT_MAPROW(...) (char[]){__VA_ARGS__}
#define keypad_INSERT_KEYMAP(...) (char*[]){__VA_ARGS__}
#define keypad_INSERT_PINARRAY(...) (uint8_t[]){__VA_ARGS__}
    
#define keypad_CREATE_PINARRAY(Name, ...) uint8_t Name[] = {__VA_ARGS__}
#define keypad_CREATE_KEYMAP(Name, ...) char* Name [] = {__VA_ARGS__}

    
    
/*
char* map_keypad [] ={
  keypad_INSERT_MAPROW('1','2','3','A'),
  keypad_INSERT_MAPROW('4','5','6','B'),
  keypad_INSERT_MAPROW('7','8','9','C'),
  keypad_INSERT_MAPROW('*','0','#','D'),
};*/

keypad_CREATE_KEYMAP(map_keypad,\
                      keypad_INSERT_MAPROW('1','2','3','A'),\
                      keypad_INSERT_MAPROW('4','5','6','B'),\
                      keypad_INSERT_MAPROW('7','8','9','C'),\
                      keypad_INSERT_MAPROW('*','0','#','D') \
                    );

keypad_CREATE_PINARRAY(pinRow,1,2,3,4);
keypad_CREATE_PINARRAY(pinCol,11,23,22,20);


keypad_cfg_sT cfg_keymap1;
/* Inicializando array que compone una estructura de datos.  */
keypad_cfg_sT cfg_keymap2 = { 
  4,
  3,
  keypad_INSERT_PINARRAY(5,7,6,2),\
  keypad_INSERT_PINARRAY(11,23,20),\
  keypad_INSERT_KEYMAP( keypad_INSERT_MAPROW('9','8','7'),\
                        keypad_INSERT_MAPROW('6','5','4'),\
                        keypad_INSERT_MAPROW('3','2','1'),\
                        keypad_INSERT_MAPROW('*','0','#') /*end keymap*/) 
};


/**
* ****************************************************************************//** 
* \fn void load_config(keypad_cfg_sT * pcfg);
* \brief Funcion para simular la configuracion de un keymap, haciendo uso de
* la estructura \ref keypad_cfg_sT.
* \param pcfg : Puntero a la estructura que contiene los datos necesarios para 
* la configuracion del keypad.
* \version 01v01d01.
* \note nota sobre la estructura.
* \warning mensaje de "warning". 
* \date Miercoles 28 De Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
keypad_cfg_sT cfg_keymapX = { 
  4,// 4- Filas
  3,// 3- Columnas
  (uint8_t[]){5,7,6,2},\ // array de Pines p/Filas
  (uint8_t[]){11,2,20},\ // array de Pines p/Columnas
  (char*[]) {(char[]){'9','8','7'},\
             (char[]){'6','5','4'},\
             (char[]){'3','2','1'},\
             (char[]){'*','0','#'}}  
};
  // -- 
  load_config(&cfg_keymapX);
</PRE>  
*********************************************************************************/
void load_config(keypad_cfg_sT * pcfg);

/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
//   const keypad_cfg_sT cfg_keymap={{1,2,3,4},{10,11,12,13},map_keypad  };
  /*
   * Nos paso mas de un arguemnto 
   * El 0 es el path/nombre del ejecutable
  */
  cfg_keymap1.nrow = 4, cfg_keymap1.ncol = 4;
  cfg_keymap1.row = pinRow, cfg_keymap1.col = pinCol;
  cfg_keymap1.map = (char**) map_keypad;
  load_config(&cfg_keymap1);
  load_config(&cfg_keymap2);
  
  exit(EXIT_SUCCESS);
}


void load_config(keypad_cfg_sT * pcfg)
{
  uint8_t i,j;
  if(pcfg == NULL) return;
  /**/
  puts("Pines de las Filas: ");
  for(i=0; i<pcfg->nrow ; i++)
  {    
//       printf("\tPin ROW[%d] = %d\t",i,*((pcfg->row)+i) );
      printf("\tPin ROW[%d] = %d\t",i, keypad_GET_PIN_ROW(pcfg,i));
  }
  puts("\nPines de las Columnas: ");
  for(i=0; i<pcfg->ncol ; i++)
  {    
//       printf("\tPin COL[%d] = %d\t",i,*((pcfg->col)+i) );
      printf("\tPin COL[%d] = %d\t",i, keypad_GET_PIN_COL(pcfg,i));
  }
  puts("");
  for(i=0; i<pcfg->nrow ; i++)
  {
    for(j=0; j<pcfg->ncol ; j++ )
    {      
//       printf("keypad[%d][%d] = %c\t",i,j,keypad_GET_DATA_FROM_MAP(pcfg,i,j));
      printf("keypad[%d][%d] = %c\t",i,j,pcfg->map[i][j]);
      
    }
    puts("");      
  }
  puts("\n\n");
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 2      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 3)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 3    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 3
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
        
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 3      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 4)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 4    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 4
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   

    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 4      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 5)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 5    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 5
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
     
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 5      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 6)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 6    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 6
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
      
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 6      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 7)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 7    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 7
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
   
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 7      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 8)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 8    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 8
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 8      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 9)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 9    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 9
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 9      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 10)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 10   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 10
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 10     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 11)


#warning "Version no definida aun"
#else /* #if (__main_version__ == 1) */
#warning "__main_version__ mal definido"
#endif /* #if (__main_version__ == 1) */
/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */



/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End APIs Definitions ]============================
 *
 * ******************************************************************************************** 
 * 
 * 
 */
