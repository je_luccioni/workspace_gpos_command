 /*
 * ******************************[ Source file main.c ]*************************************//**
 * \addtogroup template
 * @{
 * @copyright 
 * 2016, Luccioni Jesús Emanuel. \n
 * All rights reserved.\n
 * This file is part of port module.\n
 * Redistribution is not allowed on binary and source forms, with or without 
 * modification.\n
 * Use is permitted with prior authorization by the copyright holder.\n
 * \file main.c
 * \brief bloque de comenteario para documentacion para describir este archivo de 
 * cabecera o header file. 
 * \version v01.01
 * \date 20 Diciembre 2019
 * \note none
 * \author JEL, Jesus Emanuel Luccioni
 * \li piero.jel@gmail.com
 * @} doxygen end group definition
 *************************************************************************************/



/* **********************************************************************************************
 *
 * ===========================[ Begin include header file ]============================
 *
 * ********************************************************************************************** 
 * 
 */
#include <stdio.h> /* header file correspondiente a las libreria
estandar de entrada salida*/
#include <stdlib.h> /* header file correspondiente a las libreria
estandar para el uso de la funcion @ref exit() con sus 
macros @ref EXIT_FAIL y @ref EXIT_SUCCESS*/
#include <stdarg.h>
#include <typedef.h>
/*  
 * **********************************************************************************************
 *
 * ===========================[ End include header file ]============================
 *
 * *********************************************************************************************/



#ifndef __main_version__
/**
 * \def __main_version__
 * \brief Establecemos la Version para el main.
 * \li + 0, 
 * \li + 1, 
 * \li + 2,
 * \li + 3,  
 * \li + 4,
 * \li + 5, 
 * \li + 6, 
 * \li + 7, 
 * \li + 8, 
 * \li + 9, 
 * TODO: __main_version__
 */
#define __main_version__    1
#endif
/*-- Incluimos el header file del modulo principal, para que tome los cambios
 * de la definicion del label __main_version__ */
#include <main.h>



/* 
 *
 * 
 * *********************************************************************************************
 *
 * ===========================[ Begin main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */
#if (__main_version__ == 0)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 0    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 0
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
    int i = 0;
    /*--
     * Imprimimos las lista de arguementos pasada
     */
    while(i<argn)
    {
        printf(SET_COLOR(FONT,RED)"\tARG[%d] \t %s\n"
        SET_COLOR(FONT,RESET),i,arg[i]);
        i++;        
    }    
    PRINT_SIZE_LEYEND();
    PRINT_SIZE(char );    
    PRINT_SIZE(short int );    
    PRINT_SIZE(int );
    PRINT_SIZE(long int );
    PRINT_SIZE(long long int );
    PRINT_SIZE(float );
    PRINT_SIZE(double );
    PRINT_SIZE(long double );       
    printf("\t El Numero aleatorio de %u\n",get_random(5));
    for (i = 0 ; i < 10 ; i++)
    {
        printf("\tEl factorial de %u = %u\n",i,factorial(i));   
        printf("\tEl Numero aleatorio de %u = %u\n",i,get_random(0));   
    }
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 0      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 1)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 1    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 1
*/
#include <string.h>

/**
 * \var menu_opt;
 * \brief      Estructura que contiene la definiciones del comando.
 * Opcion corta, larga,leyenda/msg para cada una de las opciones.
 * \version    numero de version/es a la cual corresponde.
 * \warning mensaje de "warning".
 */
menu_sT menu_opt[] = \
{
	{"HELP","-h","--help",leyend_help,option_help},
	{"TEMPLATE","-t","--template",leyend_template,option_template},
	{NULL}
};
/*
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{    
    int i;   
	fx_optiont_pfT pfxOption;
	/*-- recorremos el array de argumentos
	 * En la posicion 0, tenemos el path y nombre de la APP
	 * */
#if 0   
    char *s = NULL;
    /*
     * insertamos un error del tipo ansi
     * CCFLAGAS += -ansi
     */
    for (int j=0; j<10;j++)
    {
        // insertamos comentario con estilo c++, error en estandar ISC C90.
        printf("\t%d\n",j);
    }
    if(!s) /* es una forma valida de chequear un puntero */
    {
        puts("puntero a char No Inicializado");
    }
#endif
    
	i = 1;
	while(i < argn)
	{
		/*printf("\t%d\t%s\n",i,*arg);
		 *arg++;*/
        /* printf("\t%d\t%s\n",i,arg[i]);*/
		pfxOption = get_fn_option(menu_opt,arg[i]);
		/*-- la opcion no esta dentro d ela lista "short" ni "long"*/
		if(pfxOption == NULL)
		{
			printf("Parametro " SET_COLOR(FONT,RED)"\"%s\""\
					SET_COLOR(FONT,RESET)" es incorrecto\n",arg[i]);
			puts(SET_COLOR(FONT,BLUE)"\nMenu de Ayuda"\
					SET_COLOR(FONT,RESET));
			/*-- tenemos que llamar a option_help(NULL)*/
			print_menu(menu_opt);
			exit(EXIT_SUCCESS);
		}
		/* En "pfxOption" tenemos le puntero a funcion a la operacion
		 * debemos buscar el argumento si es que tiene
		 * */
		i++;
		if(i == argn)
		{
			/* Nos quedamos sin argumentos, llegamos al final*/
			if((pfxOption)(NULL))
			{
				printf("Llamado incorrecto con los argumentos %s \n",arg[i-1]);
			}
			else
			{
				printf("Ejecucion sastifactoria de los Argumentos: %s\n",arg[i-1]);
			}
			exit(EXIT_SUCCESS);
		}
		/* comprobamos si el siguente argumento pertenece a otra accion */
		if(get_fn_option(menu_opt,arg[i]) == NULL)
		{
			/* Es un argumento para la opcion anterior */
			if((pfxOption)(arg[i]))
			{
				printf("Llamado incorrecto con los argumentos %s %s\n",arg[i-1],arg[i]);
				print_menu(menu_opt);
				exit(EXIT_SUCCESS);
			}
			else
			{
				printf("Ejecucion sastifactoria de los Argumentos: %s %s\n",arg[i-1],arg[i]);
			}
		}
		else
		{
			/* debemos llamar a la opcion sin argumento */
			i--;
			if((pfxOption)(NULL))
			{
				printf("Llamado incorrecto con el argumento: %s\n",arg[i]);
			}
			else
			{
				printf("Ejecucion sastifactoria con elArgumentos: %s\n",arg[i]);
			}
		}
		/*  tomamos un argumentos mas, incrementamos el index */
		i++;
	}
    exit(EXIT_SUCCESS);
}
/*
 * ******************************************************************************
 * \fn fx_optiont_pfT get_fn_option(menu_sT *pmenu, char * option);
 * \brief Funcion para obtener el puntero a funcion de la Opcion pasada como
 * argumeto option.
 * \param pmenu : Puntero a menu, sobre el cual tenemos definidos las
 * opciones que pueden ser llamadas y sobre la cual vamos a buscar.
 * \param option :  string con la opcion a buscar y sobre la cual obtendremos
 * el puntero a funcion.
 * \return devolveremos el puntero a funcion correspondiente a la opcion pasada
 * como \ref option.
 * 	En caso de devolver un NULL, quiere decir que la opcion no se encontro dentro
 * 	del menu o bien la opcion esta deshabilitada (ya con no tiene definida una
 * 	funcion a ejecutar).
 *********************************************************************************/
fx_optiont_pfT get_fn_option(menu_sT *pmenu, char * option)
{
	/* Buscamos una opcion valida, mientras una de las opciones
	 * "long" o "short" sean validas.
	 * */    
    while(pmenu->name != NULL)
    {
    	if((strcmp(pmenu->opt_short,option) == 0)||\
    			(strcmp(pmenu->opt_long,option) == 0))
    	{
    		return pmenu->fxOption;
    	}
    	pmenu++;
    }
    return NULL;
}

/*
 * ********************************************************************************
 * \fn int get_index_option(menu_sT *pmenu, char * name_opt);
 * \brief Funcion para obtener el indice, posicion dentro del array del menu,
 * de la opcion pasada como argumento.
 * \param pmenu : Puntero a menu, listado de opciones.
 * \param name_opt : Opcion que se desea buscar por nombre de la misma.
 * \return valor del tipo int
 * 	-1  : en el caso de no localizar el elemento por el nombre.
 * 	Mayor a '-1', el valor representa la posicion del Indice.
 * \note
 * \warning    mensaje de precaucion, warning.
* ********************************************************************************/
int get_index_option(menu_sT *pmenu, char * name_opt)
{
	int index = 0;
	/* Buscamos una opcion valida, mientras una de las opciones
	 * "long" o "short" sean validas.
	 * */
    while(pmenu->name != NULL)
    {
    	if(strcmp(pmenu->name,name_opt) == 0)
    	{
    		return index;
    	}
    	pmenu++;
    	index++;
    }
    return -1;
}

/*
 * ********************************************************************************
 * \fn void print_menu(menu_t *pmenu  );
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param *pmenu : Argumento uno del tipo menu_t.
 * \return valor de retorno del tipo void.
 *********************************************************************************/
void print_menu(menu_sT *pmenu)
{
	puts("");
    while(pmenu->name != NULL)
    {
    	printf(SET_COLOR(FONT,CYAN)"%s\t\n"SET_COLOR(FONT,RESET)\
    			,pmenu->name);
    	if((pmenu->opt_short != NULL)&&(pmenu->ley_short != NULL))
    		printf("\t%s\t:%s\n",pmenu->opt_short,pmenu->ley_short);

    	if((pmenu->opt_long != NULL)&&(pmenu->ley_long != NULL))
    		printf("\t%s\t:%s\n\n",pmenu->opt_long,pmenu->ley_long);
        pmenu++;
    }
}
/*
 * 
* ******************************************************************************** 
* \fn int find_str_in_strarray(const char ** strarray, char *str);
* \details Descripcion detallada sobre la funcion fn_name().
* \brief Funcion que busca un string dentro de un array de string.
* \param strarray : Array de String, el cual debe terminar con NULL.
* \param str : String que se desea buscar dentro del array.
* \return status de la opreacion.
*      \li -1, en caso de no encontrar el string dentro del array de string.
*      \li distinto de '-1', devuelve el indice donde encotro el string.
*********************************************************************************/
int find_str_in_strarray(const char ** strarray, char *str)
{
    int i = 0;
    if(str == NULL) return -1;
    while(strarray[i] != NULL)
    {
        if(strcmp(strarray[i],str) == 0)
        {
            return i;            
        }
        i++;        
    }
    return -1;    
}

/*
 * ******************************************************************************
 * \fn int option_help(char * arg   );
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 *********************************************************************************/
int option_template(char * arg) 
/*TODO : podemos disponer de pasarle a estos prototipos de funcion
 *  menu_sT *pmenu : menu al cual corresponde, en caso de que necesite anidarciones
 *  de acciones y deba buscar otros comandos.
 *  uint8_t opt_type : el cual puede ser 0, para la seleccion de la opcion corta
 *                     o 1 en caso de la seleccion larga
 *  Por si debe ralizar tareas de forma diferenes. 
 * Este los podemos evitar si solo tenemso un contexto paralelo para la opcion
 * corta y la larga. En este caso podemos usar el nombre que es unico.
 */
{
    int a;
    const char *targets[] = \
    {
        "SALE",
        "GRILL",
        "CHOICE",
        "PROMO",
        "Todo Ok",
        NULL        
    };
    if(arg != NULL)
    {
        a = find_str_in_strarray(targets,arg);    
        if(a < 0)
        {
            puts(SET_COLOR(FONT,RED)\
                 "fuera de los targets"\
            SET_COLOR(FONT,RESET));
        }
        else
        {
            printf("El target seleccionado es \t \""\
            SET_COLOR(FONT,GREEN)"%s"SET_COLOR(FONT,RESET)\
            "\"\tEn la posicion: "SET_COLOR(FONT,GREEN)"%d"\
            SET_COLOR(FONT,RESET)"\n",targets[a],a);            
        }
    }
#if 0    
    if(arg != NULL)
    {
        printf("\tSe paso como parametro %-4s al argumento TEMAPLATE\n", arg);
    }        
    else
    {
        puts("\tNo se paso ningun parametro al argumento TEMPLATE");
    }
#endif    
        return 0;
}

/*
 * ******************************************************************************
 * \fn int option_help(char * arg   );
 * \brief descripcion breve.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 * 	+ 0, Ejecucion sastifactoria.
 * 	+ 1, Fallo al ejecutar el comando.
 *********************************************************************************/
int option_help(char * arg)
{
	int locIndex;
	if(arg == NULL)
	{
		print_menu(menu_opt);
		return 0;
	}
	locIndex = get_index_option(menu_opt,arg);
	if( locIndex < 0)
	{
		puts(SET_COLOR(FONT,RED)"Opcion pasada para HELP incorrecta"\
				SET_COLOR(FONT,RESET));
		return 1;
	}
	printf(SET_COLOR(FONT,CYAN)"%s\t\n"SET_COLOR(FONT,RESET)\
			,menu_opt[locIndex].name);
	if((menu_opt[locIndex].opt_short != NULL)&&\
			(menu_opt[locIndex].ley_short != NULL))
		printf("\t%s\t:%s\n",menu_opt[locIndex].opt_short\
				,menu_opt[locIndex].ley_short);

	if((menu_opt[locIndex].opt_long != NULL)&&\
			(menu_opt[locIndex].ley_long != NULL))
		printf("\t%s\t:%s\n\n",menu_opt[locIndex].opt_long,\
				menu_opt[locIndex].ley_long);
	return 0;
}




/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 1      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 2)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 2    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 2
*/
#include <string.h>

/**
 * \var menu_opt;
 * \brief      Estructura que contiene la definiciones del comando.
 * Opcion corta, larga,leyenda/msg para cada una de las opciones.
 * \version    numero de version/es a la cual corresponde.
 * \warning mensaje de "warning". */
menu_sT menu_opt[] = \
{
	{"HELP","-h","--help",leyend_help,option_help},
	{"TEMPLATE","-t","--template",leyend_template,option_template},
	{NULL}
};
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{
	int i;
	fx_optiont_pfT pfxOption;
	/*-- recorremos el array de argumentos
	 * En la posicion 0, tenemos el path y nombre de la APP
	 * */
	i = 1;
	while(i < argn)
	{
		/*printf("\t%d\t%s\n",i,*arg);
		 *arg++;*/
		printf("\t%d\t%s\n",i,arg[i]);
		pfxOption = get_fn_option(menu_opt,arg[i]);
		/*-- la opcion no esta dentro d ela lista "short" ni "long"*/
		if(pfxOption == NULL)
		{
			printf("Parametro " SET_COLOR(FONT,RED)"\"%s\""\
					SET_COLOR(FONT,RESET)" es incorrecto\n",arg[i]);
			puts(SET_COLOR(FONT,BLUE)"\nMenu de Ayuda"\
					SET_COLOR(FONT,RESET));
			/*-- tenemos que llamar a option_help(NULL)*/
			print_menu(menu_opt);
			exit(EXIT_SUCCESS);
		}
		/* En "pfxOption" tenemos le puntero a funcion a la operacion
		 * debemos buscar el argumento si es que tiene
		 * */
		i++;
		if(i == argn)
		{
			/* Nos quedamos sin argumentos, llegamos al final*/
			if((pfxOption)(NULL))
			{
				printf("Llamado incorrecto con los argumentos %s \n",arg[i-1]);
			}
			else
			{
				printf("Ejecucion sastifactoria de los Argumentos: %s\n",arg[i-1]);
			}
			exit(EXIT_SUCCESS);
		}
		/* comprobamos si el siguente argumento pertenece a otra accion */
		if(get_fn_option(menu_opt,arg[i]) == NULL)
		{
			/* Es un argumento para la opcion anterior */
			if((pfxOption)(arg[i]))
			{
				printf("Llamado incorrecto con los argumentos %s %s\n",arg[i-1],arg[i]);
				print_menu(menu_opt);
				exit(EXIT_SUCCESS);
			}
			else
			{
				printf("Ejecucion sastifactoria de los Argumentos: %s %s\n",arg[i-1],arg[i]);
			}
		}
		else
		{
			/* debemos llamar a la opcion sin argumento */
			i--;
			if((pfxOption)(NULL))
			{
				printf("Llamado incorrecto con el argumento: %s\n",arg[i]);
			}
			else
			{
				printf("Ejecucion sastifactoria con elArgumentos: %s\n",arg[i]);
			}
		}
		/*  tomamos un argumentos mas, incrementamos el index */
		i++;
	}
    exit(EXIT_SUCCESS);
}


/*
 * ******************************************************************************
 * \fn fx_optiont_pfT get_funtionOption(menu_t *pmenu, char * option);
 * \brief Funcion para obtener el puntero a funcion de la Opcion pasada como
 * argumeto option.
 * \param pmenu : Puntero a menu, sobre el cual tenemos definidos las
 * opciones que pueden ser llamadas y sobre la cual vamos a buscar.
 * \param option :  string con la opcion a buscar y sobre la cual obtendremos
 * el puntero a funcion.
 * \return devolveremos el puntero a funcion correspondiente a la opcion pasada
 * como \ref option.
 * 	En caso de devolver un NULL, quiere decir que la opcion no se encontro dentro
 * 	del menu o bien la opcion esta deshabilitada (ya con no tiene definida una
 * 	funcion a ejecutar).
 *********************************************************************************/
fx_optiont_pfT get_fn_option(menu_sT *pmenu, char * option)
{
	/* Buscamos una opcion valida, mientras una de las opciones
	 * "long" o "short" sean validas.
	 * */
    /* while((pmenu->opt_short != NULL)||(pmenu->opt_long != NULL)) */
    while(pmenu->name != NULL)
    {
    	if((strcmp(pmenu->opt_short,option) == 0)||\
    			(strcmp(pmenu->opt_long,option) == 0))
    	{
    		return pmenu->fxOption;
    	}
    	pmenu++;
    }
    return NULL;
}


int get_index_option(menu_sT *pmenu, char * name_opt)
{
	int index = 0;
	/* Buscamos una opcion valida, mientras una de las opciones
	 * "long" o "short" sean validas.
	 * */
    while(pmenu->name != NULL)
    {
    	if(strcmp(pmenu->name,name_opt) == 0)
    	{
    		return index;
    	}
    	pmenu++;
    	index++;
    }
    return -1;
}
/**
 * ****************************************************************************//**
 * \fn int option_help(char * arg   );
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 *********************************************************************************/
int option_template(char * arg)
{
	return 0;
}

/**
 * ****************************************************************************//**
 * \fn int option_help(char * arg   );
 * \brief descripcion breve.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 * 	+ 0, Ejecucion sastifactoria.
 * 	+ 1, Fallo al ejecutar el comando.
 *********************************************************************************/
int option_help(char * arg)
{
	int locIndex;
	if(arg == NULL)
	{
		print_menu(menu_opt);
		return 0;
	}
	locIndex = get_index_option(menu_opt,arg);
	if( locIndex < 0)
	{
		puts(SET_COLOR(FONT,RED)"Opcion pasada para HELP incorrecta"\
				SET_COLOR(FONT,RESET));
		return 1;
	}
	printf(SET_COLOR(FONT,CYAN)"%s\t\n"SET_COLOR(FONT,RESET)\
			,menu_opt[locIndex].name);
	if((menu_opt[locIndex].opt_short != NULL)&&\
			(menu_opt[locIndex].ley_short != NULL))
		printf("\t%s\t:%s\n",menu_opt[locIndex].opt_short\
				,menu_opt[locIndex].ley_short);

	if((menu_opt[locIndex].opt_long != NULL)&&\
			(menu_opt[locIndex].ley_long != NULL))
		printf("\t%s\t:%s\n\n",menu_opt[locIndex].opt_long,\
				menu_opt[locIndex].ley_long);
	return 0;
}


/*
 * ********************************************************************************
 * \fn void print_menu(menu_t *pmenu  );
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param *pmenu : Argumento uno del tipo menu_t.
 * \return valor de retorno del tipo void.
 *********************************************************************************/
void print_menu(menu_sT *pmenu)
{
	puts("");
    /* while((pmenu->opt_short != NULL)||(pmenu->opt_long != NULL)) */
    while(pmenu->name != NULL)
    {
    	printf(SET_COLOR(FONT,CYAN)"%s\t\n"SET_COLOR(FONT,RESET)\
    			,pmenu->name);
    	if((pmenu->opt_short != NULL)&&(pmenu->ley_short != NULL))
    		printf("\t%s\t:%s\n",pmenu->opt_short,pmenu->ley_short);

    	if((pmenu->opt_long != NULL)&&(pmenu->ley_long != NULL))
    		printf("\t%s\t:%s\n\n",pmenu->opt_long,pmenu->ley_long);
        pmenu++;
    }
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 2      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 3)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 3    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 3
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
        
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 3      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 4)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 4    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 4
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   

    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 4      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 5)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 5    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 5
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
     
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 5      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 6)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 6    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 6
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
      
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 6      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 7)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 7    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 7
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
   
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 7      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 8)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 8    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 8
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 8      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 9)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 9    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 9
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 9      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 10)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 10   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 10
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 10     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 11)


#warning "Version no definida aun"
#else /* #if (__main_version__ == 1) */
#warning "__main_version__ mal definido"
#endif /* #if (__main_version__ == 1) */
/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */



/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End APIs Definitions ]============================
 *
 * ******************************************************************************************** 
 * 
 * 
 */
