 /*
 * ******************************[ Source file main.c ]*************************************//**
 * \addtogroup main_c 
 * @{
 * @copyright 
 * ${date}, Luccioni Jesús Emanuel. \n
 * All rights reserved.\n
 * This file is part of port module.\n
 * Redistribution is not allowed on binary and source forms, with or without 
 * modification.\n
 * Use is permitted with prior authorization by the copyright holder.\n
 * \file main.c
 * \brief bloque de comenteario para documentacion para describir este archivo de 
 * cabecera o header file. 
 * \version v01.01
 * \date   ${date}
 * \note none
 * \author JEL, Jesus Emanuel Luccioni
 * \li piero.jel@gmail.com
 * @} doxygen end group definition
 *************************************************************************************/



/* **********************************************************************************************
 *
 * ===========================[ Begin include header file ]============================
 *
 * ********************************************************************************************** 
 * 
 */
#include <stdio.h> /* header file correspondiente a las libreria
estandar de entrada salida*/
#include <stdlib.h> /* header file correspondiente a las libreria
estandar para el uso de la funcion @ref exit() con sus 
macros @ref EXIT_FAIL y @ref EXIT_SUCCESS*/
#include <stdarg.h>
#include <typedef.h>
/*  
 * **********************************************************************************************
 *
 * ===========================[ End include header file ]============================
 *
 * *********************************************************************************************/


/**
 * \def __main_version__
 * \brief Establecemos la Version para el main.
 * \li + 0, 
 * \li + 1, 
 * \li + 2,
 * \li + 3,  
 * \li + 4,
 * \li + 5, 
 * \li + 6, 
 * \li + 7, 
 * \li + 8, 
 * \li + 9, 
 * TODO: __main_version__
 */
#ifndef __main_version__

#define __main_version__    1
#endif
/*-- Incluimos el header file del modulo principal, para que tome los cambios
 * de la definicion del label __main_version__ */
#include <main.h>



/* 
 *
 * 
 * *********************************************************************************************
 *
 * ===========================[ Begin main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */
#if (__main_version__ == 0)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 0    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 0
*/
#include <string.h>
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
    if( argn != 7 )
    {
       printf(SET_COLOR(FONT,RED)\
            "\t El numero de Argumentos\"%d\", incorecto, deben ser %d\n"
        SET_COLOR(FONT,RESET),argn,6); 
        exit(EXIT_SUCCESS);
    }
    
    if( (*arg[1] == '5') && (strlen(arg[1])==1) ) 
    {
        if( (*arg[2] == '4') && (strlen(arg[2])==1)) 
        {
            if( (*arg[3] == '3') && (strlen(arg[3])==1) ) 
            {
                if( (*arg[4] == '2') && (strlen(arg[4])==1) ) 
                {
                    if( (*arg[5] == '1') && (strlen(arg[5])==1) ) 
                    {
                        if( (*arg[6] == '0' ) && (strlen(arg[6])==1)) 
                        {
                            puts(SET_COLOR(FONT,GREEN)\
                                "\tArgumento Ingresados de Forma Sastifactoria\n"
                                SET_COLOR(FONT,RESET));                            
                        }
                        else
                        {
                            printf(SET_COLOR(FONT,RED)\
                                "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
                            SET_COLOR(FONT,RESET),6,arg[6],0);         
                        }
                    }
                    else
                    {
                        printf(SET_COLOR(FONT,RED)\
                            "\tArgumento [%d] : \"%s\" es distinti \"%d\"\n"
                        SET_COLOR(FONT,RESET),5,arg[5],1);         
                    }                    
                }
                else
                {
                    printf(SET_COLOR(FONT,RED)\
                        "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
                    SET_COLOR(FONT,RESET),4,arg[4],2);         
                }                
            }
            else
            {
                printf(SET_COLOR(FONT,RED)\
                    "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
                SET_COLOR(FONT,RESET),3,arg[3],3);         
            }            
        }
        else
        {
            printf(SET_COLOR(FONT,RED)\
                "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
            SET_COLOR(FONT,RESET),2,arg[2],4);         
        }        
    }
    else
    {
        printf(SET_COLOR(FONT,RED)\
            "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
        SET_COLOR(FONT,RESET),1,arg[1],5);         
    }
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 0      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 1)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 1    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 1
*/
#include <string.h>
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
    if( argn != 7 )
    {
       printf(SET_COLOR(FONT,RED)\
            "\t El numero de Argumentos\"%d\", incorecto, deben ser %d\n"
        SET_COLOR(FONT,RESET),argn,6); 
        exit(EXIT_SUCCESS);
    }    
    do{
        if( (*arg[1] != '5') || (strlen(arg[1]) != 1) )
        {
           printf(SET_COLOR(FONT,RED)\
            "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
            SET_COLOR(FONT,RESET),1,arg[1],5); 
           break;
        }
        if( (*arg[2] != '4') || (strlen(arg[2]) != 1) )
        {
           printf(SET_COLOR(FONT,RED)\
            "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
            SET_COLOR(FONT,RESET),2,arg[2],4); 
           break;
        }
        if( (*arg[3] != '3') || (strlen(arg[3]) != 1) )
        {
           printf(SET_COLOR(FONT,RED)\
            "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
            SET_COLOR(FONT,RESET),3,arg[3],3); 
           break;
        }
        if( (*arg[4] != '2') || (strlen(arg[4]) != 1))
        {
           printf(SET_COLOR(FONT,RED)\
            "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
            SET_COLOR(FONT,RESET),4,arg[4],2); 
           break;
        }
        if( (*arg[5] != '1' ) || (strlen(arg[5]) != 1) )
        {
           printf(SET_COLOR(FONT,RED)\
            "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
            SET_COLOR(FONT,RESET),5,arg[5],1); 
           break;
        }
        if( (*arg[6] != '0') || (strlen(arg[6]) != 1))
        {
           printf(SET_COLOR(FONT,RED)\
            "\tArgumento [%d] : \"%s\" es distinto \"%d\"\n"
            SET_COLOR(FONT,RESET),6,arg[6],0); 
           break;
        }
        /*-- todas las condiciones son "FALSAS", */
        puts(SET_COLOR(FONT,GREEN)\
             "\tArgumento Ingresados de Forma Sastifactoria\n"
             SET_COLOR(FONT,RESET)); 
            
    }while(0);    
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 1      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 2)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 2    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 2
*/
#include <string.h>

/**
 * \var menu_opt;
 * \brief      Estructura que contiene la definiciones del comando.
 * Opcion corta, larga,leyenda/msg para cada una de las opciones.
 * \version    numero de version/es a la cual corresponde.
 * \warning mensaje de "warning". */
menu_sT menu_opt[] = \
{
	{"HELP","-h","--help",leyend_help,option_help},
	{"TEMPLATE","-t","--template",leyend_template,option_template},
	{NULL}
};
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{
	int i;
	fx_optiont_pfT pfxOption;
	/*-- recorremos el array de argumentos
	 * En la posicion 0, tenemos el path y nombre de la APP
	 * */
	i = 1;
	while(i < argn)
	{
		/*printf("\t%d\t%s\n",i,*arg);
		 *arg++;*/
		printf("\t%d\t%s\n",i,arg[i]);
		pfxOption = get_fn_option(menu_opt,arg[i]);
		/*-- la opcion no esta dentro d ela lista "short" ni "long"*/
		if(pfxOption == NULL)
		{
			printf("Parametro " SET_COLOR(FONT,RED)"\"%s\""\
					SET_COLOR(FONT,RESET)" es incorrecto\n",arg[i]);
			puts(SET_COLOR(FONT,BLUE)"\nMenu de Ayuda"\
					SET_COLOR(FONT,RESET));
			/*-- tenemos que llamar a option_help(NULL)*/
			print_menu(menu_opt);
			exit(EXIT_SUCCESS);
		}
		/* En "pfxOption" tenemos le puntero a funcion a la operacion
		 * debemos buscar el argumento si es que tiene
		 * */
		i++;
		if(i == argn)
		{
			/* Nos quedamos sin argumentos, llegamos al final*/
			if((pfxOption)(NULL))
			{
				printf("Llamado incorrecto con los argumentos %s \n",arg[i-1]);
			}
			else
			{
				printf("Ejecucion sastifactoria de los Argumentos: %s\n",arg[i-1]);
			}
			exit(EXIT_SUCCESS);
		}
		/* comprobamos si el siguente argumento pertenece a otra accion */
		if(get_fn_option(menu_opt,arg[i]) == NULL)
		{
			/* Es un argumento para la opcion anterior */
			if((pfxOption)(arg[i]))
			{
				printf("Llamado incorrecto con los argumentos %s %s\n",arg[i-1],arg[i]);
				print_menu(menu_opt);
				exit(EXIT_SUCCESS);
			}
			else
			{
				printf("Ejecucion sastifactoria de los Argumentos: %s %s\n",arg[i-1],arg[i]);
			}
		}
		else
		{
			/* debemos llamar a la opcion sin argumento */
			i--;
			if((pfxOption)(NULL))
			{
				printf("Llamado incorrecto con el argumento: %s\n",arg[i]);
			}
			else
			{
				printf("Ejecucion sastifactoria con elArgumentos: %s\n",arg[i]);
			}
		}
		/*  tomamos un argumentos mas, incrementamos el index */
		i++;
	}
    exit(EXIT_SUCCESS);
}


/*
 * ******************************************************************************
 * \fn fx_optiont_pfT get_funtionOption(menu_t *pmenu, char * option);
 * \brief Funcion para obtener el puntero a funcion de la Opcion pasada como
 * argumeto option.
 * \param pmenu : Puntero a menu, sobre el cual tenemos definidos las
 * opciones que pueden ser llamadas y sobre la cual vamos a buscar.
 * \param option :  string con la opcion a buscar y sobre la cual obtendremos
 * el puntero a funcion.
 * \return devolveremos el puntero a funcion correspondiente a la opcion pasada
 * como \ref option.
 * 	En caso de devolver un NULL, quiere decir que la opcion no se encontro dentro
 * 	del menu o bien la opcion esta deshabilitada (ya con no tiene definida una
 * 	funcion a ejecutar).
 *********************************************************************************/
fx_optiont_pfT get_fn_option(menu_sT *pmenu, char * option)
{
	/* Buscamos una opcion valida, mientras una de las opciones
	 * "long" o "short" sean validas.
	 * */
    /* while((pmenu->opt_short != NULL)||(pmenu->opt_long != NULL)) */
    while(pmenu->name != NULL)
    {
    	if((strcmp(pmenu->opt_short,option) == 0)||\
    			(strcmp(pmenu->opt_long,option) == 0))
    	{
    		return pmenu->fxOption;
    	}
    	pmenu++;
    }
    return NULL;
}


int get_index_option(menu_sT *pmenu, char * name_opt)
{
	int index = 0;
	/* Buscamos una opcion valida, mientras una de las opciones
	 * "long" o "short" sean validas.
	 * */
    while(pmenu->name != NULL)
    {
    	if(strcmp(pmenu->name,name_opt) == 0)
    	{
    		return index;
    	}
    	pmenu++;
    	index++;
    }
    return -1;
}
/**
 * ****************************************************************************//**
 * \fn int option_help(char * arg   );
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 *********************************************************************************/
int option_template(char * arg)
{
	return 0;
}

/**
 * ****************************************************************************//**
 * \fn int option_help(char * arg   );
 * \brief descripcion breve.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 * 	+ 0, Ejecucion sastifactoria.
 * 	+ 1, Fallo al ejecutar el comando.
 *********************************************************************************/
int option_help(char * arg)
{
	int locIndex;
	if(arg == NULL)
	{
		print_menu(menu_opt);
		return 0;
	}
	locIndex = get_index_option(menu_opt,arg);
	if( locIndex < 0)
	{
		puts(SET_COLOR(FONT,RED)"Opcion pasada para HELP incorrecta"\
				SET_COLOR(FONT,RESET));
		return 1;
	}
	printf(SET_COLOR(FONT,CYAN)"%s\t\n"SET_COLOR(FONT,RESET)\
			,menu_opt[locIndex].name);
	if((menu_opt[locIndex].opt_short != NULL)&&\
			(menu_opt[locIndex].ley_short != NULL))
		printf("\t%s\t:%s\n",menu_opt[locIndex].opt_short\
				,menu_opt[locIndex].ley_short);

	if((menu_opt[locIndex].opt_long != NULL)&&\
			(menu_opt[locIndex].ley_long != NULL))
		printf("\t%s\t:%s\n\n",menu_opt[locIndex].opt_long,\
				menu_opt[locIndex].ley_long);
	return 0;
}


/*
 * ********************************************************************************
 * \fn void print_menu(menu_t *pmenu  );
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param *pmenu : Argumento uno del tipo menu_t.
 * \return valor de retorno del tipo void.
 *********************************************************************************/
void print_menu(menu_sT *pmenu)
{
	puts("");
    /* while((pmenu->opt_short != NULL)||(pmenu->opt_long != NULL)) */
    while(pmenu->name != NULL)
    {
    	printf(SET_COLOR(FONT,CYAN)"%s\t\n"SET_COLOR(FONT,RESET)\
    			,pmenu->name);
    	if((pmenu->opt_short != NULL)&&(pmenu->ley_short != NULL))
    		printf("\t%s\t:%s\n",pmenu->opt_short,pmenu->ley_short);

    	if((pmenu->opt_long != NULL)&&(pmenu->ley_long != NULL))
    		printf("\t%s\t:%s\n\n",pmenu->opt_long,pmenu->ley_long);
        pmenu++;
    }
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 2      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 3)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 3    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 3
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
        
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 3      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 4)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 4    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 4
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   

    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 4      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 5)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 5    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 5
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
     
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 5      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 6)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 6    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 6
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
      
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 6      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 7)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 7    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 7
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
   
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 7      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 8)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 8    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 8
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 8      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 9)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 9    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 9
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 9      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 10)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 10   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 10
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 10     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 11)


#warning "Version no definida aun"
#else /* #if (__main_version__ == 1) */
#warning "__main_version__ mal definido"
#endif /* #if (__main_version__ == 1) */
/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */



/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End APIs Definitions ]============================
 *
 * ******************************************************************************************** 
 * 
 * 
 */
