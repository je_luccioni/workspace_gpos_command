 /*
 * ******************************[ Header File main.h ]*************************************//**
 * \addtogroup main_h
 * @{
 * @copyright 
 * 2016, Luccioni Jesús Emanuel. \n
 * All rights reserved.\n
 * This file is part of port module.\n
 * Redistribution is not allowed on binary and source forms, with or without 
 * modification.\n
 * Use is permitted with prior authorization by the copyright holder.\n
 * \file main.h
 * \brief modulo principal "header file". 
 * \version v01.01
 * \date   26 de feb. de 2017
 * \note none
 * \author JEL, Jesus Emanuel Luccioni
 * \li piero.jel@gmail.com 
 *************************************************************************************/
#ifndef __main_h__
#define __main_h__
/*
 * ============================[Open, cplusplus]============================
 */
#ifdef __cplusplus
extern "C" {
#endif    

/*
 *   
 * *********************************************************************************************
 *
 * ===========================[ Begin labels definitions  ]============================
 *
 * ******************************************************************************************** 
 * 
 */    


    
    
 /*
  * 
  *********************************************************************************************
  *
  * ===========================[ End labels definitions    ]============================
  *
  * ********************************************************************************************  
  */      
 /*
 * 
 * 
 * *********************************************************************************************
 *
 * ===========================[ Begin MCROS Functions ]============================
 *
 * ******************************************************************************************** 
 * 
 */      

/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End Macros Functions ]============================
 *
 * ******************************************************************************************** 
 */  
/*
 * 
 * 
 * ****************************************************************************************************
 *
 * ===========================[ Begin APIs, macros , typedef Declaration ]============================
 *
 * ***************************************************************---------*****************************
 * 
 */


#if (__main_version__ == 0)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 0    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 0
*/
/*
 * ****************************************************************************//**
 * \def PRINT_SIZE_LEYEND()
 * \brief Macro Funcion para imprimir por pantalla la leyenda correspondiente al
 * macro \ref PRINT_SIZE().
 * \return nothig 
 * \n \e Formato :
 * \n \li \f$ nombre    tamaño[byte]    tamaño[bit]\f$   
 *********************************************************************************/
/*
 * ****************************************************************************//**
 * \def PRINT_SIZE(typeVar)
 * \brief Macro Funcion para imprimir por pantalla el tamaño del tipo de variable
 * \e typeVar, pasado como parametro. Con el formato especificado por la leyenda 
 * establecida en \ref PRINT_SIZE_LEYEND().
 * \return nothig 
 * \n \e Formato :
 * \n \li \f$ nombre    tamaño[byte]    tamaño[bit]\f$   
 *********************************************************************************/ 
#if defined(__M32__)
#define PRINT_SIZE_LEYEND()     printf("\tnombre\t\ttamaño [Bytes]\t\ttamaño [bits]\n")
#define PRINT_SIZE(typeVar)     printf("    %-20s    %-20d    %-20d\n",#typeVar,sizeof(typeVar),(sizeof(typeVar)*8))
/*#define STDstream_PRINT(STDtype)	#STDtype"\t%d\n",STDtype*/
#elif defined(__M64__)
#define PRINT_SIZE_LEYEND()     printf("\tnombre\t\ttamaño [Bytes]\t\ttamaño [bits]\n")
#define PRINT_SIZE(typeVar)     printf("    %-20s    %-20ld    %-20ld\n",#typeVar,sizeof(typeVar),(sizeof(typeVar)*8))
#elif defined(__Mhost__)
#define PRINT_SIZE_LEYEND()     
#define PRINT_SIZE(typeVar)     
#else 
#warning "Arquitectura definida de forma incorrecta"
#define PRINT_SIZE_LEYEND()     
#define PRINT_SIZE(typeVar)     
#endif

/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 0      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 1)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 1    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 1
*/
/**
* \typedef fx_optiont_pfT;
* \details Redefinicion de tipo puntero a funcion que utilizaremos como
* callback a la opcion seleccionada. Ejecucion en l afuncion llamada.
* prototipo de la funcion "int fn_name(char * arg);"
* \brief Descripcion breve sobre la redefinicion de un tipo de dato primitivo.
* \version numero de version a la cual corresponde.
* \note nota.
* \warning mensaje de "warning". */
typedef int (*fx_optiont_pfT) (char *arg);



/**
* \struct menu_sT ;
* \brief redefinicion de la estructura del tipo menu, la cual continee
* todo el contexto necesario para un arguetmno del comando.
* \li \ref name
* \li \ref opt_short
* \li \ref opt_long
* \li \ref ley_short
* \li \ref ley_long
* \li \ref fxOption
* \li \ref details
* \li \ref example
* \note
* \warning
*/
typedef struct
{
	const char * name; 		/**<@brief Nombre del comando, es obligatorio si es NULL termina la lista
	 	 	 	 	 	 	 de comandos, y salta caulquier ejecucion. */
    const char * opt_short;	/**<@brief opcion corta la cual inicia con '-' y lo sigue una sola letra que
    						representa el arguemtno en si  */
    const char * opt_long; 	/**<@brief opcion larga que inicia con '--', lo sigue un string */
    const char * ley_short; /**<@brief Leyenda correspondiente a la opcion corta */
    const char * ley_long;	/**<@brief Leyenda correspondiente a la opcion larga */
    fx_optiont_pfT fxOption;/**<@brief puntero a funcion, correspondiente a la opcion del comando */
    const char * details;	/**<@brief detalle sobre la opcion de arguemtno seleccioanda */
    const char * example;	/**<@brief ejemplo de usos "para el usuario del comando" del argumento usado
     	 	 	 	 	 	 para la opcion  */
}menu_sT;


/**
 *
 * ****************************************************************************//**
 * \fn fx_optiont_pfT get_fn_option(menu_sT *pmenu, char * option);
 * \brief Funcion para obtener el puntero a funcion de la Opcion pasada como
 * argumeto \ref option.
 * \param pmenu : Puntero a menu, sobre el cual tenemos definidos las
 * opciones que pueden ser llamadas y sobre la cual vamos a buscar.
 * \param option :  string con la opcion a buscar y sobre la cual obtendremos
 * el puntero a funcion.
 * \return puntero a funcion del tipo \ref fx_optiont_pfT.
 *  \li devolveremos el puntero a funcion correspondiente a la opcion pasada
 * como \ref option.
 * 	\li En caso de devolver un NULL, quiere decir que la opcion no se encontro dentro
 * 	del menu o bien la opcion esta deshabilitada.
 * (ya que no cuenta con una definida una funcion a ejecutar).
 * \note
 * \warning
 * \par example :
<PRE>
  pfxOption = get_funtionOption(menu_option,arg[i]);
  if(pfxOption == NULL)
  {
	printf("Parametro \"%s\" es incorrecto",arg[i]);
	print_menu(menu_option);
	exit(EXIT_SUCCESS);
  }
</PRE>
 *********************************************************************************/
fx_optiont_pfT get_fn_option(menu_sT *pmenu, char * option);

/*
 * ****************************************************************************//**
 * \fn int get_index_option(menu_sT *pmenu, char * name_opt);
 * \brief Funcion para obtener el indice, posicion dentro del array del menu,
 * de la opcion pasada como argumento.
 * \param pmenu : Puntero a menu, listado de opciones.
 * \param name_opt : Opcion que se desea buscar por nombre de la misma.
 * \return valor del tipo int.
 * \li -1  : en el caso de no localizar el elemento por el nombre.
 * \li Mayor a '-1', el valor representa la posicion del Indice.
 * \note
 * \warning    mensaje de precaucion, warning.
 * @par example :
 <pre>

 </pre>
 * \date 12 oct. 2020
 * \version
 * \author jel, Luccioni, Jesus Emanuel
 * \par meil
 * piero.jel@gmail.com
* ********************************************************************************/
int get_index_option(menu_sT *pmenu, char * name_opt);
/**
 * 
* ****************************************************************************//** 
* \fn int find_str_in_strarray(const char ** strarray, char *str);
* \details Descripcion detallada sobre la funcion fn_name().
* \brief Funcion que busca un string dentro de un array de string.
* \param strarray : Array de String, el cual debe terminar con NULL.
* \param str : String que se desea buscar dentro del array.
* \return status de la opreacion.
*      \li -1, en caso de no encontrar el string dentro del array de string.
*      \li distinto de '-1', devuelve el indice donde encotro el string.
* \version numero de version a la cual corresponde.
* \note nota
* \warning mensaje de "warning". 
* \par example :
<PRE>
    int a;
    const char *targets[] = \
    {
        "SALE",
        "GRILL",
        "CHOICE",
        "PROMO",
        "Todo Ok",
        NULL        
    };
    if(arg != NULL)
    {
        a = find_str_in_strarray(targets,arg);    
        if(a < 0)
        {
            puts(SET_COLOR(FONT,RED)\
                 "fuera de los targets"\
            SET_COLOR(FONT,RESET));
        }
        else
        {
            printf("El target seleccionado es \t \""\
            SET_COLOR(FONT,GREEN)"%s"SET_COLOR(FONT,RESET)\
            "\"\tEn la posicion: "SET_COLOR(FONT,GREEN)"%d"\
            SET_COLOR(FONT,RESET),targets[a],a);            
        }
    }
</PRE>  
*********************************************************************************/
int find_str_in_strarray(const char ** strarray, char *str);

/*===================================================================================================================
 *
 * 	Definicion de las funciones y variables globales
 *
 * 		>> debemos respetar el formato
 * 			Estructura de la leyenda:	leyend_sT leyend_<NmbOption>[]{<FILL LEYEND>};
 * 			Estructura de la Funcion:	int option_<NmbOption>(char * arg); -> solo declaracion
 *
 * ==================================================================================================================*/


/**
* 
* ****************************************************************************//**
* \def leyend_help
* \details Maro labels que representa la Leyendas para las Opciones Helps.
* Tanto para la opcion corta '-h' como para la version larga '--helps'
* \brief String que representan las leyendas para las opciones del Argumento 
* HELP.
* \version Numero de version a la cual corresponde.
* \note nota.
* \warning mensaje de "warning". 
*********************************************************************************/ 
#define leyend_help \
	"formato corto del Help",\
	"formato largo de HELP"


/**
 * ****************************************************************************//**
 * \fn int option_help(char * arg   );
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 * \note nota sobre la funcion.
 * \warning mensaje de precaucion sobre la funcion.
 * \par example :
  <PRE>
  op = option_lg("on" );
  </PRE>
 *********************************************************************************/
int option_help(char * arg);

/**
* 
* ****************************************************************************//**
* \def leyend_template
* \details Maro labels que representa la Leyendas para las Opciones template.
* Tanto para la opcion corta '-t' como para la version larga '--template'
* \brief String que representan las leyendas para las opciones del Argumento 
* TEMAPLATE.
* \version Numero de version a la cual corresponde.
* \note nota.
* \warning mensaje de "warning". 
*********************************************************************************/ 
#define leyend_template \
	"formato corto para representar un Template",\
	"formato largo para representar un Template"

/**
 * ****************************************************************************//**
 * \fn int option_template(char * arg   );
 * \details descripcon detallada.
 * \brief funcion que se ejecutara cuando se pase el arguemento --template.
 * \param arg : Argumento uno del tipo puntero a char, string .
 * \return valor de retorno del tipo int.
 *  + 0, is la ejecucion de la peticion fue sastifactoria.
 *  + 1, si surgio algun error a la hora de la ejecucion.
 * \note nota sobre la funcion.
 * \warning mensaje de precaucion sobre la funcion.
 * \par example :
  <PRE>
  op = option_template("on" );
  </PRE>
 *********************************************************************************/
int option_template(char * arg);
/**
 *
 * ****************************************************************************//**
 * \fn void print_menu(menu_sT *pmenu);
 * \brief descripcion breve.
 * \details descripcon detallada.
 * \param *pmenu : Argumento uno del tipo menu_t.
 * \return valor de retorno del tipo void.
 * \note nota sobre la funcion.
 * \warning mensaje de precaucion sobre la funcion.
 * \par example :
  <PRE>
   print_menu(menu_options);
  </PRE>
 *********************************************************************************/
void print_menu(menu_sT *pmenu);



#if 0
#define OPTION(Opt) "--"#Opt
#define OPTION(Opt) OPTION_##Opt
#define LEYEND(Option) LEYEND_##Option()
#define FUNCTION(Option) FUNCTION_##Option()
#endif






/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 1      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 2)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 2    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 2
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 2      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 3)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 3    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 3
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 3      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 4)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 4    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 4
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 4      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 5)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 5    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 5
*/




/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 5      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 6)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 6    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 6
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 6      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 7)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 7    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 7
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 7      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 8)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 8    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 8
*/




/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 8      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 9)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 9    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 9
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 9      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 10)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 10   ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 10
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 10     ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#else
#warning "__main_version__ definido de forma incorrecta"

#endif

/*
 *
 *
 * ****************************************************************************************************
 *
 * ===========================[ End  APIs, macros , datos Declaration ]============================
 *
 * ***************************************************************---------*****************************
 *
 */





/*
 * 
 * 
 * *********************************************************************************************
 *
 * ===========================[ Begin main Declaration ]============================
 *
 * ******************************************************************************************** 
 * 
 */
/*
*
* ****************************************************************************//**
* \fn int main(int argn,char **arg);
* \brief funcion principal punto de partida de la aplicacion.
* \param argn numeros de Arguemtnos por defecto es '1'.
* \param arg array de arguemento, arg[0] := path/appname.
* \return return estado de la ejecucion.
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa.
 *  + EXIT_FAIL : ejecucion finalizada con error.
*********************************************************************************/
int main(int argn,char **arg);
/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End main Declaration ]============================
 *
 * ******************************************************************************************** 
 * 
 * 
 */
/*
 * ============================[close, cplusplus]============================
 */
#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef __main_h__ */
