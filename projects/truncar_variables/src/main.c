 /*
 * ******************************[ Source file main.c ]*************************************//**
 * \addtogroup main_c 
 * @{
 * @copyright 
 * ${date}, Luccioni Jesús Emanuel. \n
 * All rights reserved.\n
 * This file is part of port module.\n
 * Redistribution is not allowed on binary and source forms, with or without 
 * modification.\n
 * Use is permitted with prior authorization by the copyright holder.\n
 * \file main.c
 * \brief bloque de comenteario para documentacion para describir este archivo de 
 * cabecera o header file. 
 * \version v01.01
 * \date   ${date}
 * \note none
 * \author JEL, Jesus Emanuel Luccioni
 * \li piero.jel@gmail.com
 * @} doxygen end group definition
 *************************************************************************************/



/* **********************************************************************************************
 *
 * ===========================[ Begin include header file ]============================
 *
 * ********************************************************************************************** 
 * 
 */
#include <stdio.h> /* header file correspondiente a las libreria
estandar de entrada salida*/
#include <stdlib.h> /* header file correspondiente a las libreria
estandar para el uso de la funcion @ref exit() con sus 
macros @ref EXIT_FAIL y @ref EXIT_SUCCESS*/
#include <stdarg.h>
#include <typedef.h>
/*  
 * **********************************************************************************************
 *
 * ===========================[ End include header file ]============================
 *
 * *********************************************************************************************/


/**
 * \def __main_version__
 * \brief Establecemos la Version para el main.
 * \li + 0, 
 * \li + 1, 
 * \li + 2,
 * \li + 3,  
 * \li + 4,
 * \li + 5, 
 * \li + 6, 
 * \li + 7, 
 * \li + 8, 
 * \li + 9, 
 * TODO: __main_version__
 */
#ifndef __main_version__

#define __main_version__    2
#endif
/*-- Incluimos el header file del modulo principal, para que tome los cambios
 * de la definicion del label __main_version__ */
#include <main.h>



/* 
 *
 * 
 * *********************************************************************************************
 *
 * ===========================[ Begin main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */
#if (__main_version__ == 0)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 0    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 0
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{  
    int i = 0;
    /*--
     * Imprimimos las lista de arguementos pasada
     */
    while(i<argn)
    {
        printf(SET_COLOR(FONT,RED)"\tARG[%d] \t %s\n"
        SET_COLOR(FONT,RESET),i,arg[i]);
        i++;        
    }    
    PRINT_SIZE_LEYEND();
    PRINT_SIZE(char );    
    PRINT_SIZE(short int );    
    PRINT_SIZE(int );
    PRINT_SIZE(long int );
    PRINT_SIZE(long long int );
    PRINT_SIZE(float );
    PRINT_SIZE(double );
    PRINT_SIZE(long double );       
    printf("\t El Numero aleatorio de %u\n",get_random(5));
    for (i = 0 ; i < 10 ; i++)
    {
        printf("\tEl factorial de %u = %u\n",i,factorial(i));   
        printf("\tEl Numero aleatorio de %u = %u\n",i,get_random(0));   
    }
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 0      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 1)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 1    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 1
*/
#include <string.h>


/**
* \typedef len_varchar_vT;
* \details Descripcion detallada sobre la redefinicion de un tipo de dato primitivo.
* \brief Descripcion breve sobre la redefinicion de un tipo de dato primitivo.
* \version AAvBBdCC.
* \note nota.
* \warning mensaje de "warning". 
* \date day dayOfMonth de month, years. */
typedef int len_varchar_vT;
/**
* \struct truncate_varchar_sT; 
* \details    Descripcion detallada sobre el tipo de estructura.
* \brief      Descripcion breve sobre el tipo de estructura.
* Elementos que componen la union:
*  \li \ref truncate_varchar_item1;
*  \li \ref truncate_varchar_item2;
* \version AAvBBdCC.
* \note nota sobre la estructura.
* \warning mensaje de "warning". 
* \date day dayOfMonth de month, years.*/
typedef struct
{  
  char * name;/**<@brief descripcion breve del Item 2 de char type */
  len_varchar_vT len;    /**<@brief descripcion breve del Item 1 int type */
}truncate_varchar_sT;


#define truncate_varchar_END_LIST   {NULL,0}



const truncate_varchar_sT string_field[]={
    /* audit_log */
    {"timestamp", 14},
    {"user_id", 12},
    {"user_name", 64},
    {"view_description",80},
    {"action_description",100},
    {"details",300},
    /* cash */
    {"business_date",8},
    {"pod_short",10},
    /*change_purchase*/
    {"witness_employee_id",12},
    {"witness_employee_name",64},
    {"reception_date",8},
    {"reception_time",6},
    {"seal_number",30},
    {"despatch_number",30},
    {"comment",1000},
    {"register_ts",14},
    /**/
    truncate_varchar_END_LIST
};

len_varchar_vT truncate_varchar_getlen(const truncate_varchar_sT *plist, const char * namevarchar);

/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
  len_varchar_vT len;
  size_t i = 0;
  /*Nos paso mas de un arguemnto 
   * El 0 es el path/nombre del ejecutable
   */
  if( argn<2 ) exit(EXIT_SUCCESS);
  i = 1;
  while(i < (argn))
  {
    len = truncate_varchar_getlen(string_field,arg[i]);    
    
    printf("\t\t La longitud del campo es %i\n",\
    (len == -1) ? 0xFFFF : len );  
    i++;
  }
  
  /*
  if(len > 0)
  {
      printf("\t\t La longitud del campo es %i",len);
  }
  else
  {
    printf("\t\t El campo \"%s\" no localizado\n\n",arg[1]);
    
  }*/
  
  exit(EXIT_SUCCESS);
}


len_varchar_vT truncate_varchar_getlen(const truncate_varchar_sT *plist, const char * namevarchar)
{
  if((namevarchar == NULL)||(plist == NULL)) return 0;
  while(plist->name != NULL )
  {
    if( strcmp(plist->name,namevarchar) == 0)
    {
      return plist->len;
    }
    plist++;    
  }
  return -1;
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 1      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 2)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 2    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 2
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
        
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 2      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__main_version__ == 3)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin apis main version 3    ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 3
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
        
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End apis main version 3      ]────────────────────────  │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 4)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 4    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 4
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   

    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 4      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 5)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 5    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 5
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
     
    exit(EXIT_SUCCESS);
}

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 5      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 6)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 6    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 6
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/    
int main(int argn,char **arg)
{
      
    exit(EXIT_SUCCESS);
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 6      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/
#elif (__main_version__ == 7)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 7    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 7
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{   
   
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 7      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 8)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 8    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 8
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 8      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 9)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 9    ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 9
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 9      ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 10)
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin apis main version 10   ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: main version 10
*/
/*
 *
 * *******************************************************************
 * @brief funcion principal 
 * @param argn numeros de Arguemtnos por defecto es '1' 
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error 
 * 
 *********************************************************************/
int main(int argn,char **arg)
{       
    exit(EXIT_SUCCESS);    
}
/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End apis main version 10     ]────────────────────────  │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*/

#elif (__main_version__ == 11)


#warning "Version no definida aun"
#else /* #if (__main_version__ == 1) */
#warning "__main_version__ mal definido"
#endif /* #if (__main_version__ == 1) */
/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End main Definition ]============================
 *
 * ********************************************************************************************
 * 
 */



/*
 * 
 * *********************************************************************************************
 *
 * ===========================[ End APIs Definitions ]============================
 *
 * ******************************************************************************************** 
 * 
 * 
 */
