##
##
######################################################################################################
##
## ---------------------------------------------------------------------------------------------------
## *****************************[+ ESTABLECEMOS EL PROYECTO A COMPILAR +]*****************************
## ---------------------------------------------------------------------------------------------------
##
######################################################################################################
#PROJECT ?=
# PROJECT ?= cmd_check
#PROJECT ?= template
# PROJECT ?= nesting_if
# PROJECT ?= keypad
# PROJECT ?= tokens
PROJECT ?= test_complejo


##
##
## #############################################################################
## 
## Habilitasmo/Deshabilitamos la funcionalidad de DEBUG -> GBD
##
## #############################################################################
# DEBUG_ENABLE ?= FALSE

##
##
## #############################################################################
## 
## Establecemos el navegador con el cual deseamos abrir la documentacion
##
## #############################################################################
DOC_VIEW ?= google-chrome
# DOC_VIEW ?= firefox



##
##
## #############################################################################
## 
## Establecemso el listado de argumetos a pasarle a la aplicacion "for make run"
##
## #############################################################################
VIEW_COMPILE_INFO = YES



##
##
## #############################################################################
## 
## Definimos el compilador para los archivos fuentes *.c /*.cpp
##	>> CC compiler c		*.c
##	>> CCPP compiler c++		*.cpp
##	>> CASM compiler asembly	*.S
##	>> LD   linker command   
##
##	+ gcc (1)              - GNU project C and C++ compiler
##	+ g++ (1)              - GNU project C and C++ compiler
##	+ as (1)               - the portable GNU assembler.
##	+ ld (1)               - The GNU linker
##
## #############################################################################
CC ?= gcc 
CCPP ?= g++
CASM ?= as
LD ?= ld 
##
##
################################################################################
##
## Flags para el ingreso de valor por Consola, al llamar make {lista Arguemntos}.
## Listado de variables para editar los falgs con el llamado del make
## example :
## 		make new CFLAGS=<flags>
## CFLAGS : 
##		make new CFLAGS=" -g -DNDEBUG"
##
################################################################################ 
CCFLAGS ?= $(CFLAGS)
CCPPFLAGS ?= $(CPPFLAGS)
CASMFLAGS ?= $(ASMFLAGS)
SYMBOLS ?= $(SYM)




###
###
######################################################################################################
##
## ---------------------------------------------------------------------------------------------------
## **********************[+ incluimos el archivo de configuracion del proyecto +]*********************
## ---------------------------------------------------------------------------------------------------
##
######################################################################################################
-include projects/$(PROJECT)/config.mk


SYMBOLS += $(PROJECT_SYMBOLS)
CCFLAGS += $(PROJECT_CCFLAGS)
CCPPFLAGS += $(PROJECT_CCPPFLAGS)
CASMFLAGS += $(PROJECT_CASMFLAGS)
##
##
################################################################################
##
## Establecemos la extencion de los archivos assembler a compilar, en funcion
## del Compiler Assembler seleccionado {as;nasm}
##
################################################################################ 
##
ifeq ($(CASM),as)
EXT_ASM := S
else ifeq ($(CASM),nasm)
EXT_ASM := s
endif

ifeq ($(DEBUG_ENABLE),TRUE)
APP_NAME = debug_app.out
CCFLAGS += -g -DNDEBUG
MSG_DEBUG = DEBUG ENABLE
else
MSG_DEBUG = DEBUG DISABLE
endif
