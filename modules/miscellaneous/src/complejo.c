#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <stdarg.h>
#include <dynamic_memory.h>
#include <complejo.h>


/* R +j R = LENG*2 + 4 + '\0' 
 * 
 *  1-Byte / 8-Bits    ->  256              3-Digitos
 *  2-Byte / 16-Bits   ->  65536            5-Digitos
 *  4-Byte / 32-Bits   ->  4294967296       10-Digitos
 *  8-Byte / 64-Bits   ->  1,844674407×10¹⁹ 20-Digitos
 * 16-Byte / 128-Bits  ->  3,402823669×10³⁸ 20-Digitos 
 * 
FLT_RADIX				2
FLT_MANT_DIG				24
FLT_DIG				6
FLT_MIN_EXP				-125
FLT_MIN_10_EXP				-37
FLT_MAX_EXP				128
FLT_MAX_10_EXP				38
FLT_MIN				1.17549435E-38F
FLT_MAX				3.40282347E+38F
FLT_EPSILON				1.19209290E-07F

DBL_MANT_DIG				53
DBL_DIG				15
DBL_MIN_EXP				-1021
DBL_MIN_10_EXP				-307
DBL_MAX_EXP				1024
DBL_MAX_10_EXP				308
DBL_MAX				1.7976931348623157E+308
DBL_MIN				2.2250738585072014E-308
DBL_EPSILON				2.2204460492503131E-016

*/
#if ( complejo_PRECISION_LEVEL == 0)
const complejo_sizearr_vT str_len = ((6 + 3 + 4) * 2) + 5 + 5 /*[rad] | [°]*/;
#elif(complejo_PRECISION_LEVEL == 1 )
const complejo_sizearr_vT str_len = ((FLT_MANT_DIG + 3 + FLT_DIG) * 2) + 5;
#elif(complejo_PRECISION_LEVEL == 2 )
const complejo_sizearr_vT str_len = ((DBL_MANT_DIG + 3 + DBL_DIG) * 2) + 5;

#else 
#warning "No se definio de forma correcta complejo_PRECISION_LEVEL dentro de complejo.h"
const complejo_sizearr_vT str_len = (sizeof(complejo_vT) * 6/* 3*2 */) + 5;
#endif /* if(complejo_PRECISION_LEVEL == 0 )*/



static void z_inv(complejo_sT * z)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse */  
  /*inversion 1/z */
  complejo_change(z,complejo_POLAR_rad);
  if(z->real == 0) return;
  z->real = (1.0/z->real);
  z->img = (z->img * (-1.0));  
}

static void z_conj(complejo_sT * z)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse */
  /* op '~' conjugado z */
  complejo_change(z,complejo_RECT);
  z->img = (z->img * (-1.0));  
}


static void z_neg(complejo_sT * z)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse */
  /* op '-' negacion z */
  complejo_change(z,complejo_RECT);
  z->real = (z->real * (-1.0));
  z->img = (z->img * (-1.0));
}

/**
 * z1 = z2
 */
static void z_equ(complejo_sT *z1,complejo_sT *z2)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse */
  z1->real = z2->real;
  z1->img = z2->img;
  z1->form = z2->form;  
}

/*
 * z1 = z1 + z2
 */
static void z_add(complejo_sT *z1,complejo_sT *z2)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse.
   * El tema de mantener el formato lo debe hacer quien invoca a la funcion
   * para ambos argumentos y debe considerarse que el resultado esta en 
   * z1.
   */
  complejo_change(z1,complejo_RECT);
  complejo_change(z2,complejo_RECT);
  z1->real += z2->real;
  z1->img += z2->img; 
}
/*
 * z1 = z1 - z2
 */
static void z_sub(complejo_sT *z1,complejo_sT *z2)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse.
   * El tema de mantener el formato lo debe hacer quien invoca a la funcion
   * para ambos argumentos y debe considerarse que el resultado esta en 
   * z1.
   */
  complejo_change(z1,complejo_RECT);
  complejo_change(z2,complejo_RECT);
  z1->real -= z2->real;
  z1->img -= z2->img; 
}
/*
 * z1 = z1 * z2
 */
static void z_mul(complejo_sT *z1,complejo_sT *z2)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse.
   * El tema de mantener el formato lo debe hacer quien invoca a la funcion
   * para ambos argumentos y debe considerarse que el resultado esta en 
   * z1.
   */
  complejo_change(z1,complejo_POLAR_rad);
  complejo_change(z2,complejo_POLAR_rad);
  z1->real = z1->real * z2->real;
  z1->img += z2->img; 
}

/*
 * z1 = z1 / z2
 */
static void z_div(complejo_sT *z1,complejo_sT *z2)
{
  /* FIXME: Es de uso interno por lo que no tiene conprobaciones esta
   * deben hacerse.
   * El tema de mantener el formato lo debe hacer quien invoca a la funcion
   * para ambos argumentos y debe considerarse que el resultado esta en 
   * z1.
   */
  complejo_change(z1,complejo_POLAR_rad);
  complejo_change(z2,complejo_POLAR_rad);
  if((z1->real == 0) || (z2->real == 0))
  {
    z1->real = 0;
    z1->img = 0;
    return;
  }
  z1->real = z1->real / z2->real;
  z1->img -= z2->img; 
}
/*
      float powf(float x, float y);
      double pow(double x, double y);
      long double powl(long double x, long double y);
      
      float sqrtf(float x);
      double sqrt(double x);
      long double sqrtl(long double x);
      
      float sinf(float x);
      double sin(double x);      
      long double sinl(long double x);
      
      float cosf(float x);
      double cos(double x);      
      long double cosl(long double x);
      
      float atanf(float x);
      double atan(double x);      
      long double atanl( long double x);
      
      double fabs(double x);
      float fabsf(float x);
      long double fabsl(long double x);
 */

/**
* 
* ********************************************************************************
* \def FLOAT_IS_ZERO(Value);
*
* \brief Macro para consultar si un numero del tipo real es o no Cero.
* \param Value : Valor/variable sobre la cual se desea consultar.
* \return 
*   + FALSE , No es Cero
*   + TRUE, Es cero
* \version AAvBBdCC.
* \note nota.
* \warning mensaje de "warning".
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
</PRE> 
*********************************************************************************/ 
#define FLOAT_IS_ZERO(Value) fpclassify(Value) == FP_ZERO
#define complejo_COPY(pZ1,pZ2) \
{\
  pZ1->real = pZ2->real;\
  pZ1->img = pZ2->img;\
  pZ1->form = pZ2->form;\
}

#define z_SET_VALUE(pZ1,Real,Img,Form) \
{\
  pZ1->real = Real;\
  pZ1->img = Img;\
  pZ1->form = Form;\
}



#if ( complejo_PRECISION_LEVEL == 0)
/* snprintf(num->str,str_len,"%f  < %f",num->real,num->img);  */
#define buff_printf_polar(pNstr,Unidad) \
  snprintf(pNstr->str,str_len,"%f  < %f %s",pNstr->real,pNstr->img,Unidad)  
  
/* snprintf(num->str,str_len,"%f %cj %f",num->real,signo,value);  */
#define buff_printf_rectang(pNstr,Signo,Value) \
  snprintf(pNstr->str,str_len,"%f %cj %f",pNstr->real,Signo,Value)

#define POW(Base,Exp)   powf(Base,Exp)
#define SQRT(Base)      sqrtf(Base)
#define SIN(Rad)        sinf(Rad)
#define COS(Rad)        cosf(Rad)
#define ATAN(Rad)       atanf(Rad)
#define FABS(Val)       fabsf(Val)
  
  
  
  
#elif(complejo_PRECISION_LEVEL == 1 )
#define buff_printf_polar(pNstr,Unidad) \
  snprintf(pNstr->str,str_len,"%lf  < %lf %s",pNstr->real,pNstr->img,Unidad)

#define buff_printf_rectang(pNstr,Signo,Value) \
  snprintf(pNstr->str,str_len,"%lf %cj %lf",pNstr->real,Signo,Value)
  
#define POW(Base,Exp)   pow(Base,Exp)
#define SQRT(Base)      sqrt(Base)
#define SIN(Rad)        sin(Rad)
#define COS(Rad)        cos(Rad)
#define ATAN(Rad)       atan(Rad)
#define FABS(Val)       fabs(Val)
  

#elif(complejo_PRECISION_LEVEL == 2 )
  
#define buff_printf_polar(pNstr,Unidad) \
  snprintf(pNstr->str,str_len,"%llf  < %llf %s",pNstr->real,pNstr->img,Unidad)

#define buff_printf_rectang(pNstr,Signo,Value) \
  snprintf(pNstr->str,str_len,"%llf %cj %llf",pNstr->real,Signo,Value)
  
#define POW(Base,Exp)   powl(Base,Exp)
#define SQRT(Base)      sqrtl(Base)
#define SIN(Rad)        sinl(Rad)
#define COS(Rad)        cosl(Rad)
#define ATAN(Rad)       atanl(Rad)
#define FABS(Val)       fabsl(Val)
  
#endif /* if(complejo_PRECISION_LEVEL == 0 )*/




char * complejo_getstr( complejo_sT * num)
{
  char signo;
  complejo_vT value;
  if(num == NULL) return NULL;

  if(num->str == NULL)
  {    
    num->str = XMALLOC(str_len);
  }
  
  if(num->form == complejo_POLAR)
  {
#if 0
    switch(sizeof(complejo_vT))
    {
      /* int snprintf(char *str, complejo_sizearr_vT size, const char *format, ...);  */
      default:
      case 4: /* 4-Bytes */
        snprintf(num->str,str_len,"%f  < %f",num->real,num->img);
        return num->str;
      case 8: /* 4-Bytes */
        snprintf(num->str,str_len,"%lf  < %lf",num->real,num->img);
        return num->str;
      case 16: /* 4-Bytes */
        snprintf(num->str,str_len,"%llf  < %llf",num->real,num->img);
        return num->str;
    }   
#else

    buff_printf_polar(num,"[°]");
    return num->str;
    
#endif
  }    
  if(num->form == complejo_POLAR_rad)
  {
    buff_printf_polar(num,"[rad]");
    return num->str;
  }
  
   /* Formato Rectangular */ 
  if( num->img < 0.0 )
  {       
    value = num->img*(-1.0);
    signo = '-';    
  }
  else
  {
    value = num->img;
    signo = '+'; 
  }
#if 0  
  switch(sizeof(complejo_vT))
  {
    /* int snprintf(char *str, complejo_sizearr_vT size, const char *format, ...);  */
    default:
    case 4: /* 4-Bytes */
      snprintf(num->str,str_len,"%f %cj %f",num->real,signo,value);
      return num->str;
    case 8: /* 4-Bytes */
      snprintf(num->str,str_len,"%lf %cj %lf",num->real,signo,value);
      return num->str;
    case 16: /* 4-Bytes */
      snprintf(num->str,str_len,"%llf %cj %llf",num->real,signo,value);
      return num->str;
  }
#else
  buff_printf_rectang(num,signo,value);
  return num->str;
  
#endif
  return NULL;
}

// const char* token[]={"+j","-j","<"};


int complejo_free(complejo_sT * num,bool_t all)
{
  if (num == NULL ) return -1;
  if (num->str != NULL)
  {
    free(num->str);  
  }  
  if(all)
  {
    free(num);
  }
  return 0;
}

int complejo_free_arr(complejo_sT ** arzi, complejo_sizearr_vT len)
{
  complejo_sizearr_vT i;
  if((arzi == NULL)||(len == 0)) return -1;
  /*-- debemos libear la memoria*/   
  for( i = 0; i<len ; i++)
  {
    complejo_free(arzi[i],TRUE);    
  }
  free(arzi);
  return 0;
}


/*
 * 
 */
complejo_sT * complejo_new(const char *str)
{  
  
  complejo_sT * pr;
  pr = XMALLOC(sizeof(complejo_sT));
  if( str == NULL )
  {
    pr->real = 0.0;
    pr->img = 0.0;
    pr->form = complejo_RECT;
  }
  else
  {
    if(complejo_set_withstr(pr,str) == NULL)
    {
      /* Si no lo pudo convertir */
      pr->real = 0.0;
      pr->img = 0.0;
      pr->form = complejo_RECT;
    }
  }  
  pr->str = NULL;
  return pr;
}



complejo_sT ** complejo_new_arr(const char **str,complejo_sizearr_vT *plen)
{
  complejo_sizearr_vT i,len;
  /**/  
  len = 0 ;
  while(len++, str[len] != NULL)/* descartamos el primero, de otra forma debemo restar -1 */;
  /**/
  complejo_sT **zi = XMALLOC(sizeof(complejo_sT *) * (len));  
  /*-- Reservamos memoria para cada puntero del array de punteros */  
  for( i = 0; i< len ; i++)
  {
    zi[i] = complejo_new(str[i]);
  }
  *plen = len;
  return zi;  
}






/*
 *  Todas estas APIs retornan una estructura alocada  
 */
complejo_sT * complejo_clone(complejo_sT * z)
{
  complejo_sT * pr;
  if(z == NULL ) return NULL;
  pr = XMALLOC(sizeof(complejo_sT));
  /* copiamos la estructura */
  complejo_COPY(pr,z);
  return pr;
}



/*
 * op '!' inversion 1/z
 * op '~' conjugado z
 * op '-' negacion z
 */
#if 0
complejo_sT * complejo_clone_selfoperation(complejo_sT * z, char op,bool_t keepform)
{
  complejo_sT * pr;  
  if(z == NULL ) return NULL;
  pr = XMALLOC(sizeof(complejo_sT));
  complejo_COPY(pr,z);
  switch(op)
  {
    default:
      free(pr);
      return NULL;
    case '!': /* op '!' inversion 1/z */
      z_inv(pr);
      /*
      complejo_change(pr,complejo_POLAR_rad);
      pr->real = (1.0/pr->real);
      pr->img = (pr->img * (-1.0)); */
      break;
      
    case '~': /* op '~' conjugado z */
      z_conj(pr);/*
      complejo_change(pr,complejo_RECT);  
      pr->img = (pr->img * (-1.0));*/
      break;      
      
    case '-': /* op '-' negacion z */
      z_neg(pr);/*
      complejo_change(pr,complejo_RECT);  
      pr->real = (pr->real * (-1.0));
      pr->img = (pr->img * (-1.0));*/
      break;      
  }
  if(keepform)
    complejo_change(pr,z->form);  
  return pr;   
}
#endif


/**
* ******************************************************************************** 
* \fn complejo_sT * complejo_selfoperation(complejo_sT * z, char op,bool_t keepform);
*
* \brief Relaizar una operacion unaria sobre un numero complejo, y almacena el resultado 
* en dicho numero.
* \param z : Numero complejo donde realizaremos la operacion.
* \param op : Operacion unaria, should be:* 
*   \li op '!' inversion 1/z
*   \li op '-' negacion z
*   \li op '~' conjugado z
* 
* \param keepform : Con esta variable indicamos si deseamos o no mantener el formato despues 
* de la operacion.
*   \li TRUE : Mantiene el formato con el cual fue llamado.
*   \li FALSE : Queda con el formato \ref complejo_POLAR_rad.
* 
* \return puntero al numero complejo donde se realizo la operacion, este es el mismo que el 
*  pasado como arguento. E caso de que no se pueda realizar la operacion retorna NULL.
*      \li != NULL, success
*      \li NULL, failure
* \version .
* \note nota.
* \warning mensaje de "warning". 
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>

</PRE>  
*********************************************************************************/  
complejo_sT * complejo_selfoperation(complejo_sT * z, char op,bool_t keepform, bool_t clone)
{
#if 0  
  complejo_sT r;  
  if(z == NULL ) return NULL;  
  r.img = z->img;
  r.real = z->real;
  r.form = z->form;
  /**/
  switch(op)
  {
    default:
      return NULL;
    case '!': /* op '!' inversion 1/z */
      z_inv(&r);/*
      complejo_change(&r,complejo_POLAR_rad);
      r.real = (1.0/r.real);
      r.img = (r.img * (-1.0)); */
      break;
      
    case '~': /* op '~' conjugado z */
      z_conj(&r);/*
      complejo_change(&r,complejo_RECT);  
      r.img = (r.img * (-1.0));*/
      break;      
      
    case '-': /* op '-' negacion z */
      z_neg(&r);/*
      complejo_change(&r,complejo_RECT);  
      r.real = (r.real * (-1.0));
      r.img = (r.img * (-1.0));*/
      break;      
  }
  if(keepform)
    complejo_change(&r,z->form);  
  z->real = r.real;
  z->img = r.img;
  z->form = r.form;
  return z;   
  
#else
  /*-- otra opcion aprovecahndo las bondades de las funciones estaticas */
  complejo_eT form;  
  complejo_sT * zr;
  if(z == NULL ) return NULL;  
  if((((op != '!')&&(op != '-')))&&(op != '~')) return NULL;
  if(clone)
  {
    zr = XMALLOC(sizeof(complejo_sT));
    complejo_COPY(zr,z);    
  }
  else
  {
    zr = z;
  }
  form = z->form;
  /**/
  switch(op)
  {
    default:
      return NULL;
    case '!': /* op '!' inversion 1/z */
      z_inv(zr);
      break;
      
    case '~': /* op '~' conjugado z */
      z_conj(zr);
      break;      
      
    case '-': /* op '-' negacion z */
      z_neg(zr);
      break;      
  }
  if(keepform)
    complejo_change(zr,form);  
  return zr;   
#endif  
}


/*
 * FIXME: debemos corregir :
 *    >> "<-89.9887[rad", Esto genera errores matematicos, debemos establecerlo como 0.0000001 < -89.9887 [rad]
 *        Por cada definicion de tipo de numero real, debemos tener un valor minimo el cual debe ser colocado aqui.
 * 
 *    >> -1 "<180", devemos contemplar a la hora de crear ??, podemos corregirlos 0.000000 y el otro mod()
 * 
 *    >> La consulta mediante strtod, sirve pero no debemos consultar si es NULL el segundo Arg si no 
 *       si esta vacio, strlen == 0
 * 
 *    Debemos volver a probar partiendo desde main version 0/1.
 */
complejo_sT * complejo_set_withstr( complejo_sT * num, const char *str)
{
  /*
   * FIXME debemos contemplar el error de pasar:
   *    >> "0.01<-89.9887[rad]", para el cual es un error de interpretacion matematico
   *        ya que no deberia ser mayo a 2*M_PI si esta en [rad].
   *        Solucion:
   *        img = (Agulo[rad] > 2M_PI)? 2M_PI : Angulo[rad]
   *        img = (Agulo[°] > 360.00)? 360.00 : Angulo[°]
   * 
   *    >> "-10<89.9887[rad]", El modulo nunca puede ser negativo, Por lo que es psado por FABS().
   */
  char *pnreal = NULL ,*pnimg = NULL, *pbuff;
  complejo_vT factor;
  char *pstr_conv;
  char signo;
//   if((num == NULL)||(str == NULL)) return -1;
  if((num == NULL)||(str == NULL)) return NULL;
  num->str = NULL;
  /**/
  pbuff = strdup(str);
  do{
    /* Buscamos el distinguir el numero real  con " +j " */
    pnreal = stdlib_str_findtoken(pbuff,"+j",&pnimg);
    if( pnreal != NULL)
    {
      pbuff[pnreal-pbuff] = '\0';  
      signo = '+';
      factor = 1.0;
      break;
    }
    /* Buscamos el distinguir el numero real  */
    pnreal = stdlib_str_findtoken(pbuff,"-j",&pnimg);
    if( pnreal != NULL)
    {
      pbuff[pnreal-pbuff] = '\0';
      signo = '-';
      factor = -1.0;
      break;
    }
    /* Buscamos el distinguir el numero real  */
    pnreal = stdlib_str_findtoken(pbuff,"<",&pnimg);
    if( pnreal != NULL)
    {
      pbuff[pnreal-pbuff] = '\0';
      signo = 'p';
      break;
    }
    /* no encontro ninguno:, debemos tratar de transformar el string 
     * a numero, puede ser que sea un modulo con 0° */
    factor = strtod(pbuff,&pstr_conv);
    if((pstr_conv == NULL)||(strlen(pstr_conv) == 0))
    {
      num->real= factor;
      num->img = 0.0;
      num->form = complejo_RECT;
      free(pbuff);
      return num;
//       return 0;
    }
    printf("\tError al convertir, remainder = %s \t",pstr_conv);
    
    free(pbuff);
    return NULL;
//     return -1;
  }while(0);
  /*-- aca debemos armar el numero: */ 
  /*printf("Real = %s \t signo = %c \t Imaginario = %s\n",pbuff,signo,pnimg);*/
  if(signo == '+' || signo == '-')
  {
    /* Formato rectangular */
#if 0
    num->real= strtod(pbuff,&pstr_conv);
    if(pstr_conv != NULL) return -1;
    num->img = strtod(pnimg,&pstr_conv);
    if(pstr_conv != NULL) return -1;
#else
    num->real= strtod(pbuff,NULL);
    if(strlen(pnimg) == 0)
    {
      num->img = factor;
    }
    else
    {
      num->img = strtod(pnimg,NULL) * factor;      
    }    
    num->form = complejo_RECT;
    /*salimos conversion exitosa.*/
    free(pbuff);  
    return num;
//     return 0;
#endif       
  }

  if(signo == 'p')
  {
    
    num->real = strtod(pbuff,NULL);
    /*-- Almacenamos el Modulo
     *   nos aseguramos de insertar un modulo como modulo */
    num->real = FABS(num->real);
    /*-- Almacenamos el Angulo */
    num->img = strtod(pnimg,&pstr_conv);
    do
    {      
      if( stdlib_str_findtoken(pstr_conv,"rad",NULL) != NULL)
      {
        while(FABS(num->img) > 2*M_PI)
        {
          /* FIXME debemos reducirlo, restandole el valor 360 mientras sea mayor */
//           num->img = (num->img > 0)? 2*M_PI : -2*M_PI;          
          num->img += (num->img > 0)? (-2*M_PI) : (2*M_PI);
          
        }
        num->form = complejo_POLAR_rad;
        break;
      }
      if( stdlib_str_findtoken(pstr_conv,"Rad",NULL) != NULL)
      {
        while(FABS(num->img) > 2*M_PI)
        {
          /* FIXME debemos reducirlo, restandole el valor 360 mientras sea mayor */
//           num->img = (num->img > 0)? 2*M_PI : -2*M_PI;
          num->img += (num->img > 0)? (-2*M_PI) : (2*M_PI);
        }
        num->form = complejo_POLAR_rad;
        break;
      }
      while(FABS(num->img) > 360.0)
      {
        /* FIXME debemos reducirlo, restandole el valor 360 mientras sea mayor */
//           num->img = (num->img > 0)? 360.0 : -360.0;
          num->img += (num->img > 0)? (-360.0) : (360.0);
      }
      num->form = complejo_POLAR;
      break;   
    }while(0);
      
    //     num->img = strtod(pnimg,NULL);
    /*salimos conversion exitosa.*/
    free(pbuff);  
    return num;
//     return 0;    
  }

  if(pnimg == NULL) puts("pnimg Nulo");
  /*
  if(strlen(pnimg) == 0)
  {
    puts("pnimg vacio");
    pnimg = '1';    
  } */  
  free(pbuff);  
  return num;
//   return 0;
}


/*
 */
complejo_sT * complejo_set_withvalue( complejo_sT * num,complejo_vT v1,complejo_vT v2, complejo_eT form)
{
  if(num == NULL)
  {
    complejo_sT *pr = XMALLOC(sizeof(complejo_sT));
    pr->real = v1;
    pr->img = v2;
    pr->form = form;
    pr->str = NULL;
    return pr;
  }
  num->real = v1;
  num->img = v2;
  num->form = form;
  num->str = NULL;
  return num;
}




/*
 * 
 */
complejo_sT * complejo_change(complejo_sT * num ,complejo_eT formtoconv )
{
  unsigned int tmp;
  complejo_eT f;  
  complejo_vT x,y;
//   if(num == NULL) return -1;
  if(num == NULL) return NULL;
//   if((num->real == 0) && (num->img == 0)) return 0;
  if((num->real == 0) && (num->img == 0)) return num;
//   if(num->form == formtoconv) return 0;
  if(num->form == formtoconv) return num;
/*
 *  Actual                    Cambio Requerido                OR
 * 
 * complejo_POLAR [1]          complejo_POLAR_rad [2]         0x01|0x20 = 0x21
 * complejo_POLAR [1]          complejo_RECT [4]              0x01|0x40 = 0x41
 * complejo_POLAR_rad [2]      complejo_POLAR [1]             0x02|0x10 = 0x12
 * complejo_POLAR_rad [2]      complejo_RECT [4]              0x02|0x40 = 0x42
 * complejo_RECT [4]           complejo_POLAR [1]             0x04|0x10 = 0x14
 * complejo_RECT [4]           complejo_POLAR_rad [2]         0x04|0x20 = 0x24
 * 
 */
  tmp = (unsigned int) (num->form | ((formtoconv<<4)&0xF0));
  switch(tmp)
  {
    case 0x21:
      /* polar [°] -> polar [rad] */
      f = complejo_POLAR_rad;
      x = num->real;
      y = num->img * (M_PI/180);
      break;
    case 0x41:
      /* polar [°] -> rectangular */
      f = complejo_RECT;
      x = num->real * COS(num->img*(M_PI/180));
      y = num->real * SIN(num->img*(M_PI/180));
      break;
    case 0x12:
      /* polar [rad] -> polar [°] */
      f = complejo_POLAR;
      x = num->real;
      y = num->img * (180*M_1_PI);
      break;
    case 0x42:
    /* polar [rad] -> rectangular  0x02|0x40 = 0x42*/
      f = complejo_RECT;
      x = num->real * COS(num->img);
      y = num->real * SIN(num->img);
      break;
    case 0x14:
    case 0x24:
    /* rectangular  -> polar [rad]  0x04|0x10 = 0x14*/
      f = complejo_POLAR_rad;
      if((num->real == 0)&&(num->img != 0))
      {
        /*-- caso x = 0 ==> Z1 = +j y = |y| < (y>0)? 90:270; */
        x = FABS(num->img);        
        y = (num->img > 0)? (M_PI_2):(M_PI_2*3);
        break;
      }
      if((num->real != 0)&&(num->img == 0))
      {
        /*-- caso y = 0 ==> Z1 = x = |x| < (x>0)? 0:180; */
        x = FABS(num->real);
        y = (num->real > 0) ? 0: M_PI;
        break;
      }      
      /*-- Obtenemos el Modulo  */
      x = SQRT(POW(num->real,2) + POW(num->img,2));    
      /*-- Obtenemos la Fase */      
      y = ATAN(num->img/num->real) ;
      if( num->real < 0)
      {               
        if(num->img < 0) y += M_PI;
        else y -= M_PI;
      }
      break;   
    /*-- El modo pedido no esta definido */    
    default:
      return NULL;
      
  }
  if(tmp == 0x14) 
  {
    f = complejo_POLAR;
    num->img = y * (180/M_PI);
  }
  else
  {
    num->img = y;
  }  
  num->real = x;  
  num->form = f;
  return num;
}

/*
 * 
 */
complejo_sT * complejo_operation(complejo_sT * zr, complejo_sT * z1, complejo_sT * z2, char operation)
{
  complejo_sT * pzr;
  complejo_eT /*f,*/fz1,fz2;
//   complejo_vT x,y;
  /*-- inicio de la logica */
  if((z1 == NULL)||(z2 == NULL)) return NULL;
  /*-- para evitar reservar memoria si lo es requerido*/
  if(((operation != '+') && (operation == '-')) && \
     ((operation != '*') && (operation == '/'))) return NULL;
  if(zr == NULL)
  {
    pzr = XMALLOC(sizeof(complejo_sT));  
  }
  else
  {
    pzr = zr;
  }  
  /*-- Salvamos el formato en el que esta cada numero z1 y z2 */
  fz1 = z1->form;
  fz2 = z2->form;  
  /*-- Realizamos el calculo temporal */
  switch(operation)
  {
    case '+':
        /*-- pasamos ambos al formato rectangular*/
        z_equ(pzr,z1);  /* pzr = z1  */ 
        z_add(pzr,z2);  /* pzr += z2 */
//         f = complejo_RECT;
        /*
        complejo_change(z1,complejo_RECT);
        complejo_change(z2,complejo_RECT);
        f = complejo_RECT;
        x = z1->real + z2->real;
        y = z1->img + z2->img ; */
        break;
    case '-':
        /*-- pasamos ambos al formato rectangular*/
        z_equ(pzr,z1);  /* pzr = z1  */ 
        z_sub(pzr,z2);  /* pzr -= z2 */
//         f = complejo_RECT;
        /*
        complejo_change(z1,complejo_RECT);
        complejo_change(z2,complejo_RECT);
        f = complejo_RECT;
        x = z1->real - z2->real;
        y = z1->img - z2->img ; */
        break;
    case '*':
        /*-- pasamos ambos al formato Polar [rad] */
        z_equ(pzr,z1);  /* pzr = z1  */ 
        z_mul(pzr,z2);  /* pzr *= z2 */
//         f = complejo_POLAR_rad;
        /*
        complejo_change(z1,complejo_POLAR_rad);
        complejo_change(z2,complejo_POLAR_rad);
        f = complejo_POLAR_rad;
        x = z1->real * z2->real;
        y = z1->img + z2->img ;*/ 
        break;
    case '/':
        /*-- pasamos ambos al formato Polar [rad] */
        z_equ(pzr,z1);  /* pzr = z1  */ 
        z_div(pzr,z2);  /* pzr /= z2 */
//         f = complejo_POLAR_rad;
        /*
        complejo_change(z1,complejo_POLAR_rad);
        complejo_change(z2,complejo_POLAR_rad);
        f = complejo_POLAR_rad;
        if(FLOAT_IS_ZERO(z1->real) || FLOAT_IS_ZERO(z2->real) )
        {
          x = 0.0;
          y = 0.0;
          break;
        }
        x = z1->real / z2->real;
        y = z1->img - z2->img ; */
        break;
        /**/        
//     default:
//       return NULL;
  } 
  /*-- restauramos el formato de cada operando */
  complejo_change(z1,fz1);
  complejo_change(z2,fz2);
  return pzr;
#if 0  
  /*-- Consultamos si el resultado lo almacenamos en una estructura reservada */
  if(zr == NULL)
  {
    pzr = XMALLOC(sizeof(complejo_sT));
    /*-- iniciamos la suma y la almacenamos */
    pzr->real = x;
    pzr->img = y;
    pzr->form = f;
    pzr->str = NULL;
    return pzr;    
  }
  /*-- retormanos el resultado en zr*/
  zr->real = x;
  zr->img = y;
  zr->form = f;
  zr->str = NULL;
  return zr; 
#endif  
}





char * stdlib_str_findtoken(const char *pstr,char *token,char **premaind)
{
  complejo_sizearr_vT len_pstr,len_token,i,j;
  if((token == NULL)||(pstr == NULL)) return NULL;
  len_pstr = strlen(pstr);
  len_token = strlen(token);
  /* la longitud del token es mayor que la de str */
  if(len_token > len_pstr) return NULL;
  j=0;
  for(i = 0 ; i< len_pstr ; i++)
  {
    if(pstr[i] == token[j])
    {
      if(++j < len_token)
      {
        continue;
      }
      else
      {
        //-- encontramos la secuencia completa de token
#if 0      
        if((i+j)<len_pstr)
        {
          *premaind = (char*) &pstr[i+1];          
        }
        else
        {
          *premaind = NULL;
        }
#else
        if(premaind != NULL) 
          *premaind = (char*) &pstr[i+1]; 
#endif
        return (char *) (&pstr[(i-j)+1]);
      }      
    }
    else
    {
      j=0;
    }  
  }
//   puts("Salimos por aqui???");
  return NULL;  
}




/*
 *
 * *******************************************************************
 * @brief funcion principal
 * @param argn numeros de Arguemtnos por defecto es '1'
 * @param arg array de arguemento, arg[0] := path/appname
 * @return estado de la ejecucion
 *  + EXIT_SUCCESS : ejecucion finalizada de forma exitosa
 *  + EXIT_FAIL : ejecucion finalizada con error
 *
 *********************************************************************/
complejo_sT * complejo_sumatoria(unsigned int numargs, ... )
{
#if 0
  complejo_sT *zt,*zi;
  complejo_vT x,y;
  va_list ap;
  /* Inicializamos los acumuladores */
  x = 0.0 , y = 0.0 ;
  zt = complejo_new(NULL);
  /* inicializamos el listado de arg, pasandole 
    * el primer arguemnto recibido en el listado
    */
  va_start(ap, numargs);
  while (numargs--) /* consulta y luego decrementa */
  {
    /* extraemos el zi */
    zi = va_arg(ap, complejo_sT* );
    /*-- debemos chequear si zi == NULL, para saltarlo */
    if(zi == NULL) continue;
    /* copiamos el zi localmente */
    zt->real = zi->real;
    zt->img = zi->img;
    zt->form = zi->form;
    /*-- cambiamos el formato */
    complejo_change(zt,complejo_RECT); 
    /* iniciamos la sumatoria */
    x += zt->real;
    y += zt->img;      
  }      
  zt->real = x;
  zt->img = y;
  va_end(ap);
  /*-- imprimimos la sumatoria */
  return zt;  
  
#else
  complejo_sT *zr,z1, *zi;
  va_list ap;
  /* Inicializamos 
   * zr el caumulador
   * z1 la copia de zi */  
  zr = complejo_new(NULL);
  //z1 = complejo_new(NULL);
  /*-- Iniciamos zr, con la indtidad de suma -> 0*/
  z_SET_VALUE(zr,0.0,0.0,complejo_RECT);
  /* inicializamos el listado de arg, pasandole 
    * el primer arguemnto recibido en el listado
    */
  va_start(ap, numargs);
  while (numargs--) /* consulta y luego decrementa */
  {
    /* extraemos el zi */
    zi = va_arg(ap, complejo_sT* );
    /*-- debemos chequear si zi == NULL, para saltarlo */
    if(zi == NULL) continue;
    /* copiamos el zi localmente */
    z_equ(&z1,zi);
    z_add(zr,&z1);    
  }      
  va_end(ap);
  /*-- imprimimos la sumatoria */
  return zr;    
#endif  
    
    
    
}


complejo_sT * complejo_sumatoriainv(unsigned int numargs, ... )
{
#if 0
complejo_sT *zt, *zi;
  complejo_vT x,y;
  va_list ap;
  /* Inicializamos los acumuladores */
  x = 0.0 , y = 0.0 ;
  zt = complejo_new(NULL);
  /* inicializamos el listado de arg, pasandole 
    * el primer arguemnto recibido en el listado
    */
  
  va_start(ap, numargs);
  while (numargs--) /* consulta y luego decrementa */
  {
    /* extraemos el zi */
    zi = va_arg(ap, complejo_sT* );
    /*-- debemos chequear si zi == NULL, para saltarlo */
    if(zi == NULL) continue;
    /* copiamos el zi localmente */
    zt->real = zi->real;
    zt->img = zi->img;
    zt->form = zi->form;      
    /*-- cambiamos el formato y realizamos la operacion */
    complejo_selfoperation(zt,'!',FALSE,FALSE);/*1/z*/
    complejo_change(zt,complejo_RECT); 
    /* iniciamos la sumatoria */
    x += zt->real;
    y += zt->img;      
  }      
  zt->real = x;
  zt->img = y;
  va_end(ap);
  /*-- imprimimos la sumatoria */
  return zt;  
#else
  complejo_sT *zr,z1,*zi;  
  va_list ap;
  /* Inicializamos los acumuladores */  
  zr = complejo_new(NULL);
  z_SET_VALUE(zr,0.0,0.0,complejo_RECT);
  /* inicializamos el listado de arg, pasandole 
    * el primer arguemnto recibido en el listado
    */  
  va_start(ap, numargs);
  while (numargs--) /* consulta y luego decrementa */
  {
    /* extraemos el zi */
    zi = va_arg(ap, complejo_sT* );
    /*-- debemos chequear si zi == NULL, para saltarlo */
    if(zi == NULL) continue;
    /* copiamos el zi localmente */
    z_equ(&z1,zi);
    z_inv(&z1);
    z_add(zr,&z1);     
  }        
  va_end(ap);
  /*-- imprimimos la sumatoria */
  return zr;  
#endif  
}





/**
 * op '!' inversion 1/z
 * op '~' conjugado z
 * op '-' negacion z
 * FIXME aque quedamos 
 */
complejo_sizearr_vT complejo_selfoperation_arr(complejo_sT **zi, complejo_sizearr_vT len, char op,bool_t keepform)
{
  complejo_sizearr_vT i;    
  if(( zi == NULL ) || ( len == 0 )) return -1;
  /*-- debemos libear la memoria*/   
  for( i = 0; i<len ; i++)
  {    
    complejo_free(zi[i],TRUE);    
  }
  free(zi);
  return 0;
}

