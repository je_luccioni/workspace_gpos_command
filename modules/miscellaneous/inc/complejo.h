#ifndef __complejo_h__
#define __complejo_h__
/**
* 
* *************************************************************************************
* \addtogroup complejo 
* @{ borreame -> }@
* \copyright  
* 2020, Luccioni Jesús Emanuel \n
* All rights reserved.\n 
* This file is part of complejo.h .\n
* Redistribution is not allowed on binary and source forms, with or without \n
* modification. Use is permitted with prior authorization by the copyright holder. &copy;
* \file complejo.h
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.    
* \brief Descripcion breve.
* \details Descripcion detallada.
*
* \version 01v01d01.
* \date Jueves 19 de Noviembre, 2020.
* \pre pre, condiciones que deben cuplirse antes del llamado, example: First
* initialize the system.
* \bug bug, depuracion example: Not all memory is freed when deleting an object 
* of this class.
* \warning mensaje de precaucion, warning.
* \note nota.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* @} doxygen end group definition 
* ********************************************************************************** */

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * =======================================[begin of project file]=======================================
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 */


/*
 *
 *
 * *********************************************************************************************
 *
 * =================================[ Begin include header file ]================================
 *
 * ********************************************************************************************
 *
 * TODO: include header file sector, "only in case especific"
 *
 */
#include <errno.h>
#include <typedef.h>
#include <stdio.h>






/*
 *
 * ============================[Open, cplusplus]============================
 *
 */
#ifdef __cplusplus
extern "C" {
#endif


/**
* 
* ********************************************************************************
* \def complejo_PRECISION_LEVEL 
* \brief Etiqueta para seleccionar el nivel de presicion para los calculos. 
* La velocidad y tamaño dependeran de la arquitectura para el cual se compile.
* Los valores de este pueden ser:
*   \li 0 Real de simple presiscion \b float .  
*   \li 1 Real de doble presiscion \b double .
*   \li 2 Real de doble doble presiscion \b long \b double .
* \version 01v01d01.
* \note nota.
* \warning El valor por defecto esta seteado a 0, para generar la menor cantidad de
* codigo y lograr mayor eficiencia a la hora de ejecucion. Es recomendado para
* sistemas con rescurso escasos. Para sobremesa se puede seleccionar 1 o 2
* dependiendo la aplicacion en la que se utilizara.
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
*********************************************************************************/ 
#define complejo_PRECISION_LEVEL  1 

/**
* 
* ********************************************************************************
* \def complejo_SIZE_ARRAY_LEVEL 
* \brief Etiqueta para seleccionar el nivel/capacidad de los Array con los que se 
* pueden trabajar.
* Los valores de este pueden ser:
*   \li 0 Soporte para Arrays del tamaños de 256 Numeros Complejos.
*   \li 1 Soporte para Arrays del tamaños de 64K (64*1024) Numeros Complejos.
*   \li 2 Soporte para Arrays del tamaños de 4G (4*K*K*K) Numeros Complejos.
*   \li 3 Soporte para Arrays del tamaños de 16 [G*G] Numeros Complejos.
* \version 01v01d01.
* \note nota.
* \warning El valor por defecto esta seteado a 0, para generar la menor cantidad de
* codigo y lograr mayor eficiencia a la hora de ejecucion. Es recomendado para
* sistemas con rescurso escasos. Para sobremesa se puede seleccionar 1 
* dependiendo la aplicacion en la que se utilizara, en casos extremos el 2 y muy 
* extremos el caso 3.
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
*********************************************************************************/ 
#define complejo_SIZE_ARRAY_LEVEL  0




/**
* \typedef complejo_sizearr_vT;
* \brief Redefinicion del tipo de dato que utilizaremos para el manejo de array 
* del tipo de datos \ref complejo_sT.
* \version 01v01d01.
* \note nota.
* \warning en caso de cambiar la redefinicion de este mensaje de "warning". 
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE> */
#if (complejo_SIZE_ARRAY_LEVEL == 0 )
typedef uint8_t complejo_sizearr_vT;
#elif (complejo_SIZE_ARRAY_LEVEL == 1 )
typedef uint16_t complejo_sizearr_vT;
#elif (complejo_SIZE_ARRAY_LEVEL == 2 )
typedef uint32_t complejo_sizearr_vT;
#elif (complejo_SIZE_ARRAY_LEVEL == 3 )
typedef size_t complejo_sizearr_vT;
#endif /* if (complejo_PRECISION_LEVEL == 0 ) */

/**
* \typedef complejo_vT;
* \brief Redefinicion del tipo de dato que utilizaremos como dato 
* real. 
* \version 01v01d01.
* \note nota.
* \warning en caso de cambiar la redefinicion de este mensaje de "warning". 
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE> */
#if (complejo_PRECISION_LEVEL == 0 )
typedef float complejo_vT;
#elif (complejo_PRECISION_LEVEL == 1 )
typedef double complejo_vT;
#elif (complejo_PRECISION_LEVEL == 2 )
typedef long double complejo_vT;
#endif /* if (complejo_PRECISION_LEVEL == 0 ) */


/**
* \enum complejo_eT;
* \brief Redefinicion del tipo de Enumeracion para representar el formato
* en el que puede estar representado el numero complejo.
* Elementos que componen la enumeracion:
*  \li \ref complejo_DEFAULT . 
*  \li \ref complejo_POLAR .
*  \li \ref complejo_POLAR_rad .
*  \li \ref complejo_RECT .
* \version numero de version a la cual corresponde.
* \warning mensaje de "warning".
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE> */
typedef enum
{
  complejo_DEFAULT = 0x00,  /**<@brief Valor por defecto para representar a un numero complejo. */
  complejo_POLAR = 0x01,    /**<@brief Valor que indica un formato polar en degesimal [°]. */
  complejo_POLAR_rad = 0x02,/**<@brief Valor que indica un formato polar en radiales [rad]. */
  complejo_RECT = 0x04,     /**<@brief Valor que indica un formato rectangular.*/
}complejo_eT;


/**
* \struct complejo_sT; 
* \brief Esturctura de datos que representa a un numero complejo.
* Elementos que componen la union:
*  \li \ref real
*  \li \ref img 
*  \li \ref form
*  \li \ref str
* 
* \version 10v01d01.
* \note nota sobre la estructura.
* \warning mensaje de "warning". 
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>  */
typedef struct
{
  complejo_vT real; /**<@brief Numero con coma flotante que represente la parte real o modulo de un numero complejo.*/
  complejo_vT img;  /**<@brief Numero con coma flotante que represente la parte imaginaria o fase de un numero complejo.*/
  complejo_eT form; /**<@brief Variable del Enumeracion que almacena el formato actual en cual esta el numero complejo.*/
  char * str;       /**<@brief puntero para reserva dinamica de memoria, para obtener el string imprimible */
}complejo_sT;



  

/**
* ****************************************************************************//** 
* \fn char * complejo_getstr( complejo_sT * num);
* \brief Funcion para obtener el string que representa a un numero complejo, en 
* su formato actual.s
* \param num : Puntero al numero complejo.
* \return El string de la representacion del complejo. Este es parte del numero
* complejo, ya que reserva memoria si es NULL para este numero complejo.
*      \li 0, success
*      \li 1, failure
* \version numero de version a la cual corresponde.
* \note nota.
* \warning Debe llamarse a la funcion \ref complejo_free() cuando el numero complejo
* deje de usarse. Por mas que el mismo no sea obtenido mediante \ref complejo_new()
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
  complejo_sT a;
  complejo_set_withstr("1+j2");
  //
  printf("a = %s",complejo_getstr(&a));
  //... more code
  
  complejo_free(&a,FALSE); // libreamso solo la memoria del "str"
</PRE>  
*********************************************************************************/
char * complejo_getstr( complejo_sT * num);


// int complejo_set_withstr( complejo_sT * num, const char *str);
complejo_sT * complejo_set_withstr( complejo_sT * num, const char *str);

// void complejo_set_withvalue( complejo_sT * num,complejo_vT v1,complejo_vT v2, complejo_eT form);
complejo_sT * complejo_set_withvalue( complejo_sT * num,complejo_vT v1,complejo_vT v2, complejo_eT form);

int complejo_free(complejo_sT * num,bool_t all);
complejo_sT * complejo_new(const char *str);
// complejo_new_arr(const char **str,complejo_sizearr_vT *plen);
complejo_sT ** complejo_new_arr(const char **str,complejo_sizearr_vT *plen);
int complejo_free_arr(complejo_sT ** arzi, complejo_sizearr_vT len);


// int complejo_change(complejo_sT * num ,complejo_eT formtoconv );
complejo_sT * complejo_change(complejo_sT * num ,complejo_eT formtoconv );



/*
 * complejo_clone(z)    : Retorna una copia de 'z', no modifica a 'z'
 * complejo_inv(z)      : Retorna la inversa de 'z', modifica 'z'. 
 * complejo_conj(z)     : Retorna el conjugado de 'z', modifica 'z'. 
 * complejo_neg(z)      : Retorna la negacion de (invierte R y la I) 'z', modifica 'z'.
 * 
 * complejo_sumatorio(i,__VA_ARGS__)        : Retorna la "Sumatoria(Zi);"
 * complejo_sumatorio_inv(i,__VA_ARGS__)    : Retorna la "Sumatoria(1/Zi) = Sumatoria(Yi);" 
 * complejo_productorio(i,__VA_ARGS__)      : Retorna la "Productoria(Zi);" 
 * complejo_productorio_inv(i,__VA_ARGS__)  : Retorna la "Productoria(1/Zi) = Productoria(Yi);" 
 */
/*
 *  Todas estas APIs retornan una estructura alocada  
 */
complejo_sT * complejo_clone(complejo_sT * z);


/**
 * 
* ****************************************************************************** 
* \fn complejo_sT * complejo_selfoperation(complejo_sT * z, char op,bool_t keepform);
* \brief Funcion para realizar una operacion sobre un numero complejo, almacenando
* el resultado en la misma variable.
* \param z : Puntero al numero complejo.
* \param op : Operacion a realizar sobre el numero complejo y almacenar sobre el.
*   \li '!', inversion 1/z
*   \li '~', conjugado z
*   \li '-', negacion z
* \param keepform : 
* \return Puntero al resultado, este coincide con el del argumento pasado. En caso de
* no poder realizar la operacion retorna NULL.
*      \li != , success 
*      \li NULL, failure
* \version 01v01d01.
* \note nota.
* \warning mensaje de "warning". 
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>

</PRE>  
*********************************************************************************/
complejo_sT * complejo_selfoperation(complejo_sT * z, char op,bool_t keepform,bool_t clone);

/*
#define complejo_inv_KEEP(pZ)   complejo_selfoperation(pZ,'!',TRUE)
#define complejo_inv_NOKEEP(pZ) complejo_selfoperation(pZ,'!',FALSE)
#define complejo_selfop(Op,pZ,...)  #define complejo_##Op(pZ , __VA_ARGS__)  
*/
#define complejo_inv(pZ)    complejo_selfoperation(pZ,'!',TRUE,FALSE)
#define complejo_conj(pZ)   complejo_selfoperation(pZ,'~',TRUE,FALSE)
#define complejo_neg(pZ)    complejo_selfoperation(pZ,'-',TRUE,FALSE)

/*
 * op '!' inversion 1/z
 * op '~' conjugado z
 * op '-' negacion z
 */
// complejo_sT * complejo_clone_selfoperation(complejo_sT * z, char op,bool_t keepform);

#define complejo_cinv(pZ)    complejo_selfoperation(pZ,'!',TRUE,TRUE)
#define complejo_cconj(pZ)   complejo_selfoperation(pZ,'~',TRUE,TRUE)
#define complejo_cneg(pZ)    complejo_selfoperation(pZ,'-',TRUE,TRUE)




/**
* 
* ******************************************************************************** 
* \fn complejo_sT * complejo_add(complejo_sT * zr, complejo_sT * z1,complejo_sT * z2);
*
* \brief Funcion para realizar la suma de dos numeros complejos pasado como puntero. 
* Si pasamos un puntero valido en \ref zr, almacenamos el resultado en este. 
* Si el mismo es NULL, retornamos un putero a una estructura reservada internamente
* la cual debe ser liberada luego de utilizarla.
* 
* \param zr : Puntero al complejo donde almacenaremos el resultado.
* \param z1 : Puntero al complejo Operando uno de la suma.
* \param z2 : Puntero al complejo Operando dos de la suma.
* \return si \ref zr es un puntero NULL, retorna una estructura reservada.
* \version 01v01d01.
* \note nota.
* \warning En caso de que \ref zr sea un puntero NULL, retornamos un puntero 
* al complejo reservado, el caul contiene el resultado. Este debe ser liberado
* mediante la funcion \ref complejo_free(p,TRUE).
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
complejo_sT *a,*b, *r; 
a = complejo_new("25.0 +j 1.52"); 
b = complejo_new("-1.522-j25.0");
//Suma 
r = complejo_add(NULL,a,b);// reservamamos memoria para 'r', luego 
//lo usamos para almacenar el resultado de cada operacion.
printf("\t (%s) + (%s) = %s \n",complejo_getstr(a),\
      complejo_getstr(b),complejo_getstr(r));
//Resta
printf("\t (%s) - (%s) = %s \n",complejo_getstr(a),\
      complejo_getstr(b),complejo_getstr(complejo_sub(r,a,b)));
// Multiplicacion
printf("\t (%s) * (%s) = %s \n",complejo_getstr(a),\
      complejo_getstr(b),complejo_getstr(complejo_mul(r,a,b)));
//Division
printf("\t (%s) / (%s) = %s \n",complejo_getstr(a),\
      complejo_getstr(b),complejo_getstr(complejo_div(r,a,b)));
</PRE>  
*********************************************************************************/
complejo_sT * complejo_operation(complejo_sT * zr, complejo_sT * z1,complejo_sT * z2, char operation);



#define complejo_add(pZr, pZ1, pZ2) \
    complejo_operation(pZr,pZ1,pZ2,'+')

#define complejo_sub(pZr, pZ1, pZ2) \
    complejo_operation(pZr,pZ1,pZ2,'-')
    
#define complejo_mul(pZr, pZ1, pZ2) \
    complejo_operation(pZr,pZ1,pZ2,'*')

#define complejo_div(pZr, pZ1, pZ2) \
    complejo_operation(pZr,pZ1,pZ2,'/')





/**
 * 
* ****************************************************************************** 
* \fn complejo_sT * complejo_add(complejo_sT * zr, complejo_sT * z1,complejo_sT * z2);
*
* \brief descripcion breve sobre la funcion, fn_name().  
* \param Arg1 : Puntero al array.
* \param Arg2 : longitud del array.
* \return status de la opreacion.
*      \li 0, success
*      \li 1, failure
* \version numero de version a la cual corresponde.
* \note nota.
* \warning mensaje de "warning". 
* \date date dayOfMonth de month, years.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
char array[ARRAY_LENG];
//..more code
if( fn_name(array,siezeof(array)) )
puts("operacion correcta");    
else
  puts("operacion no valida");
</PRE>  
*********************************************************************************/
#define complejo_init(z) \
  z.real = 0.0 , z.img = 0.0 , z.str = NULL, z.form = complejo_RECT 



/*-- prototipo para 5 o 6 o hasta 15, superado este nos conviene armar un array, ya
 * que el mecanismo es lento.
 * 
 * complejo_sumatoria(i,__VA_ARGS__)        : Retorna la "Sumatoria(Zi);"
 * complejo_sumatoriainv(i,__VA_ARGS__)     : Retorna la "Sumatoria(1/Zi) = Sumatoria(Yi);" 
 * complejo_productoria(i,__VA_ARGS__)      : Retorna la "Productoria(Zi);" 
 * complejo_productoriainv(i,__VA_ARGS__)   : Retorna la "Productoria(1/Zi) = Productoria(Yi);" */


complejo_sT * complejo_sumatoria(unsigned int numargs, ... );
complejo_sT * complejo_sumatoriainv(unsigned int numargs, ... );
complejo_sT * complejo_productoria(unsigned int numargs, ... );
complejo_sT * complejo_productoriainv(unsigned int numargs, ... );

/**
 * -- Cuando supera el numero y necesitamos calculos rapidos Implementamos Array 
 * complejo_new_arr(const char **str,complejo_sizearr_vT *plen)              : str, debe terminar con una linea de NULL
 * complejo_sumatoria_arr(complejo_sT **arZi, complejo_sizearr_vT len, complejo_sT *zr)       : Retorna la "Sumatoria(arZi);"
 * complejo_sumatoriainv_arr(complejo_sT **arZi, complejo_sizearr_vT len, complejo_sT *zr)    : Retorna la "Sumatoria(1/Zi) = Sumatoria(Yi);" 
 * complejo_productoria_arr(complejo_sT **arZi, complejo_sizearr_vT len, complejo_sT *zr)     : Retorna la "Productoria(Zi);" 
 * complejo_productoriainv_arr(complejo_sT **arZi, complejo_sizearr_vT len, complejo_sT *zr)  : Retorna la "Productoria(1/Zi) = Productoria(Yi);" 
 * 
 *  Si zr es NULL, debemos reservar memoria dinamica para retorar el resultado, y quien lo utilice lo desaloque.
 *  Ya qe podemos necesitar velocidad, en tal caso debemos pasar un puntero a una esturctura que no debe ser
 *  proveniente del heap.
 * 
 */




/*-- debemos pasarla al archivo stdlibc.c / stdlibc.h */
char * stdlib_str_findtoken(const char *pstr,char *token,char **premaind);

/*
 *============================[close, cplusplus]============================
 */
#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==============================[end of file]==============================*/
#endif /* __complejo_h__ */
/*
 *
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * ========================================[end of project file]========================================
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
