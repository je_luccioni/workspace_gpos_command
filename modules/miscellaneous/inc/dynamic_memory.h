#ifndef dynamic_memory_projectEnable
#define dynamic_memory_projectEnable 1 /**<@brief enable/disable the project */
#endif
/** \addtogroup dynamic_memory_h dynamic_memory.h module
 ** @{ */
/********************************[ copyright ]*************************************//**
 *@copyright 2019, Luccioni Jesús Emanuel
 * All rights reserved.
 * This file is part of terminal
 * Redistribution is not allowed on binary and source forms,
 * with or without modification.
 * Use is permitted with prior authorization by
 * the copyright holder
 * \n meil :
 * 	- piero.jel@gmail.com
 ************************************************************************************
 *
 */
/*
 * ******************************[Header File]*************************************//**
 * @file dynamic_memory.h
 * @brief
 * @versiones
 * @date 25 jun. 2019
 * @note
 * @author Luccioni, Jesus Emanuel
 ************************************************************************************
 *
 */
/*
// -- Definimos el Nombre del modulo
*/
#ifndef __dynamic_memory_h__
#define __dynamic_memory_h__
#if(dynamic_memory_projectEnable == 1)
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * =======================================[begin of project file]=======================================
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 */
/*
 *
 * ============================[Open, cplusplus]============================
 *
 */
#ifdef __cplusplus
extern "C" {
#endif
/*
 *
 *
 * *********************************************************************************************
 *
 * =================================[ Begin include header file ]================================
 *
 * ********************************************************************************************
 *
 * TODO: include header file sector, "only in case especific"
 *
 */
#include <stdio.h>


/*
 * *********************************************************************************************
 *
 * ==================================[ End include header file ]=================================
 *
 * ******************************************************************************************** *
 *
 */
/*
 *
 * *********************************************************************************************
 *
 * ==========================[ Begin labels enable/disable functions ]===========================
 *
 * ******************************************************************************************** *
 *
 * TODO: labels enable/disable functions
 *
 */
/* -- local */
#define dynamic_memory_USE_LocalData 0 /**<@brief enable/disable the use Global Data */
#define dynamic_memory_USE_LocalFunctions 0 /**<@brief enable/disable the use Gobal functions */
#define dynamic_memory_USE_LocalMacroApis 0 /**<@brief enable/disable the use Local macros functions */
/* -- global */
#define dynamic_memory_USE_GlobalMacro 0  /**<@brief enable/disable the use Global Macros/Labels */
#define dynamic_memory_USE_GlobalTypedef 0 /**<@brief enable/disable the use Global Typedef */
#define dynamic_memory_USE_GlobalData 0 /**<@brief enable/disable the use Global Data */
#define dynamic_memory_USE_GlobalFunctions 1 /**<@brief enable/disable the use Gobal functions */
#define dynamic_memory_USE_GlobalMacroApis 1 /**<@brief enable/disable the use Gobal macros functions */

#ifndef __dynamic_memory_version__
/**@brief versiones :
 *  + 0     -> Version inicial,
 *  + 1     -> es la version 0 mas el ....
 *  + 2     -> es la version 1 mas el agregado de
 * */
#define __dynamic_memory_version__   0 /**<@brief Definimos la Version que utilizaremos */
#endif /* #ifndef __field_name_version__ */

/*
 * *********************************************************************************************
 *
 * ==========================[ End labels enable/disable functions ]===========================
 *
 * ******************************************************************************************** *
 *
 */
/*
 *
 * *********************************************************************************************
 *
 * ==========================[ Begin Global Macros/labels definition]===========================
 *
 * ******************************************************************************************** **/
#if(dynamic_memory_USE_GlobalMacro == 1)
/*
 * TODO : Macros/labels definition
 */
#if (__dynamic_memory_version__ == 0)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ Begin Global Macros/labels version 0   ]──────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ End   Global Macros/labels version 0   ]──────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__dynamic_memory_version__ == 1)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ Begin Global Macros/labels version 1   ]──────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ End   Global Macros/labels version 1   ]──────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__dynamic_memory_version__ == 0) */
#endif /* #if(dynamic_memory_USE_GlobalMacro == 1) */
/*
 * *********************************************************************************************
 *
 * ==========================[ End Macros/labels Globals ]===========================
 *
 * ******************************************************************************************** *
 *
 */
/*
 *
 * *********************************************************************************************
 *
 * ====================================[Begin Global typedef]====================================
 *
 * ******************************************************************************************** */
#if(dynamic_memory_USE_GlobalTypedef == 1)
/*
 *  TODO: Global typedef
 */
#if (__dynamic_memory_version__ == 0)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ Begin Global typedef version 0   ]──────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/
/**
 * @brief definimos el tipo de estrucura para el handler de la terminal */
typedef struct
{
	char vtchar ;  /**<@brief */
	int vtint ;    /**<@brief */
	double vtdouble ;  /**<@brief */
}struct_example_t ;
/**
 * @brief example definition global enum, <brief> */
typedef enum
{
	 field_name_e1 = 0 ,  /**<@brief */
	 field_name_e2 = 1 ,  /**<@brief */
}enum_example_t;
/**
 * @brief example de redefinicion de una variable/estructura/union/enumeracion */
typedef unsigned int v_example_t;
/**
 * @brief example de redifinicion puntero a funcion
 *  prototipo: unsigned int fx (unsigned int,unsigned int);
 */
typedef unsigned int (*uint_fx_uint_uint_t) (unsigned int,unsigned int);
/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ End   Global typedef version 0   ]──────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__dynamic_memory_version__ == 1)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ Begin Global typedef version 1   ]──────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ End   Global typedef version 1   ]──────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__dynamic_memory_version__ == 0) */
#endif /* #if(dynamic_memory_USE_GlobalTypedef == 1) */
/*
* *********************************************************************************************
*
* ====================================[End Global typedef]====================================
*
* ********************************************************************************************
*
*
*/
#ifdef __dynamic_memory_c__
/* **********************************************************************************************
 *
 * ==================================[ Begin local declaration ]=================================
 *
 **********************************************************************************************
 *
 */
#if (__dynamic_memory_version__ == 0)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ Begin local declaration version 0   ]───────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/


/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ End   local declaration version 0   ]───────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__dynamic_memory_version__ == 1)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ Begin local declaration version 1   ]───────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/


/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────────[ End   local declaration version 1   ]───────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__dynamic_memory_version__ == 0) */
/*
 *
 * *********************************************************************************************
 *
 * ==================================[ End local declaration ]=================================
 *
 **********************************************************************************************
 *
 */
#else /* ifdef __dynamic_memory_c__ */
/*
 *
 * *********************************************************************************************
 *
 * ==============================[ Begin Only externa declaration ]=============================
 *
 **********************************************************************************************
 * SECTOR excluido de -> field_name.c, utlizar con cuidado
 *
 * TODO : Only external declaration
 */
#if (__dynamic_memory_version__ == 0)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin      version 0      ]────────────────────────     │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/


/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End      version 0        ]────────────────────────     │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__dynamic_memory_version__ == 1)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ Begin version 1           ]────────────────────────     │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 1
*/


/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ─────────────────────────[ End      version 1        ]────────────────────────     │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__dynamic_memory_version__ == 0) */

/*
 *
 * *********************************************************************************************
 *
 * ==============================[ End Only externa declaration ]=============================
 *
 **********************************************************************************************
 *
 */
 #endif /* ifdef __dynamic_memory_c__ */
/*
 *
 *
 * *********************************************************************************************
 *
 * ==================================[ Begin Global declaration ]=================================
 *
 **********************************************************************************************
 *
 *
 */
/* ---------------------------------------------------------------------------------
 *
 * ==================[Begin Global data declaration]==============================
 *
 * --------------------------------------------------------------------------------*/
#if(dynamic_memory_USE_GlobalData==1)
/*
// TODO : Global data declaration
*/
#if (__field_name_version__ == 0)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ Begin Global data declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/
extern unsigned int globalExample; /**<@brief variable global example, brief of data */


/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ End   Global data declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__dynamic_memory_version__ == 1)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ Begin Global data declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ End   Global data declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__dynamic_memory_version__ == 0) */
#endif /* #if(dynamic_memory_USE_GlobalTypedef == 1) */
/* ---------------------------------------------------------------------------------
 *
 * ==================[End Global data declaration]==============================
 *
 * ---------------------------------------------------------------------------------
 *
 */
/* ---------------------------------------------------------------------------------
 *
 * ==================[Begin Global functions declaration]=========================
 *
 * --------------------------------------------------------------------------------*/
#if(dynamic_memory_USE_GlobalFunctions == 1)
/*
// TODO : Global functions declaration
*/

#if (__dynamic_memory_version__ == 0)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ Begin Global APIs Declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/
/*
 *
 * *****************************************************************//**
 * @brief descripcion breve
 * @param a : varaible a
 * @param name : puntero al buffer del string
 * @return valor del tipo int
 * @note
 * @par example :
 <pre>
 a = dynamic_memory_test_01(1,"hola");
 </pre>
 *********************************************************************/
int dynamic_memory_test_01(unsigned int a,const char* name);
/*
 *
 * *****************************************************************//**
 * @brief descripcion breve
 * @param a : varaible a
 * @param name : puntero al buffer del string
 * @return valor del tipo int
 * @note
 * @par example :
 <pre>
 a = dynamic_memory_test_02(1,"hola");
 </pre>
 *********************************************************************/
int dynamic_memory_test_02(unsigned int a,const char* name);

/*
*
* ****************************************************************************//**
* \fn void * xmalloc(size_t size, unsigned int nline)
* \brief Funcion para reservar memoria dinamica "heap"
* \param size : tamaño de memoria a reservar, tipo size_t.
* \param nline : Numero de linea de donde es llamada la funcion, con finalidad
* de depuracion del programa.
* \return puntero a void al bloque de memoria reservado. En caso de error imprime
* el mensaje correspondiente en el stream "sterr" y finaliza la ejecucion del
* programa.
*********************************************************************************/
/*void * xmalloc(size_t size, unsigned int nline);*/
void * xmalloc(size_t size, const char * nfile, unsigned int nline);
/*
*
* **************************************************************************//**
* \fn void * xrealloc(void *ptr,size_t size, unsigned int nline)
* \brief Funcion para realocar memoria dinamica "heap"
* \param ptr : puntero al bloque de memoria que se desea redimensionar
* (incrementar tamaño).
* \param size : tamaño de memoria a reservar, tipo size_t.
* \param nline : Numero de linea de donde es llamada la funcion, con finalidad
* de depuracion del programa, este puede ser el valor __LINE__.
* \return puntero a void al bloque de memoria reservado. En caso de error imprime
* el mensaje correspondiente en el stream "sterr" y finaliza la ejecucion del
* programa.
*********************************************************************************/
void * xrealloc(void *ptr,size_t size, unsigned int nline);
/*
*
* **************************************************************************//**
* \fn void * xcalloc(size_t nmemb,size_t size, unsigned int nline);
* \brief Funcion para alocar memoria dinamica "heap" inicializada con ceros.
* \param nmemb : Numero de miembros que contendra el bloque de memoria.
* \param size : tamaño de cada miembo, en byte.
* \param nline : Numero de linea de donde es llamada la funcion, con finalidad
* de depuracion del programa, este puede ser el valor __LINE__.
* \return puntero a void al bloque de memoria reservado. En caso de error imprime
* el mensaje correspondiente en el stream "sterr" y finaliza la ejecucion del
* programa.
*********************************************************************************/
void * xcalloc(size_t nmemb,size_t size, unsigned int nline);
/*
*
* **************************************************************************//**
* \fn char * string_save (const char *pstr);
* \brief Funcion para salvar una cadena de caracteres en un bloque de memora,
* reservado en Heap del proceso.
* \param pstr : Puntero a la cadena de caracteres.
* \return puntero a de la direccion de memoria donde se salvo el string .
*********************************************************************************/
char * string_save (const char *pstr, const unsigned int nline);
/*
*
* ****************************************************************************//**
* \fn void string_PlaceText(char *pS,const char *txt )
* \brief Funcion para colocar una linea de texto en un string o array del
* tipo char.
* \param pS: Puntero al String (array del tipo char).
* \param txt: Puntero al array que contiene el string a colocar, o un string
* del tipo "place text".
* \return nothing
*********************************************************************************/
void string_PlaceText(char *pS,const char *txt );
/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ End   Global APIs Declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__dynamic_memory_version__ == 1)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ Begin Global APIs Declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ──────────────────[ End   Global APIs Declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__dynamic_memory_version__ == 0) */
#endif /* #if( dynamic_memory_USE_GlobalFunctions == 1) */
/* ---------------------------------------------------------------------------------
 *
 * ==================[End Global functions declaration]=========================
 *
 * --------------------------------------------------------------------------------
 *
 */
/* **********************************************************************************************
 *
 * ===========================[ Begin Global Macros functions ]==================================
 *
 ********************************************************************************************** */
 #if (dynamic_memory_USE_GlobalMacroApis == 1)
 #if (__dynamic_memory_version__ == 0)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ Begin Global Macro APIs version 0   ]─────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/
/*
 *
 * ******************************************************************//**
 * @brief Bit Clear File
 * @param file : register file
 * @param posBit : position bit in the register file
 * @return	Nothing
 *********************************************************************/
#define XMALLOC(Size) \
    xmalloc(Size, __FILE__ , __LINE__ )
/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ End   Global Macro APIs version 0   ]─────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__dynamic_memory_version__ == 1)
/**
*
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ Begin Global Macro APIs version 1   ]─────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/**
┌────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                    │
│                                                                                    │
│ ────────────────────[ End   Global Macro APIs version 1   ]─────────────────────── │
│                                                                                    │
│                                                                                    │
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__dynamic_memory_version__ == 0) */
#endif /* #if(dynamic_memory_USE_GlobalMacroApis == 1) */
/* **********************************************************************************************
 *
 * ===========================[ End Global Macros functions ]==================================
 *
 **********************************************************************************************
 *
 */
/*
 *
 * *********************************************************************************************
 *
 * ==================================[ END GLOBAL declaration ]=================================
 *
 ********************************************************************************************** */
/*
 *============================[close, cplusplus]============================
 */
#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==============================[end of file]==============================*/
#endif /* #ifndef __dynamic_memory_h__ */
/*
 *
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * ========================================[end of project file]========================================
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#endif /* #if(dynamic_memory_projectEnable == 1) */
