/** \def typedef_projectEnable \brief enable/disable the project */
#ifndef typedef_projectEnable
#define typedef_projectEnable 1 
#endif
/** 
* \addtogroup typedef
** @{ **/
/********************************[Header File]*************************************//**
* @copyright 
* 2016, Luccioni Jesús Emanuel. \n
* All rights reserved.\n
* This file is part of port module.\n
* Redistribution is not allowed on binary and source forms, with or without 
* modification.\n
* Use is permitted with prior authorization by the copyright holder.\n
* \file typedef.h
* \brief bloque de comenteario para documentacion para describir este archivo de 
* cabecera o header file. 
* \version v01.01
* \date   26 de feb. de 2017
* \note none
* \author JEL, Jesus Emanuel Luccioni
* \li piero.jel@gmail.com  
*************************************************************************************/ 
/*
// -- Definimos el Nombre del modulo
*/
/**
* \def __typedef_h__ 
* \brief definimos el nombre del header file, para evitar redefiniciones */
#ifndef __typedef_h__
#define __typedef_h__
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
* =======================================[begin of project file]=======================================
*
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
* 
*/
/*
*
* ============================[Open, cplusplus]============================
* 
*/
#ifdef __cplusplus
extern "C" { /* } */
#endif
/*
* 
* 
* *********************************************************************************************
*
* =================================[ Begin include header file ]================================
*
* ******************************************************************************************** 
* 
* TODO: include header file sector, "only in case especific"
* 
*/
/*#include <stdio.h>*/


/* 
* *********************************************************************************************
*
* ==================================[ End include header file ]=================================
*
* ******************************************************************************************** *
* 
*/
/* 
* 
* *********************************************************************************************
*
* ==========================[ Begin labels enable/disable functions ]===========================
*
* ******************************************************************************************** *
* 
* TODO: labels enable/disable functions
* 
*/
/* -- local */ 
#define typedef_USE_LocalData 0 /**<@brief enable/disable the use Global Data */
#define typedef_USE_LocalFunctions 0 /**<@brief enable/disable the use Gobal functions */
#define typedef_USE_LocalMacroApis 0 /**<@brief enable/disable the use Local macros functions */
#define typedef_USE_LocalTypedef 0 /**<@brief enable/disable the use Local Typedef */
#define typedef_USE_LocalMacro 0  /**<@brief enable/disable the use Local Macros/Labels */
/* -- global */
#define typedef_USE_GlobalMacro 0  /**<@brief enable/disable the use Global Macros/Labels */
#define typedef_USE_GlobalTypedef 1 /**<@brief enable/disable the use Global Typedef */
#define typedef_USE_GlobalData 0 /**<@brief enable/disable the use Global Data */
#define typedef_USE_GlobalFunctions 1 /**<@brief enable/disable the use Gobal functions */
#define typedef_USE_GlobalMacroApis 1 /**<@brief enable/disable the use Gobal macros functions */


/** \def __typedef_version__
* \brief Definimos la Version que utilizaremos, por defecto \n
*  descripcon de las versiones :
*  + 0     -> Version inicial,
*  + 1     -> es la version 0 mas el ....
*  + 2     -> es la version 1 mas el agregado de
* */
#ifndef __typedef_version__
#define __typedef_version__   0 
#endif /* #ifndef __typedef_version__ */

/* 
* *********************************************************************************************
*
* ==========================[ End labels enable/disable functions ]===========================
*
* ******************************************************************************************** *
*   
*/
/* 
* 
* *********************************************************************************************
*
* ==========================[ Begin Global Macros/labels definition]===========================
*
* ******************************************************************************************** **/
#if(typedef_USE_GlobalMacro == 1)
/*
* TODO : Macros/labels definition
*/
#if (__typedef_version__ == 0)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ Begin Global Macros/labels version 0   ]──────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ End   Global Macros/labels version 0   ]──────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__typedef_version__ == 1)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ Begin Global Macros/labels version 1   ]──────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ End   Global Macros/labels version 1   ]──────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__typedef_version__ == 0) */
#endif /* #if(typedef_USE_GlobalMacro == 1) */
/*
* *********************************************************************************************
*
* ==========================[ End Macros/labels Globals ]===========================
*
* ******************************************************************************************** *
* 
*/
/* 
* 
* *********************************************************************************************
*
* ====================================[Begin Global typedef]====================================
*
* ******************************************************************************************** */
#if(typedef_USE_GlobalTypedef == 1)
/*
*  TODO: Global typedef
*/
#if (__typedef_version__ == 0)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ Begin Global typedef version 0   ]──────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/


/**
 * \def __UINT_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __INT_T__
 * \brief label para evitar redefinicion
 * */

/**
 * \def __INT8_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __INT16_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __INT32_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __INT64_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __UINT8_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __UINT16_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __UINT32_T__
 * \brief label para evitar redefinicion
 * */
/**
 * \def __UINT64_T__
 * \brief label para evitar redefinicion
 * */
/**
* \typedef uint_t;
* \brief redefinicion del tipo de dato primitivo, cuyo tamaño se corresponde
* con el del procesador (para lograr mayor eficiencia ) */

/**
* \typedef uint8_t;
* \brief redefinicion del tipo de dato primitivo, unsigned int 8-Bits */
/**
* \typedef uint16_t;
* \brief redefinicion del tipo de dato primitivo, unsigned int 16-Bits */
/**
* \typedef uint32_t;
* \brief redefinicion del tipo de dato primitivo, unsigned int 32-Bits */
/**
* \typedef uint64_t;
* \brief redefinicion del tipo de dato primitivo, unsigned int 64-Bits */
/**
* \typedef int8_t;
* \brief redefinicion del tipo de dato primitivo, signed int 8-Bits */
/**
* \typedef int16_t;
* \brief redefinicion del tipo de dato primitivo, signed int 16-Bits */
/**
* \typedef int32_t;
* \brief redefinicion del tipo de dato primitivo, signed int 32-Bits */
/**
* \typedef int64_t;
* \brief redefinicion del tipo de dato primitivo, signed int 64-Bits */




#if defined(__M16__)
/*-- redefinicion de datos primitvos arquitectura de 16-Bits
 * ┌─────────────────┬──────┬───────┬─────┬──────┬───────────┬───────┬────────┬─────────────┐
 * │  Data model     │ char │ short │ int │ long │ long long │ float │ double │ long double │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  IP16L32 (near) │   8  │  16   │  32 │  32  │ --------- │ ----- │ ------ │ ----------- │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  I16LP32 (far)  │   8  │  16   │  32 │  32  │ --------- │ ----- │ ------ │ ----------- │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  -m32           │   8  │  16   │  32 │  32  │    64     │   32  │   64   │     96      │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  -m64           │   8  │  16   │  32 │  64  │    64     │   32  │   64   │    128      │
 * └─────────────────┴──────┴───────┴─────┴──────┴───────────┴───────┴────────┴─────────────┘
 * */
#ifndef __UINT8_T__
#define __UINT8_T__
typedef unsigned char uint8_t ;
#endif
#ifndef __INT8_T__
#define __INT8_T__
typedef signed char int8_t ;
#endif
#ifndef __UINT16_T__
#define __UINT16_T__
typedef unsigned short uint16_t ;
#endif
#ifndef __INT16_T__
#define __INT16_T__
typedef signed short int16_t ;
#endif
#ifndef __UINT32_T__
#define __UINT32_T__
typedef unsigned int uint32_t ;
#endif
#ifndef __INT32_T__
#define __INT32_T__
typedef signed int int32_t ;
#endif
/**/
#elif defined(__M32__) /* #if defined(__M16__) */
/*-- redefinicion de datos primitvos arquitectura de 16-Bits
 * ┌─────────────────┬──────┬───────┬─────┬──────┬───────────┬───────┬────────┬─────────────┐
 * │  Data model     │ char │ short │ int │ long │ long long │ float │ double │ long double │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  -m32 [Bits]    │   8  │  16   │  32 │  32  │    64     │   32  │   64   │     96      │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  -m32 [Bytes]   │  01  │  02   │  04 │  04  │    08     │   04  │   08   │     12      │
 * └─────────────────┴──────┴───────┴─────┴──────┴───────────┴───────┴────────┴─────────────┘
 * */
#ifndef __UINT8_T__
#define __UINT8_T__
typedef unsigned char uint8_t ;
#endif
#ifndef __INT8_T__
#define __INT8_T__
typedef signed char int8_t ;
#endif
#ifndef __UINT16_T__
#define __UINT16_T__
typedef unsigned short uint16_t;
#endif
#ifndef __INT16_T__
#define __INT16_T__
typedef signed short int16_t ;
#endif
#ifndef __UINT32_T__
#define __UINT32_T__
typedef unsigned int uint32_t ;
#endif
#ifndef __INT32_T__
#define __INT32_T__
typedef signed int int32_t ;
#endif
#ifndef __UINT64_T__
#define __UINT64_T__
typedef unsigned long long uint64_t ;
#endif
#ifndef __INT64_T__
#define __INT64_T__
typedef signed long long int64_t ;
#endif
/**/
#ifndef __NLINE_T__
#define __NLINE_T__
typedef unsigned int nline_t ;
#endif


#elif defined(__M64__) /* #if defined(__M16__) */
/*-- redefinicion de datos primitvos arquitectura de 16-Bits
 * ┌─────────────────┬──────┬───────┬─────┬──────┬───────────┬───────┬────────┬─────────────┐
 * │  Data model     │ char │ short │ int │ long │ long long │ float │ double │ long double │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  -m64 [Bits]    │   8  │  16   │  32 │  64  │    64     │   32  │   64   │    128      │
 * ├─────────────────┼──────┼───────┼─────┼──────┼───────────┼───────┼────────┼─────────────┤
 * │  -m64 [Bytes]   │  01  │  02   │  04 │  08  │    08     │   04  │   08   │    016      │
 * └─────────────────┴──────┴───────┴─────┴──────┴───────────┴───────┴────────┴─────────────┘
 * */
#ifndef __UINT_T__
#define __UINT_T__	32
typedef unsigned int uint_t ;
#endif
#ifndef __INT_T__
#define __INT_T__	32
typedef signed int int_t ;
#endif

#ifndef __UINT8_T__
#define __UINT8_T__
typedef unsigned char uint8_t ;
#endif
#ifndef __INT8_T__
#define __INT8_T__
typedef signed char int8_t ;
#endif
#ifndef __UINT16_T__
#define __UINT16_T__
typedef unsigned short uint16_t ;
#endif
#ifndef __INT16_T__
#define __INT16_T__
typedef signed short int16_t ;
#endif
#ifndef __UINT32_T__
#define __UINT32_T__
typedef unsigned int uint32_t ;
#endif
#ifndef __INT32_T__
#define __INT32_T__
typedef signed int int32_t ;
#endif
#define __UINT64_T__
#ifndef __UINT64_T__
#define __UINT64_T__
typedef unsigned long long uint64_t ;
#endif
#ifndef __INT64_T__
#define __INT64_T__
/*typedef signed long long int64_t ;*/
#endif
/**/
#ifndef __NLINE_T__
#define __NLINE_T__
typedef unsigned int nline_t ;
#endif
/* */
#else
#warning "Arquitectura del procesador no definida"
#endif


/**
* \enum bool_t
* \brief enumeracion para establecer estados logicos
* \li \ref FALSE
* \li \ref TURE
*/
typedef enum
{
    FALSE = 0,  /**<@brief enumeracion para establecer el valor logico FALSE '0' */
    TRUE = !FALSE,  /**<@brief enumeracion para establecer el valor logico TRUE '1' */
}bool_t;

#if 0

/**
* \struct struct_example_t;
* \brief ejemplo de estructura de datos
* \li char vtchar ; \ref vtchar
* \li int vtint ;  \ref vtint 
* \li double vtdouble ; \ref vtdouble 
*/
typedef struct
{
    char vtchar ;  /**<@brief variable del tipo signed char */
    int vtint ;    /**<@brief variable del tipo signed int */
    double vtdouble ;  /**<@brief variable del tipo double */
}struct_example_t ; 
/**  
* \enum enum_example_t 
* \brief example definition global enum
* \li \ref typedef_e1 
* \li \ref typedef_e2 
*/
typedef enum
{
    typedef_e1 = 0,  /**<@brief enumeracion typedef_e1 */
    typedef_e2 = 1,  /**<@brief enumeracion typedef_e2  */
}enum_example_t;
/**
* \typedef unsigned int v_example_t; 
* \brief example de redefinicion de una variable/estructura/union/enumeracion */
typedef unsigned int v_example_t;
/**
* \typedef unsigned int (*uint_fx_uint_uint_t) (unsigned int,unsigned int);
* \brief example de redifinicion puntero a funcion */
typedef unsigned int (*uint_fx_uint_uint_t) (unsigned int,unsigned int);
#endif

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ End   Global typedef version 0   ]──────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__typedef_version__ == 1)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ Begin Global typedef version 1   ]──────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/**
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ End   Global typedef version 1   ]──────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__typedef_version__ == 0) */
#endif /* #if(typedef_USE_GlobalTypedef == 1) */
/*
* *********************************************************************************************
*
* ====================================[End Global typedef]====================================
*
* ******************************************************************************************** 
* 
* 
*/
#ifdef __typedef_c__
/* **********************************************************************************************
*
* ==================================[ Begin local declaration ]=================================
*
********************************************************************************************** 
* 
*/
#if (__typedef_version__ == 0)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ Begin local declaration version 0   ]───────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ End   local declaration version 0   ]───────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__typedef_version__ == 1)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ Begin local declaration version 1   ]───────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────────[ End   local declaration version 1   ]───────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__typedef_version__ == 0) */
/* 
* 
* *********************************************************************************************
*
* ==================================[ End local declaration ]=================================
*
********************************************************************************************** 
* 
*/
#else /* ifdef __typedef_c__ */
/*
* 
* *********************************************************************************************
*
* ==============================[ Begin Only externa declaration ]=============================
*
********************************************************************************************** 
* SECTOR excluido de -> typedef.c, utlizar con cuidado
* 
* TODO : Only external declaration
*/
#if (__typedef_version__ == 0)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin      version 0      ]────────────────────────     │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End      version 0        ]────────────────────────     │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__typedef_version__ == 1)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ Begin version 1           ]────────────────────────     │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 1
*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ─────────────────────────[ End      version 1        ]────────────────────────     │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__typedef_version__ == 0) */

/*
* 
* *********************************************************************************************
*
* ==============================[ End Only externa declaration ]=============================
*
**********************************************************************************************
* 
*/
#endif /* ifdef __typedef_c__ */
/*
* 
* 
* *********************************************************************************************
*
* ==================================[ Begin Global declaration ]=================================
*
********************************************************************************************** 
* 
* 
*/
/* ---------------------------------------------------------------------------------
*
* ==================[Begin Global data declaration]==============================
*
* --------------------------------------------------------------------------------*/
#if(typedef_USE_GlobalData==1)
/*
// TODO : Global data declaration
*/
#if (__typedef_version__ == 0)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ Begin Global data declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/
/**
* \var
* \brief variable global example, brief of data */
extern unsigned int globalExample; 


/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ End   Global data declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__typedef_version__ == 1)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ Begin Global data declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ End   Global data declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__typedef_version__ == 0) */
#endif /* #if(typedef_USE_GlobalTypedef == 1) */
/* ---------------------------------------------------------------------------------
*
* ==================[End Global data declaration]==============================
*
* ---------------------------------------------------------------------------------
* 
*/
/* ---------------------------------------------------------------------------------
*
* ==================[Begin Global functions declaration]=========================
*
* --------------------------------------------------------------------------------*/
#if(typedef_USE_GlobalFunctions == 1)
/*
// TODO : Global functions declaration
*/

#if (__typedef_version__ == 0)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ Begin Global APIs Declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/
/*
*
* ****************************************************************************//**
* \fn unsigned int get_random(unsigned int mask);
* \brief funcion para obtener un numero aleatorio
* \param bit_mask: numeros de bit para enmascarar, nos permite establecer el
* valor maximo del numero aleatorio a obtener.
*   \li should be un valor del tipo entero positivo, comprendido entre el
*   rango {0 - 32}.
* \return valor entero que representa el numero aleatorio a obtener.
* \author jel
* \note:
*********************************************************************************/
uint32_t get_random(uint8_t bit_mask);
uint8_t get_randomu8(uint8_t bit_mask);

/*
*
* ****************************************************************************//**
* \fn uint32_t factorial(uint32_t n);
* \brief funcion recursiva para el calculo del factorial
 * \f[
 * n! = \prod_{k=1}^n (k)
 * \f]
* \param value: valor sobre el cual se requiere obtener el factorial.
*   \li should be un valor del tipo entero positivo.
* \return valor entero que representa el factorial
* \author jel
* \note
*********************************************************************************/
uint32_t factorial(uint32_t n);

/*
* ****************************************************************************//** 
* \fn int typedef_test_02(unsigned int a,const char* name);
* \brief descripcion breve
* \param a : varaible a
* \param name : puntero al buffer del string
* \return valor del tipo int
* \note
* \par example :
<pre>
a = typedef_test_01(1,"hola");
</pre>
*********************************************************************************/
/*//int typedef_test_02(unsigned int a,const char* name);*/


/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ End   Global APIs Declaration version 0   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__typedef_version__ == 1)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ Begin Global APIs Declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ──────────────────[ End   Global APIs Declaration version 1   ]─────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__typedef_version__ == 0) */
#endif /* #if( typedef_USE_GlobalFunctions == 1) */
/* ---------------------------------------------------------------------------------
*
* ==================[End Global functions declaration]=========================
*
* --------------------------------------------------------------------------------
* 
*/
/* **********************************************************************************************
*
* ===========================[ Begin Global Macros functions ]==================================
*
********************************************************************************************** */
#if (typedef_USE_GlobalMacroApis == 1) 
#if (__typedef_version__ == 0)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ Begin Global Macro APIs version 0   ]─────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/
/*
* ****************************************************************************//**
* \def ARRAYSIZE(x)
* \brief Bit Clear File
* \param x : array name
* \return	size in byte of array
*********************************************************************************/
#define ARRAY_SIZE(x) (sizeof (x)/sizeof (x[0]))
/*
* ****************************************************************************//**
* \def ARRAY_PRINT(x,i,format)
* \brief macro api para imprimir un array
* \param x : array name
* \param i : subscript name (nombre del indice del tipo size_t)
* \param format : print format { %x,%d,%u,%f,%ld,%lf...}
* \return	nothing, macro expanding
*********************************************************************************/
#define ARRAY_PRINT(x,i,format)\
{\
	for (i = 0; i < ARRAY_SIZE(x); i++ )\
	{\
		/*printf("\t array[%ld] = %f\n",i,array[i]); */\
		printf("\t"#x"[%lu] = "#format"\n",i,x[i]);\
	}\
}

/*
* ****************************************************************************//**
* \def ARRAY_INTEGER_PRINT(x,index,len,format,token_begin,token_end)
* \brief macro api para imprimir un array entero, ya sea unsigned, signed long etc
* \param x : array name
* \param index : varibale del tipo size_t que usaremos como indice
* \param len : Longitud del array.
* \param format : formato del item que compone el array "print format
* { %x,%d,%u,%f,%ld,%lf...}"
* \param token_begin : conjunto de token que se deben ejecutar antes de
* imprimir un item.
* \param token_end : conjunto de token que se deben ejecutar despues de
* imprimir un item.
* \return	nothing, macro expanding
*********************************************************************************/
#define ARRAY_INTEGER_PRINT(x,index,len,format,token_begin,token_end)\
{\
	for (index = 0; index < len ; index++ )\
	{\
		token_begin\
		printf("\t"#x"[%lu] = "#format"\n",index,x[index]);\
		token_end\
	}\
}

/*
* ****************************************************************************//**
* \def ARRAY_PRINT(x,i,format)
* \brief macro api para imprimir un array
* \param x : array name
* \param i : subscript name (nombre del indice del tipo size_t)
* \param format : print format { %x,%d,%u,%f,%ld,%lf...}
* \return	nothing, macro expanding
*********************************************************************************/
#define ARRAY_FILL(x,i,mask)\
{\
	get_random(mask);\
	for (i = 0; i < ARRAY_SIZE(x); i++ )\
	{\
		/*printf("\t array[%ld] = %f\n",i,array[i]); */\
		x[i] = get_random(0)/1.00011;\
	}\
}

/*
* 
* ****************************************************************************//**
* \def typedef_BITcLEARfILE(file,posBit)
* \brief Bit Clear File
* \param file : register file
* \param posBit : position bit in the register file
* \return	Nothing
*********************************************************************************/
#define typedef_BITcLEARfILE(file,posBit)
/*
*
* ****************************************************************************//**
* \def CASTING(var,type)
* \brief macro para realizar el casting de una variable
* \param var : Nombre de la variable
* \param type : typo de dato ,primitivo o no, sobre el cual se desea realizar
* el casting.
* \return	Nothing
*********************************************************************************/
#define CASTING(var,type)	((type) (var))



#define FONT_COLOR_BLACK           "\e[0;30m"
#define FONT_COLOR_RED               "\e[0;31m"
#define FONT_COLOR_GREEN         "\e[0;32m"
#define FONT_COLOR_YELLOW        "\e[0;33m"
#define FONT_COLOR_BLUE          "\e[0;34m"
#define FONT_COLOR_MAGENTA    "\033[0;35m"
#define FONT_COLOR_CYAN          "\e[0;36m"
#define FONT_COLOR_WHITE         "\e[0;37m"
#define FONT_COLOR_RESET           "\e[0m"

#define SET_COLOR_FONT(Color)        FONT_COLOR_##Color


/**/
#define BACKGROUND_COLOR_BLACK           "\e[0;48;5;21m"
#define BACKGROUND_COLOR_RED               "\e[0;48;5;31m"
#define BACKGROUND_COLOR_GREEN           "\e[0;48;5;41m"
#define BACKGROUND_COLOR_YELLOW         "\e[0;48;5;51m"
#define BACKGROUND_COLOR_BLUE              "\e[0;48;5;61m"
#define BACKGROUND_COLOR_MAGENTA      "\e[0;48;5;71m"
#define BACKGROUND_COLOR_CYAN             "\e[0;48;5;81m"
#define BACKGROUND_COLOR_WHITE            "\e[0;48;5;91m"
#define BACKGROUND_COLOR_RESET             "\e[0;48;2;0;0;0m"


#define SET_COLOR_BACK(Color)        BACKGROUND_COLOR_##Color

#define PRINTF_COLOR_N2                 "\033[0;40m"
#define PRINTF_COLOR_N3                 "\033[0;41m"

#define PRINTF_COLOR_RESET           "\033[0m"



/**
 * ******************************************************************//**
 * \def SET_COLOR(Type,Color);
 * \brief macro para establecer el color de Font y background de la
 * salida por consola.
 * \details descripcion detallada.
 * \param Type : Tipo de color  a establecer, este puede ser
 * 		\li BACK : para establecer el color de background.
 * 		\li FONT : para establecer el color de font.
 * \param Color : Argumento dos.
 * \return  Nothing
 * \note nota sobre la macro funcion.
 * \warning mensaje de precaucion.
 * \par example :
 <PRE>
	printf(SET_COLOR(FONT,RED)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    printf(SET_COLOR(FONT,GREEN)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    printf(SET_COLOR(FONT,YELLOW)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    printf(SET_COLOR(FONT,BLUE)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    printf(SET_COLOR(FONT,MAGENTA)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    printf(SET_COLOR(FONT,CYAN)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    printf(SET_COLOR(FONT,WHITE)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    printf(SET_COLOR(FONT,BLACK)"\t El Numero aleatorio de %u\n"SET_COLOR(FONT,RESET),get_random(5));
    //
    printf(SET_COLOR(BACK,RED)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));
    printf(SET_COLOR(BACK,GREEN)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));
    printf(SET_COLOR(BACK,YELLOW)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));
    printf(SET_COLOR(BACK,BLUE)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));
    printf(SET_COLOR(BACK,MAGENTA)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));
    printf(SET_COLOR(BACK,CYAN)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));
    printf(SET_COLOR(BACK,WHITE)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));
    printf(SET_COLOR(BACK,BLACK)"\t El Numero aleatorio de %u\n"SET_COLOR(BACK,RESET),get_random(5));

 </PRE>
 *
 *********************************************************************/
#define SET_COLOR(Type,Color) \
    SET_COLOR_##Type(Color)

/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ End   Global Macro APIs version 0   ]─────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#elif (__typedef_version__ == 1)
/*
* 
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ Begin Global Macro APIs version 1   ]─────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
TODO: version 0
*/



/*
┌────────────────────────────────────────────────────────────────────────────────────┐           
│                                                                                    │  
│                                                                                    │  
│ ────────────────────[ End   Global Macro APIs version 1   ]─────────────────────── │
│                                                                                    │
│                                                                                    │             
└────────────────────────────────────────────────────────────────────────────────────┘
*
*/
#endif /* #if (__typedef_version__ == 0) */
#endif /* #if(typedef_USE_GlobalMacroApis == 1) */
/* **********************************************************************************************
*
* ===========================[ End Global Macros functions ]==================================
*
**********************************************************************************************
* 
*/
/*
* 
* *********************************************************************************************
*
* ==================================[ END GLOBAL declaration ]=================================
*
********************************************************************************************** */
/*
*============================[close, cplusplus]============================
*/
#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==============================[end of file]==============================*/
#endif /* #ifndef __typedef_h__ */
/*
*
* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
* ========================================[end of project file]========================================
*
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
