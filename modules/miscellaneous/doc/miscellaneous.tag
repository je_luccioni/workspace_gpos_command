<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>dynamic_memory.h</name>
    <path>/mnt/461c4378-272b-48d3-8c73-c4e27a8d2ae2/home/jel/Escritorio/0P-Programacion_GNU/0M-Manuales/0M-Manual_TheGNU_C/workspace_gpos_command/modules/miscellaneous/inc/</path>
    <filename>d2/d23/dynamic__memory_8h.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>__dynamic_memory_h__</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>abd592e7f6cade7a555ce7aa029a173c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_LocalData</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a31ce39f27eff487ba25042fc134e84f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_LocalFunctions</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a1ee2ada9d5c6e6b8de8688b21576be19</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_LocalMacroApis</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a321bbf43b1d99c30df78d5c229c2def8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_GlobalMacro</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a9d4b86aab4f59bcf9692603ac2577121</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_GlobalTypedef</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>ad370523d4cf7589421d379bf0fd9b61a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_GlobalData</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>ab86548e96a16d92a8b30c898cc412c87</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_GlobalFunctions</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a06b364348aeeb9f1eb000cfd20e593af</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>dynamic_memory_USE_GlobalMacroApis</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a2ba012f6bbda9b696850b3120b16b90a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__dynamic_memory_version__</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>ac893183c5b8c7db3fa5c46b01cbe1496</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>xmalloc</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a24e067391194bd73ba71817d45ec59cd</anchor>
      <arglist>(size_t size, nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>xrealloc</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a7f2a1ab95fdff6fcca2c6cd82b0b8d41</anchor>
      <arglist>(void *ptr, size_t size, nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>xcalloc</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a20a6f8600cfd7db6211da03c21894cff</anchor>
      <arglist>(size_t nmemb, size_t size, nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>string_save</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a5b833ecd6184ba0a0ae3a6b60d9c87cb</anchor>
      <arglist>(const char *pstr, const nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>string_PlaceText</name>
      <anchorfile>d2/d23/dynamic__memory_8h.html</anchorfile>
      <anchor>a00bde209912b202c4b3b80758f942e44</anchor>
      <arglist>(char *pS, const char *txt)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>msg_error.h</name>
    <path>/mnt/461c4378-272b-48d3-8c73-c4e27a8d2ae2/home/jel/Escritorio/0P-Programacion_GNU/0M-Manuales/0M-Manual_TheGNU_C/workspace_gpos_command/modules/miscellaneous/inc/</path>
    <filename>d1/dfd/msg__error_8h.html</filename>
    <includes id="d3/d0b/typedef_8h" name="typedef.h" local="no" imported="no">typedef.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__msg_error_h__</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a4334fdbbf820d3638cce441bd6ada807</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_LocalData</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a3bfadef1ef95ff3ceff11ddd54e8fccd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_LocalFunctions</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a61ff4dfc13c844f7712b0ba72d250d10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_LocalMacroApis</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a4b522e76378208f9ec3a89e56351b028</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_GlobalMacro</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a898ef46a3674beffd97e5e22995f707d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_GlobalTypedef</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a5c53b2d537b375071a471e59e2fd2540</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_GlobalData</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a7e6a4ec87aeb7373604349e9580ff570</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_GlobalFunctions</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a5309e06f7fcc35497b86b3bb63ad31fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>msg_error_USE_GlobalMacroApis</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a9fb054bc9a86d808c74f22a87e1298d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>msg_error_print</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>a391399a805d1526bab71480842baaf4c</anchor>
      <arglist>(char *msj, int verrno, const char *nfile, const nline_t nline, bool_t st_to_exit)</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>errno</name>
      <anchorfile>d1/dfd/msg__error_8h.html</anchorfile>
      <anchor>ad65a8842cc674e3ddf69355898c0ecbf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>typedef.h</name>
    <path>/mnt/461c4378-272b-48d3-8c73-c4e27a8d2ae2/home/jel/Escritorio/0P-Programacion_GNU/0M-Manuales/0M-Manual_TheGNU_C/workspace_gpos_command/modules/miscellaneous/inc/</path>
    <filename>d3/d0b/typedef_8h.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>__typedef_h__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a4a2ce6c00d4f19abe77c967a780af522</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_LocalData</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>aa825f7708cb69d374a534826e1d93852</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_LocalFunctions</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>ac4f36f490b5b1989f3d4eee1a6db9796</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_LocalMacroApis</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>aedfe08fc6aabcfd9bf20bc46d111b83d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_LocalTypedef</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a6292412adeb77934fbd612df841afec8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_LocalMacro</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a33550b73c109cacf03bb732c2992891c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_GlobalMacro</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>abda88f1e29a964d365e660966a015fa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_GlobalTypedef</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>aa6774276fd24a783ca87454a864656d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_GlobalData</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a93812cdd3fa47a40b31848938a7e42d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_GlobalFunctions</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a2ddb85f68935adc535c790e08cd29260</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_USE_GlobalMacroApis</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a1bd708cd8fd57fae0a11190f2fce897d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__UINT_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a87d8e32ce7a6d5d2cfda2cb52c557693</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__INT_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>abf71bab06c7f2a4e8b0a9ddbc4ba4b6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__UINT8_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>afc32450a9e1e775eb0e0839d6ceb9874</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__INT8_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a0e9c17d7baa608b1ee00310e3a28802e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__UINT16_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>af577ac2303d72d9b7b52e1d89e8988eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__INT16_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>aa1ebf4b439978d58af458a6bd4232e76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__UINT32_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>adc8e1151d76f3117a160760d93745445</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__INT32_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a3623b229079ea864e37c57d3ddc7afe4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>__NLINE_T__</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a7e527bbd56a5b50a6ff368c47d472c1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ARRAY_SIZE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a6242a25f9d996f0cc4f4cdb911218b75</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ARRAY_PRINT</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a0fa91496fb145a243da49a73dcad9ab3</anchor>
      <arglist>(x, i, format)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ARRAY_INTEGER_PRINT</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a76c0cab2f387c67f6fd5fb949182e6dc</anchor>
      <arglist>(x, index, len, format, token_begin, token_end)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ARRAY_FILL</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a15b9f7c88cc275c4dd6c0d9d363a3fce</anchor>
      <arglist>(x, i, mask)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>typedef_BITcLEARfILE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>ac196db125c60a4760e571a986aabf07e</anchor>
      <arglist>(file, posBit)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CASTING</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>afc395cffbde9d4bb67ba7bd62d51de4a</anchor>
      <arglist>(var, type)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_BLACK</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a4cc5bed2c77cd91cb91957f3b96cf3b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_RED</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>ac4e53908333e386f1aee880f8c04e435</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_GREEN</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a80b013d9ddc1e8a9d8e65e16b49bb72f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_YELLOW</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a4ec9b22dd658d39d7c5bde6512633543</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_BLUE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a87f73865dc66c3bf22e87b1279cc1d91</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_MAGENTA</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a0b39e9c092b9a434490cfe2f480069e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_CYAN</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>ad9269acfd920d34de918501fec558d7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_WHITE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a9aadb9034a8af01ff10692c363acf2a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FONT_COLOR_RESET</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a11e36aa069152c51443c31c81ba7511a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SET_COLOR_FONT</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>aea72d765332cd3c2ea85c26a98ec42ea</anchor>
      <arglist>(Color)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_BLACK</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a1f4662d7f58fb3a9428e53828a20018b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_RED</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a76ae5120e95972432e1fe58c775b6c37</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_GREEN</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>aa12f7a19622e6aa25e5ff0521baa0f07</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_YELLOW</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a967f5bacb6964984fa903519371f46e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_BLUE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a51b875f5accc3e456d69b5dd898007cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_MAGENTA</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a85747f9ff89b8e4b06e83f165eedefc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_CYAN</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a26cdb01a44c30b5d9826645b3e326b5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_WHITE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a00ac1d31860151def6543022ca9ef652</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BACKGROUND_COLOR_RESET</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a79842326912e65b4dd403a5c3e836101</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SET_COLOR_BACK</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a04c62d9aac5cc735df39e52539509b1e</anchor>
      <arglist>(Color)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SET_COLOR</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a65589359a2b6338289586da33e3f448c</anchor>
      <arglist>(Type, Color)</arglist>
    </member>
    <member kind="typedef">
      <type>unsigned int</type>
      <name>uint_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a83d6866a5b3c76f2ad436a370ecd5b5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed int</type>
      <name>int_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a13c47ad785892c3674231b72fe20dbd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned char</type>
      <name>uint8_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>ae1affc9ca37cfb624959c866a73f83c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed char</type>
      <name>int8_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a20c38cc5aac7cc6b0a3c6ab05428436d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned short</type>
      <name>uint16_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a5a8b2dc9e45a9ee81a94ef304fb62505</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed short</type>
      <name>int16_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a2140805d08462d474b82ddc8d1c2f3e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned int</type>
      <name>uint32_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a09a1e304d66d35dd47daffee9731edaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed int</type>
      <name>int32_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>afd12020da5a235dfcf0c3c748fb5baed</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned int</type>
      <name>nline_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a1e22813bae37a72b1d46f96e81900962</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>bool_t</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a04dd5074964518403bf944f2b240a5f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FALSE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a04dd5074964518403bf944f2b240a5f8aa1e095cc966dbecf6a0d8aad75348d1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TRUE</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a04dd5074964518403bf944f2b240a5f8aa82764c3079aea4e60c80e45befbb839</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>get_random</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>ae375056bf249f07dcded39034f384af6</anchor>
      <arglist>(uint8_t bit_mask)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>get_randomu8</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>abde9d13cd9401f7b343a6d3d01c94edd</anchor>
      <arglist>(uint8_t bit_mask)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>factorial</name>
      <anchorfile>d3/d0b/typedef_8h.html</anchorfile>
      <anchor>a8d7af12626fdbf881f08cbb0fff93a68</anchor>
      <arglist>(uint32_t n)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>dynamic_memory.c</name>
    <path>/mnt/461c4378-272b-48d3-8c73-c4e27a8d2ae2/home/jel/Escritorio/0P-Programacion_GNU/0M-Manuales/0M-Manual_TheGNU_C/workspace_gpos_command/modules/miscellaneous/src/</path>
    <filename>de/de5/dynamic__memory_8c.html</filename>
    <includes id="d1/dfd/msg__error_8h" name="msg_error.h" local="no" imported="no">msg_error.h</includes>
    <includes id="d2/d23/dynamic__memory_8h" name="dynamic_memory.h" local="no" imported="no">dynamic_memory.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__dynamic_memory_c__</name>
      <anchorfile>de/de5/dynamic__memory_8c.html</anchorfile>
      <anchor>abc0ba5821983e9caaf9830d93cf6ef62</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>string_save</name>
      <anchorfile>de/de5/dynamic__memory_8c.html</anchorfile>
      <anchor>a29fca13d0ed58e37746c110df8135701</anchor>
      <arglist>(const char *pstr, const nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>xmalloc</name>
      <anchorfile>de/de5/dynamic__memory_8c.html</anchorfile>
      <anchor>adc75fb78a0b25a13500c200628286d7e</anchor>
      <arglist>(size_t size, nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>xrealloc</name>
      <anchorfile>de/de5/dynamic__memory_8c.html</anchorfile>
      <anchor>ad0b74dfbf7c4bd6d521f7e80d2a10562</anchor>
      <arglist>(void *ptr, size_t size, nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>xcalloc</name>
      <anchorfile>de/de5/dynamic__memory_8c.html</anchorfile>
      <anchor>ac1ec0faba8a58ec358900c0457f845e8</anchor>
      <arglist>(size_t nmemb, size_t size, nline_t nline)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>string_PlaceText</name>
      <anchorfile>de/de5/dynamic__memory_8c.html</anchorfile>
      <anchor>a00bde209912b202c4b3b80758f942e44</anchor>
      <arglist>(char *pS, const char *txt)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>msg_error.c</name>
    <path>/mnt/461c4378-272b-48d3-8c73-c4e27a8d2ae2/home/jel/Escritorio/0P-Programacion_GNU/0M-Manuales/0M-Manual_TheGNU_C/workspace_gpos_command/modules/miscellaneous/src/</path>
    <filename>da/dc4/msg__error_8c.html</filename>
    <includes id="d1/dfd/msg__error_8h" name="msg_error.h" local="no" imported="no">msg_error.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__msg_error_c__</name>
      <anchorfile>da/dc4/msg__error_8c.html</anchorfile>
      <anchor>a72664fc336d29f162755c0cd36f1caa9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>msg_error_print</name>
      <anchorfile>da/dc4/msg__error_8c.html</anchorfile>
      <anchor>af3d58f12fa186d09c57b642ac5d0e777</anchor>
      <arglist>(char *msj, int verrno, const char *nfile, const nline_t nline, bool_t st_to_exit)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>typedef.c</name>
    <path>/mnt/461c4378-272b-48d3-8c73-c4e27a8d2ae2/home/jel/Escritorio/0P-Programacion_GNU/0M-Manuales/0M-Manual_TheGNU_C/workspace_gpos_command/modules/miscellaneous/src/</path>
    <filename>dd/dc4/typedef_8c.html</filename>
    <includes id="d3/d0b/typedef_8h" name="typedef.h" local="no" imported="no">typedef.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__typedef_c__</name>
      <anchorfile>dd/dc4/typedef_8c.html</anchorfile>
      <anchor>a2ce49121f6669d64e7b5e48750d42aae</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>miscellaneous</name>
    <title>&quot;miscellaneous module&quot;</title>
    <filename>dc/dde/group__miscellaneous.html</filename>
    <file>dynamic_memory.c</file>
    <file>msg_error.c</file>
    <file>typedef.c</file>
    <file>dynamic_memory.h</file>
    <file>msg_error.h</file>
    <file>typedef.h</file>
  </compound>
</tagfile>
