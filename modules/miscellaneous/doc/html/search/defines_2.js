var searchData=
[
  ['background_5fcolor_5fblack_146',['BACKGROUND_COLOR_BLACK',['../d3/d0b/typedef_8h.html#a1f4662d7f58fb3a9428e53828a20018b',1,'typedef.h']]],
  ['background_5fcolor_5fblue_147',['BACKGROUND_COLOR_BLUE',['../d3/d0b/typedef_8h.html#a51b875f5accc3e456d69b5dd898007cf',1,'typedef.h']]],
  ['background_5fcolor_5fcyan_148',['BACKGROUND_COLOR_CYAN',['../d3/d0b/typedef_8h.html#a26cdb01a44c30b5d9826645b3e326b5a',1,'typedef.h']]],
  ['background_5fcolor_5fgreen_149',['BACKGROUND_COLOR_GREEN',['../d3/d0b/typedef_8h.html#aa12f7a19622e6aa25e5ff0521baa0f07',1,'typedef.h']]],
  ['background_5fcolor_5fmagenta_150',['BACKGROUND_COLOR_MAGENTA',['../d3/d0b/typedef_8h.html#a85747f9ff89b8e4b06e83f165eedefc6',1,'typedef.h']]],
  ['background_5fcolor_5fred_151',['BACKGROUND_COLOR_RED',['../d3/d0b/typedef_8h.html#a76ae5120e95972432e1fe58c775b6c37',1,'typedef.h']]],
  ['background_5fcolor_5freset_152',['BACKGROUND_COLOR_RESET',['../d3/d0b/typedef_8h.html#a79842326912e65b4dd403a5c3e836101',1,'typedef.h']]],
  ['background_5fcolor_5fwhite_153',['BACKGROUND_COLOR_WHITE',['../d3/d0b/typedef_8h.html#a00ac1d31860151def6543022ca9ef652',1,'typedef.h']]],
  ['background_5fcolor_5fyellow_154',['BACKGROUND_COLOR_YELLOW',['../d3/d0b/typedef_8h.html#a967f5bacb6964984fa903519371f46e6',1,'typedef.h']]]
];
