var searchData=
[
  ['set_5fcolor_72',['SET_COLOR',['../d3/d0b/typedef_8h.html#a65589359a2b6338289586da33e3f448c',1,'typedef.h']]],
  ['set_5fcolor_5fback_73',['SET_COLOR_BACK',['../d3/d0b/typedef_8h.html#a04c62d9aac5cc735df39e52539509b1e',1,'typedef.h']]],
  ['set_5fcolor_5ffont_74',['SET_COLOR_FONT',['../d3/d0b/typedef_8h.html#aea72d765332cd3c2ea85c26a98ec42ea',1,'typedef.h']]],
  ['string_5fplacetext_75',['string_PlaceText',['../de/de5/dynamic__memory_8c.html#a00bde209912b202c4b3b80758f942e44',1,'string_PlaceText(char *pS, const char *txt):&#160;dynamic_memory.c'],['../d2/d23/dynamic__memory_8h.html#a00bde209912b202c4b3b80758f942e44',1,'string_PlaceText(char *pS, const char *txt):&#160;dynamic_memory.c']]],
  ['string_5fsave_76',['string_save',['../de/de5/dynamic__memory_8c.html#a29fca13d0ed58e37746c110df8135701',1,'string_save(const char *pstr, const nline_t nline):&#160;dynamic_memory.c'],['../d2/d23/dynamic__memory_8h.html#a5b833ecd6184ba0a0ae3a6b60d9c87cb',1,'string_save(const char *pstr, const nline_t nline):&#160;dynamic_memory.c']]]
];
