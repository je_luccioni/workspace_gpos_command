var indexSectionsWithContent =
{
  0: "_abcdefgilmnstux",
  1: "dmt",
  2: "fgmsx",
  3: "e",
  4: "inu",
  5: "b",
  6: "ft",
  7: "_abcdfmst",
  8: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "typedefs",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Archivos",
  2: "Funciones",
  3: "Variables",
  4: "typedefs",
  5: "Enumeraciones",
  6: "Valores de enumeraciones",
  7: "defines",
  8: "Páginas"
};

