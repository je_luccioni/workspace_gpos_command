var searchData=
[
  ['xcalloc_95',['xcalloc',['../de/de5/dynamic__memory_8c.html#ac1ec0faba8a58ec358900c0457f845e8',1,'xcalloc(size_t nmemb, size_t size, nline_t nline):&#160;dynamic_memory.c'],['../d2/d23/dynamic__memory_8h.html#a20a6f8600cfd7db6211da03c21894cff',1,'xcalloc(size_t nmemb, size_t size, nline_t nline):&#160;dynamic_memory.c']]],
  ['xmalloc_96',['xmalloc',['../de/de5/dynamic__memory_8c.html#adc75fb78a0b25a13500c200628286d7e',1,'xmalloc(size_t size, nline_t nline):&#160;dynamic_memory.c'],['../d2/d23/dynamic__memory_8h.html#a24e067391194bd73ba71817d45ec59cd',1,'xmalloc(size_t size, nline_t nline):&#160;dynamic_memory.c']]],
  ['xrealloc_97',['xrealloc',['../de/de5/dynamic__memory_8c.html#ad0b74dfbf7c4bd6d521f7e80d2a10562',1,'xrealloc(void *ptr, size_t size, nline_t nline):&#160;dynamic_memory.c'],['../d2/d23/dynamic__memory_8h.html#a7f2a1ab95fdff6fcca2c6cd82b0b8d41',1,'xrealloc(void *ptr, size_t size, nline_t nline):&#160;dynamic_memory.c']]]
];
