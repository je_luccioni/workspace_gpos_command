var dynamic__memory_8h =
[
    [ "__dynamic_memory_h__", "d2/d23/dynamic__memory_8h.html#abd592e7f6cade7a555ce7aa029a173c8", null ],
    [ "__dynamic_memory_version__", "d2/d23/dynamic__memory_8h.html#ac893183c5b8c7db3fa5c46b01cbe1496", null ],
    [ "dynamic_memory_USE_GlobalData", "d2/d23/dynamic__memory_8h.html#ab86548e96a16d92a8b30c898cc412c87", null ],
    [ "dynamic_memory_USE_GlobalFunctions", "d2/d23/dynamic__memory_8h.html#a06b364348aeeb9f1eb000cfd20e593af", null ],
    [ "dynamic_memory_USE_GlobalMacro", "d2/d23/dynamic__memory_8h.html#a9d4b86aab4f59bcf9692603ac2577121", null ],
    [ "dynamic_memory_USE_GlobalMacroApis", "d2/d23/dynamic__memory_8h.html#a2ba012f6bbda9b696850b3120b16b90a", null ],
    [ "dynamic_memory_USE_GlobalTypedef", "d2/d23/dynamic__memory_8h.html#ad370523d4cf7589421d379bf0fd9b61a", null ],
    [ "dynamic_memory_USE_LocalData", "d2/d23/dynamic__memory_8h.html#a31ce39f27eff487ba25042fc134e84f9", null ],
    [ "dynamic_memory_USE_LocalFunctions", "d2/d23/dynamic__memory_8h.html#a1ee2ada9d5c6e6b8de8688b21576be19", null ],
    [ "dynamic_memory_USE_LocalMacroApis", "d2/d23/dynamic__memory_8h.html#a321bbf43b1d99c30df78d5c229c2def8", null ],
    [ "string_PlaceText", "d2/d23/dynamic__memory_8h.html#a00bde209912b202c4b3b80758f942e44", null ],
    [ "string_save", "d2/d23/dynamic__memory_8h.html#a5b833ecd6184ba0a0ae3a6b60d9c87cb", null ],
    [ "xcalloc", "d2/d23/dynamic__memory_8h.html#a20a6f8600cfd7db6211da03c21894cff", null ],
    [ "xmalloc", "d2/d23/dynamic__memory_8h.html#a24e067391194bd73ba71817d45ec59cd", null ],
    [ "xrealloc", "d2/d23/dynamic__memory_8h.html#a7f2a1ab95fdff6fcca2c6cd82b0b8d41", null ]
];