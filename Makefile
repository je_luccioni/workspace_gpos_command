################################################################################
##
## # directorio jeraquico:
##
################################################################################ 
##
#    workspace
#         |
#         +-> Makefile
#         +-> project   
#         |   |
#         |   +-> project_01 
#         |   |      |  
#         |   |      +-> src {main.c} 
#         |   |      +-> inc {main.h}
#         |   |      +-> doc 
#         |   |           |
#         |   |           +-> html {index.html}
#         |   |      +-> doxyfile
#         |   |      +-> Makefile   
#         |   +-> $(PROJECT) 
#         |          |  
#         |          +-> src {main.c} 
#         |          +-> inc {main.h}
#         |          +-> doc 
#	  |          |    |
#	  |          |    +-> html {index.html}
#         |          +-> doxyfile
#         |          +-> Makefile 
#         |
#         +-> out
#		  +-> modules
#             |
#             +-> miscellaneous
#             |      |  
#             |      +-> src {main.c} 
#             |      +-> inc {main.h}
#             |      +-> doc 
#	      |      |    |
#	      |      |    +-> html {index.html}
#             |      +-> doxyfile
#             |      +-> Makefile
#             +-> $(MODULES)     
#                    |  
#                    +-> src {main.c} 
#                    +-> inc {main.h}
#                    +-> doc 
#                    |    |
#                    |    +-> html {index.html}
#                    +-> doxyfile
#                    +-> Makefile 
#      
##
##
################################################################################
##
## include *.mk files
##
################################################################################
##
##
-include project.mk
-include version.mk
##
##
## #############################################################################
## 
## Rutas de los Directorios del Proyecto
## >> INC_PATH : ruta de los header files 
## >> SRC_PATH : ruta de los archivos fuentes
## >> OBJ_PATH : ruta donde colocaremos los archivos Objetos
## >> OUT_PATH : ruta donde crearemos el Ejecutable
## >> DOC_PATH : ruta donde colocaremos la documentacion 
##
## #############################################################################  
## 
PROJECT_PATH := projects/$(PROJECT)
INC_PATH := $(PROJECT_PATH)/inc $(addprefix modules/,$(addsuffix /inc,$(MODULES))) $(LIB_INC_PATH_LIST)
#SRC_PATH := $(PROJECT_PATH)/src
#INC_PATH = $(PROJECT_PATH)/inc $(addprefix modules/,$(addsuffix /inc,$(MODULES)))
SRC_PATH = $(PROJECT_PATH)/src $(addprefix modules/,$(addsuffix /src,$(MODULES)))
OBJ_PATH := out/obj
OUT_PATH := out/app
##
##
## #############################################################################
## 
## Listamos los archivos necesarios 
## >> SRC_FILE : Listado de archivos Fuentes *.c
## >> ASM_SRC_FILE : Listado de archivos Fuentes *.$(EXT_ASM)
## >> OBJ_FILE : Listado de Archivos Objetos 
##
## #############################################################################   
##
SRC_FILE := $(foreach source_file,$(SRC_PATH),$(wildcard $(source_file)/*.c))
#SRC_FILE := $(wildcard $(SRC_PATH)/*.c)


OBJ_FILE := $(addprefix $(OBJ_PATH)/, $(notdir $(SRC_FILE:.c=.o)))
ifeq ($(COMPILE_ASM_FILE),YES)
ASM_SRC_FILE := $(wildcard $(SRC_PATH)/*.S)
OBJ_FILE += $(addprefix $(OBJ_PATH)/,$(notdir $(ASM_SRC_FILE:.S=.o)))
SRC_FILE += $(ASM_SRC_FILE)
endif
##
##
## #############################################################################
## 
## Listamos los Header Files *.h / *.hpp
## >> 
## >> 
##
## #############################################################################   
##
INCLUDES := $(addprefix -I,$(INC_PATH)) 
##
##
## #############################################################################
## 
## Definimos el Conjunto de simbolos que deseamos pasar a la hora de 
## compilar, a preprocesador 
##
## #############################################################################
##

SYMBOLS += $(addprefix -D,$(LIST_SYMBOLS))
SYMBOLS += $(addprefix -D,$(SYMBOLS_ETC))

### 
## Establecemos el mensaje a utilizar en la compilacion, para 
## visualizar informacion sobre el proyecto a compilar.
### 
COMPILE_MSG1 = Proyecto: \"$(PROJECT)\", Aplicacion: \"$(APP_NAME)\"
COMPILE_MSG2 = Depuracion: $(MSG_DEBUG), Version del Main: \"$(MAIN_VERSION)\"
##
##
################################################################################
##
#########################[ Fin de las configuraciones ]#########################
##
################################################################################ 
##
##
##
## #############################################################################
##
## -- Especificamos la ruta de busqueda de los archivos 
##    con la estencion %.$(EXT_FILE), para el make
##
## #############################################################################
##
vpath %.o $(OBJ_PATH)
vpath %.c $(SRC_PATH)
vpath %.h $(INC_PATH)
ifeq ($(COMPILE_ASM_FILE),YES)
vpath %.S $(SRC_PATH)
endif
##
##
## #############################################################################
## 
## Definimos el TARGET para la opcion make all | make
##
## #############################################################################
all: $(APP_NAME)

##
### El lugar de este en este make depende si contine target a ejecutar, si 
### no los tienes nos conviene dejarlo junto a los demas.
### caso contrario despues del target all, para al ejecutar el make
### sin argumento ejecute el all y no los del primer target del makefile
### a incluir
##
include $(PROJECT_PATH)/Makefile
# include $(foreach mod,$(MODULES),modules/$(mod)/Makefile) 

##
##
## #############################################################################
## 
## Definimos el TARGET principal p/la opcion make 
##
## #############################################################################
$(APP_NAME): $(notdir $(OBJ_FILE))
	@echo "\n ====================[ Begin, compiling ]===================="
	@printf "Informacion:\n"
	@printf "\t\033[36m$(COMPILE_MSG1)\n"
	@printf "\t\033[36m$(COMPILE_MSG2)\n"
	@printf "\033[0m \n"
ifeq ($(VIEW_COMPILE_INFO),YES)
	#$(CC) -o $(OUT_PATH)/$(APP_NAME)$(EXT_APP) $(OBJ_FILE)
	$(CC) $(CCFLAGS) $(LDFLAGS) $(LDFILE) -o $(OUT_PATH)/$(APP_NAME) $(OBJ_FILE) $(addprefix -l,$(LIB_NAME)) $(addprefix -L,$(LIB_PATH))
	@echo "\n   *********[ Tamaño del Programa compilado ]**********"
	size -B $(OUT_PATH)/$(APP_NAME)$(EXT_APP)
else
#	@$(CC) $(CCFLAGS) $(LDFLAGS) $(LDFILE) -o $(OUT_PATH)/$(APP_NAME)$(EXT_APP) $(OBJ_FILE)
	$(CC) $(CCFLAGS) $(LDFLAGS) $(LDFILE) -o $(OUT_PATH)/$(APP_NAME) $(OBJ_FILE) $(addprefix -l,$(LIB_NAME)) $(addprefix -L,$(LIB_PATH))
	@echo "\n   *********[ Tamaño del Programa compilado ]**********"
	@size -B $(OUT_PATH)/$(APP_NAME)
endif
	@echo "\n ====================[ make all, END ]====================\n"	



## #############################################################################
## 
## Definimos el TARGET para obtener los object files, compile source files
##
## #############################################################################
%.o: %.c
ifeq ($(VIEW_COMPILE_INFO),YES)
	@echo "\n ====================[ Begin, compiling C file $(notdir $<) ]===================="
	$(CC) $(SYMBOLS) -c $< $(CCFLAGS) $(INCLUDES) -o $(OBJ_PATH)/$@
	$(CC) $(SYMBOLS) -c $< $(CCFLAGS) $(INCLUDES) -MM > $(OBJ_PATH)/$(@:.o=.d)
	@echo "\t***** compiled $(notdir $<) for get $@ *****"
	@echo "\n ====================[ End, compiling C file $(notdir $<) ]====================\n"
else
	@echo "\n ====================[ Begin, compiling C file $(notdir $<) ]===================="
	@$(CC) $(SYMBOLS) $(CCFLAGS) $(INCLUDES) -c $< -o $(OBJ_PATH)/$@
	@$(CC) $(SYMBOLS) $(CCFLAGS) $(INCLUDES) -c $< -MM > $(OBJ_PATH)/$(@:.o=.d)
	@echo "\t***** compiled $(notdir $<) for get $@ *****"
	@echo "\n ====================[ End, compiling C file $(notdir $<) ]====================\n"
endif

ifeq ($(COMPILE_ASM_FILE),YES)
%.o: %.S
ifeq ($(VIEW_COMPILE_INFO),YES)
	@echo "\n ====================[ Begin, compiling assembly file $(notdir $<) ]===================="
	$(CASM) $(SYMBOLS) $(CASMFLAGS) $(INCLUDES) -c $< -o $(OBJ_PATH)/$@
	$(CASM) $(SYMBOLS) $(CASMFLAGS) $(INCLUDES) -c $< -MM > $(OBJ_PATH)/$(@:.o=.d)	
	@echo "compiled $(notdir $<) for get $@ "
	@echo "\n ====================[ End, compiling C file $(notdir $<) ]====================\n"	
else
	@echo "\n ====================[ Begin, compiling assembly file $(notdir $<) ]===================="
	@$(CASM) $(SYMBOLS) $(CASMFLAGS) $(INCLUDES) -c $< -o $(OBJ_PATH)/$@
	@$(CASM) $(SYMBOLS) $(CASMFLAGS) $(INCLUDES) -c $< -MM > $(OBJ_PATH)/$(@:.o=.d)	
	@echo "compiled $(notdir $<) for get $@ "
	@echo "\n ====================[ End, compiling C file $(notdir $<) ]====================\n"	
endif
endif
##
##
## #############################################################################
## 
## Definimos el TARGET para la limpieza de directorios 
##
## #############################################################################
clean:
	@clear
	@echo "\n==========[ Limpiando los directorios ]==========\n"
	@rm -f $(OBJ_PATH)/*.o
	@rm -f $(OBJ_PATH)/*.d
# 	@rm -f $(OUT_PATH)/$(APP_NAME)
	@rm -f $(APP_NAME)
	@echo "\n==========[ make clear, END ]==========\n\n"
##
##
## #############################################################################
## 
## Definimos el TARGET para una nueva compilacion clean and make all 
##
## #############################################################################
new: 
	@make clean --no-print-directory 
	@make all --no-print-directory
##
##
## #############################################################################
## 
## Definimos el TARGET para la ejecucion de la palicacion 
## 	>> clean, limpiamos los directorios
## 	>> all, compilacion 
##
## #############################################################################
run: new
	@echo "\n==========================[Begin, make run $(APP_NAME), version: \"$(MAIN_VERSION)\"]============================\n"
	@$(OUT_PATH)/./$(APP_NAME)$(EXT_APP) $(ARG)
	@echo "\n==========================[End  , make run $(APP_NAME), version: \"$(MAIN_VERSION)\"]============================\n"
##
##
## #############################################################################
## 
## Definimos el TARGET para mostrar el mensaje de ayuda para el usuario
##
## #############################################################################

help:
	@clear	
	@echo "\n\033[31m========================================[ Imprimiendo Ayuda para Usuario ]========================================\033[0m\n"
	@echo "\033[32m El Orden del seteo de los flags es indiferente, almenos que se lo indique, de forma especifica (solo en casos particualres)\033[0m"	
	@printf "\t\033[36m make all\033[0m   Compila el proyecto segun los FLAGS seteados a su valor por defecto\n"
	@printf "\t\033[36m make all APP_NAME=<nameAppDesired>\033[0m   Idem al anterior pero el ejecutable toma el nombre \"nameAppDesired\"\n"	
	@printf "\t\033[36m make clean\033[0m   Limpia los directorios, es una buena tecnica cuando se crean modificaciones de FLAGS y demas parametros\n"
	@printf "\t\033[36m make new\033[0m    Este ejecuta un clean y luego un all, limpia y recompila\n"	
	@printf "\t\033[36m make run\033[0m    este ejecuta en orden los siguentes target : clean, all y luego ejecuta la aplicacion pasandole la variable ARG, como argumento\n"	
	@printf "\t\033[36m make new APP_NAME=<nameAppDesired>, idem al anterior pero seteando el nombre de la app\033[0m\n"
	@printf "\n\033[32m Listado de Variables que podemos establecer de la misma forma que la anterior\033[0m\n"
	@printf "\t\033[36m APP_NAME \033[0m Nombre que recibira la Apliacion compilada.\n"
	@printf "\t\033[36m MAIN_VERSION \033[0m Identificador de la Version a Compilar 0~10\n"	
	@printf "\t\033[36m ARG \033[0m Listado de Argumentos que podemos pasar, ejemplo de uso util:\n"
	@printf "\t\t\033[36m make run ARG=\" arga 0 er\" \033[0m Se recompila y ejecuta la aplciacion pasandole el listado de 3 argumentos\n"		
	@printf "\t\033[36m CFLAGS \033[0m Mediante este podemos establecer FLASG para el C compiler ejemplo de uso util:\n"
	@printf "\t\t\033[36m make new CFLAGS=\"-Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields\" \033[0m optimizacion para tamaño de codigo\n"
	@printf "\t\033[36m make run ARG=\"\\\"<arg_String>\\\" <arg2> <arg3>\"\033[0m    para pasarle un listado de argumentos que contiene cadena de caracteres, incluyendo el caracter de espacio\n"
	@echo "\n\033[33m==================================[ begin, doc help ]==================================\033[0m"
	@make doc_help --no-print-directory
	@echo "\n\033[33m==================================[ begin, doc help ]==================================\033[0m"
	@echo "\n\033[33m==============================[begin, GIT HELP]==============================\033[0m"
	@printf "\nSintaxis (Uso):\n\n"	
	@printf "\tmake\033[36m PUSH\033[0m Para realizar un push del repositorio Local\n"
	@printf "\tmake\033[36m COMMIT\033[0m Crea un Coomit y push interactuando \n"
	@printf "\tmake\033[36m LOG\033[0m Muestra el log del repositorio local\n"
	@printf "\tmake\033[36m INFO\033[0m Muestra la inforacion Actual del Repositorio local\n"
	@printf "\tmake\033[36m help\033[0m Muestra este mensaje de Ayuda\n"
	@echo "\033[33m==============================[ end, GIT HELP ]==============================\033[0m"
	@echo "\n\033[31m========================================[ Fin del mensage de ayuda ]========================================\033[0m\n\n"
##
##
## #############################################################################
## 
## Definimos el TARGET para el debug del Makefile
##
## #############################################################################
info_flags:
	@clear
	@echo "\n==============================[ print Info p/DEBUG Makefile]==============================\n"	
	@echo "\t-----[ Linker, Linker flags and linker script]-----"
	@echo "\tlinker commando \"LD\" : $(LD)"
	@echo "\tlinker file script \"LDFILE\" : $(LDFILE)"
	@echo "\tlinker flags \"LDFLAGS\" : $(LDFLAGS)"
	@echo "\t-----[compilers for source files]-----"
	@echo "\t Compiler C \"CC\": $(CC)"
	@echo "\t Compiler C++ \"CCPP\": $(CCPP)"
	@echo "\t Compiler Asembler \"CASM\": $(CASM)"
	@echo "\t Include header File \"INCLUDES\": $(INCLUDES)"
	@echo "\t-----[compilers flags]-----"
	@echo "\t Flags, Compiler C, set for user make \"CFLAGS\": $(CFLAGS)"
	@echo "\t CCFLAGS: $(CCFLAGS)"
	@echo "\t Flags, Compiler C++, set for user make \"CPPFLAGS\": $(CPPFLAGS)"
	@echo "\t CCPPFLAGS: $(CCPPFLAGS)"
	@echo "\t Flags, Compiler Assembler, set for user make \"ASMFLAGS\": $(ASMFLAGS)"
	@echo "\t CASMFLAGS: $(CASMFLAGS)"
	@echo "\t SYMBOLS, set for user compiler time"
	@echo "\t $(SYMBOLS)"	
	@echo "\n====================================[ meke info_flags, END ]====================================\n"
	
info:
	@clear
	@echo "\n==============================[ print Info p/DEBUG Makefile]==============================\n"	
	@echo "\t-----[ Project Info ]-----"
	@echo "\t source file path: $(SRC_PATH)"
	@echo "\t header file path: $(INC_PATH)"
	@echo "\t source file : $(SRC_FILE)"
	@echo "\t Project name: $(PROJECT_PATH)"
	@echo "\t Main Version: $(MAIN_VERSION)"
	@echo "\t Aplication Name: $(APP_NAME)"
	@echo "\t-----[ documment generator ]-----"
	@echo "\t DOC_FILE : $(DOC_FILE) "
	@echo "\t DOC_PATH : $(DOC_PATH) "
	@echo "\t DOC_CFG : $(DOC_CFG) "
	@echo "\t DOC_CMD : $(DOC_CMD) "
	@echo "\t DOC_FLAGS : $(DOC_FLAGS)"
	@echo "\t-----[ Linker, Linker flags and linker script]-----"
	@echo "\tlinker commando \"LD\" : $(LD)"
	@echo "\tlinker file script \"LDFILE\" : $(LDFILE)"
	@echo "\tlinker flags \"LDFLAGS\" : $(LDFLAGS)"
	@echo "\t-----[compilers for source files]-----"
	@echo "\t Compiler C \"CC\": $(CC)"
	@echo "\t Compiler C++ \"CCPP\": $(CCPP)"
	@echo "\t Compiler Asembler \"CASM\": $(CASM)"
	@echo "\t Include header File \"INCLUDES\": $(INCLUDES)"
	@echo "\t-----[compilers flags]-----"
	@echo "\t Flags, Compiler C, set for user make \"CFLAGS\": $(CFLAGS)"
	@echo "\t CCFLAGS: $(CCFLAGS)"
	@echo "\t Flags, Compiler C++, set for user make \"CPPFLAGS\": $(CPPFLAGS)"
	@echo "\t CCPPFLAGS: $(CCPPFLAGS)"
	@echo "\t Flags, Compiler Assembler, set for user make \"ASMFLAGS\": $(ASMFLAGS)"
	@echo "\t CASMFLAGS: $(CASMFLAGS)"
	@echo "\t-----[symbols]-----"
	@echo "\t SYMBOLS: $(SYMBOLS)"
	@echo "\t LIST_SYMBOLS: $(LIST_SYMBOLS)"
	@echo "\t-----[versiones]-----"
	@echo "\t main: $(MAIN_VERSION)"
	@echo "\t other: $(OTHER_VERSION)"
	@echo "\t-----[ others vars]-----"
	@echo "\tVIEW_COMPILE_INFO : $(VIEW_COMPILE_INFO)"	
	@echo "\tArquitectura de la Libreria : $(ARCH)"
	@echo "\tOBJ_FILE : $(OBJ_FILE)"
	@echo "\tOBJ_PATH : $(OBJ_PATH)"
	@echo "\tAPP_NAME : $(APP_NAME)"
	@echo "\tOUT_PATH : $(OUT_PATH)"
	@echo "\t\"S.O\" host of the compiler : $(SO_COMPILER)"
	@echo "\tabs OBJ_FILE: $(notdir $(OBJ_FILE))"
	@echo "\t-----[ extern libraries ]-----"
	@echo "\t LIB_NAME: $(LIB_NAME)"
	@echo "\t LIB_PATH: $(LIB_PATH)"
	@echo "\t-----[ XXXXXXXXXXXXXXXX ]-----"
	@echo "\n====================================[ meke info, END ]====================================\n"


PUSH:
	@echo "\n==============================[begin, target git PUSH]==============================\n"
	@./git_script push
	@echo "\n==============================[ End , target git PUSH ]==============================\n"
	

COMMIT: clean
	@echo "\n==============================[begin, target git COMMIT]==============================\n"
	@./git_script commit
	@echo "\n==============================[ End , target git COMMIT ]==============================\n"

LOG:
	@echo "\n==============================[begin, target git LOG]==============================\n"
	@./git_script log
	@echo "\n==============================[ End , target git LOG ]==============================\n"
INFO:
	@echo "\n==============================[begin, target git LOG]==============================\n"
	@./git_script info
	@echo "\n==============================[ End , target git LOG ]==============================\n"	
	
	

.ONESHELL:
remote_push:
	scp -P 5679 -r ../workspace_gnu_c c_tallion@127.0.0.1:/home/c_tallion/
	#cd ..
	#scp -P 5679 -r ./workspace_gnu_c c_tallion@127.0.0.1:/home/c_tallion/
	
install:
	@make new --no-print-directory
	@sudo cp $(OUT_PATH)/./$(APP_NAME)$(EXT_APP) /usr/bin
#local_puss	
	
doc:
	cd $(PROJECT_PATH)
	@make document --no-print-directory
# .ONESHELL:
# 	@cd modules/miscellaneous
# 	@make miscellaneous_document

doc_clean:
	@cd $(PROJECT_PATH)
	@make document_clean --no-print-directory
	
doc_create:
	@cd $(PROJECT_PATH)
	@make document_create --no-print-directory
	
doc_open:
	@make doc_clean --no-print-directory
	@make doc --no-print-directory
	@echo "opening index.html ..... "
	@$(DOC_VIEW) $(PROJECT_PATH)/doc/html/index.html
	
doc_help:
	@printf "\nSintaxis (Uso):"
	@printf "\t\033[33mImportante: Debemos tener instalado \033[32mdoxygen\033[33m en nuesto sistema.\033[0m\n"
	@printf "\t\t En linux:\033[36m sudo apt-get install doxygen\033[0m\n"
	@printf "\t\t En windows, download unpack 7Zip and install:\033[36m https://sourceforge.net/projects/doxygen/files/snapshots/\033[0m\n"
	@printf "\tmake\033[36m doc\033[0m Genera la documentacion\n"
	@printf "\tmake\033[36m doc_clean\033[0m Genera la documentacion\n"
	@printf "\tmake\033[36m doc_create\033[0m Genera la documentacion\n"
	@printf "\tmake\033[36m doc_open\033[0m Limpia la documentacion, la crea de nuevo\n"
	@printf "\ty abre la misma con navegador establecido en la variable \n"
	@printf "\t\"DOC_VIEW = $(DOC_VIEW)\", establecida en el archivo \"\033[32mproject.mk\033[0m\"\n"
	@printf "\tPodemos establecerlo al llamar a \"make\" de la siguente manera:\n"
	@printf "\t\t\033[36mmake doc_open DOC_VIEW="firefox"\033[0m\n"
	@printf "\tmake\033[36m doc_help\033[0m Muestra el mensaje de ayuda de la docuemtnacion solamente\n"
