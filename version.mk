###
## #############################################################################
## 
##  Definicion la  version de compilacion para cada modulo de codigo
##  En caso de usar eclipse como IDE, recuerde configurar el IDE, para evitar 
##  errores del entorno grafico(marcado de error p/typedef no definidos ). 
##		project -> Properties -> C/C++ General -> path and symbols
##		TABS{symbols}, select GNU GCC add Symbols : __M32__
##
## #############################################################################
##
##--------------------------------------------------------------------------
#  modulo main:
#	+ 0 :
#	+ 1 :
#	+ 2 :
#	+ 3 :
#
##--------------------------------------------------------------------------
MAIN_VERSION ?= 10
##




###
## #############################################################################
## 
## Agregamos a SYMBOLS el listado de versionado para cada archivo 
##   + 0, main.c
##   + 1, main.h and main.c -> APIs
##   + 2                                      
##   + 3                                                        
##
## #############################################################################
#AUTHOR := "Jesus Emanuel, Luccioni"
#SYMBOLS_ETC := AUTHOR='"Luccioni,\x20Jesus\tEmanuel"'
     
#SYMBOLS_ETC := LABEL_NAME_NUMBER=0x2536
#SYMBOLS_ETC += LABEL_NAME_STRING='"Macro\x20Label\tcomo\x20string"'
#SYMBOLS_ETC += LABEL_NAME_STRING='"Macro\x20Label\tcomo\x20string"'
SYMBOLS += -DLABEL_NAME_STRING='"Macro Label como string"'
SYMBOLS += -DLABEL_NAME_NUMBER=0x2536
SYMBOLS += -DLABEL_NAME_VCT='0x05,0x22,0x33,0x45,0xee'

SYMBOLS += $(addprefix -D,$(SYMBOLS_ETC))
#SYMBOLS += -D$(LABEL_NAME_STRING)       
SYMBOLS += -D__main_version__=$(MAIN_VERSION)
